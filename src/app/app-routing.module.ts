import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { ExampleComponent } from './pages/example/example.component';
import { HorizontalComponent } from './pages/horizontal/horizontal.component';
import { TimeComponent } from './pages/time/time.component';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'example/:type', component: ExampleComponent },
  { path: 'horizontal/:type', component: HorizontalComponent },
  { path: 'time/:type', component: TimeComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', redirectTo: '/home' }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
