import * as d3 from 'd3';
import * as Model from './chart-data.model';
import { D3ScaleType } from './enums';

export class D3Axes {
  context: any;
  margin: any;
  dimensions: any;

  private bottomAxesHeight = 0;
  private leftAxesWidth = 0;
  private rightAxesWidth = 0;
  private axes: any;
  private grid: any;
  private bottomLabels: string[] = [];
  private leftLabels: string[] = [];
  private rightLabels: string[] = [];
  private leftTextHeight = 0;
  private rightTextHeight = 0;
  private textTransformRatio = 1;
  private splitGutter = 60;
  private splitGutterVertical = 20;

  constructor(context: any) {
    this.context = context; // d3 will choke if svg is not referenced through the original context
  }

  merge(pos: number, points: any[]): any[] {
    const hash = {};
    const arr = [];
    for (const point of points) {
      if (hash[point[pos]] !== true) {
        arr[arr.length] = point[pos];
        hash[point[pos]] = true;
      }
    }
    return arr;
  }

  buildScaleDomain(data: Model.ChartData, scaleData: any, scaleIndex: number): any[] {
    let usageCountX = 0;
    let usageCountY = 0;
    let isBarChart = false;
    let allPoints = [];
    let scaleDomain = [].concat(scaleData.domain);
    data.groups.forEach((group) => {
      if (!group.disabled) {
        if (group.xScale === scaleIndex || group.yScale === scaleIndex) {
          if (group.xScale === scaleIndex) {
            usageCountX += 1;
          } else {
            usageCountY += 1;
          }
          if (group.bars && group.bars.points && group.bars.points.length > 0) {
            isBarChart = true;
            allPoints = allPoints.concat(group.bars.points);
          }
          if (group.boxes && group.boxes.points && group.boxes.points.length > 0) {
            group.boxes.points.forEach(point => {
              const individualPoints = [];
              for (let i = 1; i < point.length; i++) {
                individualPoints.push([point[0], point[i]]);
              }
              allPoints = allPoints.concat(individualPoints);
            });
          }
          if (group.lines && group.lines.length > 0) {
            group.lines.forEach(line => {
              if (['main', 'ucl', 'lcl', 'group'].includes(Model.ChartLineTypeClasses[line.type]) &&
                line.points && line.points.length > 0) {
                allPoints = allPoints.concat(line.points);
              }
            });
          }
          if (group.notes && group.notes.length > 0) {
            group.notes.forEach(note => {
              allPoints = allPoints.concat([note.point]);
            });
          }
        }
      }
    });
    if (allPoints.length === 0) {
      // Everything is disabled; use default domain
      return scaleDomain;
    }
    if (scaleData.type === D3ScaleType.Point || scaleData.type === D3ScaleType.Band) {
      if (usageCountX > 0) {
        allPoints = this.merge(0, allPoints);
      } else {
        allPoints = this.merge(1, allPoints);
      }
      // Filter missing points from scaleDomain
      for (let i = scaleDomain.length - 1; i >= 0; i--) {
        const lookup = allPoints.find(element => element === scaleDomain[i]);
        if (typeof lookup === 'undefined') {
          scaleDomain.splice(i, 1);
        }
      }
    } else {
      let min = Infinity;
      let max = -Infinity;
      allPoints.forEach(point => {
        if (usageCountX) {
          if (point[0] < min) { min = point[0]; }
          if (point[0] > max) { max = point[0]; }
        } else {
          if (point[1] !== null) {
            const rowMax = Math.max.apply(null, point.slice(1, 2));
            const rowMin = Math.min.apply(null, point.slice(1, 2));
            if (rowMax > max) { max = rowMax; }
            if (rowMin < min) { min = rowMin; }
          }
        }
      });
      if (min === Infinity || max === -Infinity) {
        scaleDomain = scaleData.domain;
      } else if (scaleData.type === D3ScaleType.Log) {
        min = (min <= 0) ? 0.1 : min / 2;
        max = Math.pow(10, Math.floor(max).toString().length);
        scaleDomain = [min, max];
      } else {
        // Add 5% buffer for space on each side
        // NOTE: Scale supporting negative values does not guarantee drawing code supports it...
        if (usageCountY) {
          const buffer = (max - min) * .05;
          max += buffer;
          if (min >= 0) {
            if (isBarChart) { // Barcharts must have a zero point in the bar (can still go below zero)
              min = 0;
            } else {
              if (min - buffer > 0) { // Non-barcharts don't require zero. maximize vertical usage.
                min -= buffer;
              } else { // All values are positive, don't show negatives on the scale.
                min = 0;
              }
            }
          } else { // Some values are negative add a display buffer
            min -= buffer;
          }
        }
        scaleDomain = [min, max];
      }
    }
    return scaleDomain;
  }

  getScaleCount(data: Model.ChartData, location: Model.ChartAxisLocation): number {
    let count = 0;
    data.scales.forEach(scaleData => {
      if (scaleData.location === location) {
        count++;
      }
    });
    return count;
  }

  drawAxes(data: Model.ChartData): void {
    this.axes = [];
    this.grid = [];
    d3.select(this.context.svg).node().selectAll('g.axis').remove();
    this.bottomAxesHeight = this.leftAxesWidth = this.rightAxesWidth = 0;
    this.bottomLabels = [];
    this.leftLabels = [];
    this.rightLabels = [];
    let bottomLocationIndex = -1;
    let leftLocationIndex = -1;
    let rightLocationIndex = -1;
    const bottomScaleCount = this.getScaleCount(data, Model.ChartAxisLocation.Bottom);
    const leftScaleCount = this.getScaleCount(data, Model.ChartAxisLocation.Left);
    const rightScaleCount = this.getScaleCount(data, Model.ChartAxisLocation.Right);
    data.scales.forEach((scaleData, idx) => {
      let scaleDomain = scaleData.domain;
      if (!scaleData.lockDomain || !scaleDomain || scaleDomain.length === 0) {
        if (scaleData.domainCustom) {
          scaleDomain = scaleData.domainCustom;
        } else {
          scaleDomain = this.buildScaleDomain(data, scaleData, idx);
        }
      }
      scaleData.scale.domain(scaleDomain);
      switch (scaleData.location) {
        case Model.ChartAxisLocation.Bottom:
          bottomLocationIndex++;
          this.drawBottomAxis(scaleData, data, bottomScaleCount, bottomLocationIndex);
          break;
        case Model.ChartAxisLocation.Left:
          leftLocationIndex++;
          this.drawLeftAxis(scaleData, data, leftScaleCount, leftLocationIndex);
          break;
        case Model.ChartAxisLocation.Right:
          rightLocationIndex++;
          this.drawRightAxis(scaleData, data, rightScaleCount, rightLocationIndex);
          break;
      }
    });
    if (!this.context.miniChart) {
      this.drawLabels(data);
    }
    let tooWide = false;
    if (this.bottomAxesHeight > this.context.margin.bottom) {
      this.context.margin.bottom = this.bottomAxesHeight;
      tooWide = true;
    }
    if (this.context.minMargin.bottom > this.context.margin.bottom) {
      this.context.margin.bottom = this.context.minMargin.bottom;
      tooWide = true;
    }
    if (this.leftAxesWidth > this.context.margin.left) {
      this.context.margin.left = this.leftAxesWidth;
      tooWide = true;
    }
    if (this.context.minMargin.left > this.context.margin.left) {
      this.context.margin.left = this.context.minMargin.left;
      tooWide = true;
    }
    if (this.rightAxesWidth > this.context.margin.right) {
      this.context.margin.right = this.rightAxesWidth;
      tooWide = true;
    }
    if (this.context.minMargin.right > this.context.margin.right) {
      this.context.margin.right = this.context.minMargin.right;
      tooWide = true;
    }
    if (tooWide) {
      this.drawAxes(data);
    } else {
      this.drawGrid();
    }
  }

  getTrueScaleIndex(data: Model.ChartData, location: Model.ChartAxisLocation, locationIndex: number): number {
    let trueIndex: number = null;
    let locationCount = -1;
    data.scales.forEach((scale, scaleIndex) => {
      if (scale.location === location) {
        locationCount++;
        if (locationCount === locationIndex) {
          trueIndex = scaleIndex;
        }
      }
    });
    return trueIndex;
  }

  isScaleHidden(data: Model.ChartData, scaleIndex: number): boolean {
    const isHidden = data.scales[scaleIndex].hideScale;
    return isHidden;
  }

  isBottomScaleActive(data: Model.ChartData, scaleIndex: number): boolean {
    if (data.scales[scaleIndex].forceDisplay) {
      return true;
    }
    let isActive = false;
    data.groups.forEach(group => {
      if (group.xScale === scaleIndex && !group.disabled) {
        isActive = true;
      }
    });
    return isActive;
  }

  getBottomScaleActiveCount(data: Model.ChartData): number {
    let count = 0;
    data.scales.forEach((scale, scaleIndex) => {
      if (scale.location === Model.ChartAxisLocation.Bottom &&
        this.isBottomScaleActive(data, scaleIndex)) {
        count++;
      }
    });
    return count;
  }

  getBottomIntervalCount(data: Model.ChartData): number {
    let intervalCount = 0;
    data.scales.forEach((scale, scaleIndex) => {
      if (scale.location === Model.ChartAxisLocation.Bottom &&
        this.isBottomScaleActive(data, scaleIndex)) {
        if (scale.domain && scale.domain.length > 1) {
          intervalCount += (scale.domain.length - 1);
        } else {
          intervalCount += 1;
        }
      }
    });
    return intervalCount;
  }

  getBottomAxisIntervalWidth(data: Model.ChartData): number {
    const bottomRangeWidth = this.context.dimensions.width - this.context.margin.left - this.context.margin.right;
    const activeScaleCount = this.getBottomScaleActiveCount(data);
    const effectiveRangeWidth = bottomRangeWidth - (this.splitGutter * (activeScaleCount - 1));
    const intervalCount = this.getBottomIntervalCount(data);
    const intervalWidth = effectiveRangeWidth / intervalCount;
    return intervalWidth;
  }

  getBottomAxisWidths(data: Model.ChartData): number[] {
    const intervalWidth = this.getBottomAxisIntervalWidth(data);
    const axisWidths: number[] = [];
    data.scales.forEach((scale) => {
      if (scale.location === Model.ChartAxisLocation.Bottom) {
        if (scale.domain && scale.domain.length > 1) {
          const axisWidth = (scale.domain.length - 1) * intervalWidth;
          axisWidths.push(axisWidth);
        } else {
          axisWidths.push(intervalWidth);  // have to put a placeholder, or it will error
        }
      }
    });
    return axisWidths;
  }

  getBottomAxisStarts(data: Model.ChartData, axisWidths: number[]): number[] {
    const axisStarts: number[] = [];
    let widthTracker = 0;
    axisWidths.forEach((width, index) => {
      axisStarts.push(widthTracker);
      if (this.isBottomScaleActive(data, index)) {
        widthTracker += (width + this.splitGutter);
      }
    });
    return axisStarts;
  }

  isSideScaleActive(data: Model.ChartData, scaleIndex: number): boolean {
    if (data.scales[scaleIndex].forceDisplay) {
      return true;
    }
    let isActive = false;
    data.groups.forEach(group => {
      if (group.yScale === scaleIndex && !group.disabled) {
        isActive = true;
      }
    });
    return isActive;
  }

  getLeftScaleActiveCount(data: Model.ChartData): number {
    let count = 0;
    data.scales.forEach((scale, scaleIndex) => {
      if (scale.location === Model.ChartAxisLocation.Left &&
        this.isSideScaleActive(data, scaleIndex)) {
        count++;
      }
    });
    return count;
  }

  getLeftAxisHeights(data: Model.ChartData): number[] {
    const axisHeights: number[] = [];
    const rangeHeight = this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom;
    const activeCount = this.getLeftScaleActiveCount(data) || 1;
    const axisHeight = ((rangeHeight + this.splitGutterVertical) / activeCount);
    data.scales.forEach(scale => {
      if (scale.location === Model.ChartAxisLocation.Left) {
        axisHeights.push(axisHeight);
      }
    });
    return axisHeights;
  }

  getLeftAxisStarts(data: Model.ChartData, axisHeights: number[]): number[] {
    const axisStarts: number[] = [];
    let heightTracker = 0;
    axisHeights.forEach((height, index) => {
      axisStarts.push(heightTracker);
      const trueScaleIndex = this.getTrueScaleIndex(data, Model.ChartAxisLocation.Left, index);
      if (this.isSideScaleActive(data, trueScaleIndex)) {
        heightTracker += (height);
      }
    });
    return axisStarts;
  }

  getRightScaleActiveCount(data: Model.ChartData): number {
    let count = 0;
    data.scales.forEach((scale, scaleIndex) => {
      if (scale.location === Model.ChartAxisLocation.Right &&
        this.isSideScaleActive(data, scaleIndex)) {
        count++;
      }
    });
    return count;
  }

  getRightAxisHeights(data: Model.ChartData): number[] {
    const axisHeights: number[] = [];
    const rangeHeight = this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom;
    const activeCount = this.getRightScaleActiveCount(data) || 1;
    const axisHeight = ((rangeHeight + this.splitGutterVertical) / activeCount);
    data.scales.forEach(scale => {
      if (scale.location === Model.ChartAxisLocation.Right) {
        axisHeights.push(axisHeight);
      }
    });
    return axisHeights;
  }

  getRightAxisStarts(data: Model.ChartData, axisHeights: number[]): number[] {
    const axisStarts: number[] = [];
    let heightTracker = 0;
    axisHeights.forEach((height, index) => {
      axisStarts.push(heightTracker);
      const trueScaleIndex = this.getTrueScaleIndex(data, Model.ChartAxisLocation.Left, index);
      if (this.isSideScaleActive(data, trueScaleIndex)) {
        heightTracker += (height);
      }
    });
    return axisStarts;
  }

  drawBottomAxis(scaleData: Model.ChartScale, data: Model.ChartData, bottomScaleCount: number, bottomScaleIndex: number): void {
    // const that = this;
    const translateX = this.context.margin.left;
    let translateY = this.context.dimensions.height - this.context.margin.bottom + this.bottomAxesHeight;
    const bottomRangeWidth = this.context.dimensions.width - this.context.margin.left - this.context.margin.right;
    let axisWidths = [];
    let axisStarts = [];
    for (let i = 0; i < bottomScaleCount; i++) {
      axisWidths.push(bottomRangeWidth);
      axisStarts.push(0);
    }
    if (bottomScaleCount > 1 && (data.splitBottomScaleRange || data.singleLegendMode)) {
      axisWidths = this.getBottomAxisWidths(data);
      axisStarts = this.getBottomAxisStarts(data, axisWidths);
      translateY = this.context.dimensions.height - this.context.margin.bottom;
      scaleData.scale.range([axisStarts[bottomScaleIndex], axisStarts[bottomScaleIndex] + axisWidths[bottomScaleIndex]]);
    } else {
      scaleData.scale.range([0, bottomRangeWidth]);
    }
    const newAxis = d3.axisBottom(scaleData.scale);
    if (scaleData.type === D3ScaleType.Point ||
      scaleData.type === D3ScaleType.Band) {
      this.setExplicitTickValues(newAxis, scaleData.scale, axisWidths[bottomScaleIndex], scaleData.hideLabels);
    } else if (scaleData.type === D3ScaleType.Log) {
      if (scaleData.hideLabels) {
        newAxis.ticks([]);
      } else {
        newAxis.ticks(d3.format('d'));
      }
    } else {
      if (scaleData.hideLabels) {
        newAxis.ticks([]);
      } else {
        newAxis.ticks(null);
      }
    }
    this.axes.push(newAxis);
    const trueScaleIndex = this.getTrueScaleIndex(data, Model.ChartAxisLocation.Bottom, bottomScaleIndex);
    const svgGroup = this.drawAxis(newAxis, translateX, translateY, 'axis bottom', trueScaleIndex);
    svgGroup.style('color', this.context.fontColor);
    if (this.isBottomScaleActive(data, trueScaleIndex) &&
      !this.isScaleHidden(data, trueScaleIndex)) {
      svgGroup.style('opacity', 1);
    } else {
      svgGroup.style('opacity', 0);
    }
    this.labelWrapperBottom(svgGroup, scaleData, axisWidths[bottomScaleIndex]);

    if (!data.splitBottomScaleRange && !data.singleLegendMode) {
      if (this.isBottomScaleActive(data, bottomScaleIndex)) {
        this.bottomAxesHeight += svgGroup.node().getBBox().height + 5;
      }
    } else {
      this.bottomAxesHeight = Math.max(this.bottomAxesHeight, svgGroup.node().getBBox().height + 5);
    }
    this.getGridLinesX(data, scaleData, newAxis, trueScaleIndex);

    this.drawBottomSliders(svgGroup, scaleData, data, data.groups.filter(group => !group.disabled), 'handlesWrap', false, bottomScaleIndex);
    if (!data.splitBottomScaleRange) {
      // Not rendering the overall drag range across multiple bottom scales
      this.drawBottomSliders(svgGroup, scaleData, data, [scaleData], 'handlesMainWrap', true, bottomScaleIndex);
    }
  }

  drawLeftAxis(scaleData: Model.ChartScale, data: Model.ChartData, leftScaleCount: number, leftLocationIndex: number): void {
    let translateX = this.context.margin.left - this.leftAxesWidth;
    const translateY = this.context.margin.top;
    const rangeHeight = this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom;
    let axisHeights = [];
    let axisStarts = [];
    for (let i = 0; i < leftScaleCount; i++) { // default to not split
      axisHeights.push(rangeHeight);
      axisStarts.push(0);
    }
    if (leftScaleCount > 1 && data.splitLeftScaleRange) {
      axisHeights = this.getLeftAxisHeights(data);
      axisStarts = this.getLeftAxisStarts(data, axisHeights);
      translateX = this.context.margin.left;
      scaleData.scale.range([axisStarts[leftLocationIndex] + axisHeights[leftLocationIndex] -
        this.splitGutterVertical, axisStarts[leftLocationIndex]]);
    } else {
      scaleData.scale.range([rangeHeight, 0]);
    }
    const newAxis = d3.axisLeft(scaleData.scale);
    if (scaleData.type === D3ScaleType.Point ||
      scaleData.type === D3ScaleType.Band) {
      this.setExplicitTickValues(newAxis, scaleData.scale, axisHeights[leftLocationIndex], scaleData.hideLabels);
    } else if (scaleData.type === D3ScaleType.Log) {
      if (scaleData.hideLabels) {
        newAxis.ticks([]);
      } else {
        newAxis.ticks(axisHeights[leftLocationIndex] / 25, d3.format('d'));
      }
    } else {
      if (scaleData.hideLabels) {
        newAxis.ticks([]);
      } else {
        newAxis.ticks(null);
      }
    }
    this.axes.push(newAxis);
    const trueScaleIndex = this.getTrueScaleIndex(data, Model.ChartAxisLocation.Left, leftLocationIndex);
    const svgGroup = this.drawAxis(newAxis, translateX, translateY, 'axis left', trueScaleIndex);
    svgGroup.style('color', this.context.fontColor);
    if ((!data.splitLeftScaleRange || this.isSideScaleActive(data, trueScaleIndex)) &&
      !this.isScaleHidden(data, trueScaleIndex)) {
      svgGroup.style('opacity', 1);
    } else {
      svgGroup.style('opacity', 0);
    }
    if (!data.splitLeftScaleRange) {
      this.leftAxesWidth += svgGroup.node().getBBox().width + 5;
    } else {
      this.leftAxesWidth = Math.max(this.leftAxesWidth, svgGroup.node().getBBox().width + 5);
    }
    this.getGridLinesY(data, scaleData, newAxis, trueScaleIndex);
  }

  drawRightAxis(scaleData: Model.ChartScale, data: Model.ChartData, rightScaleCount: number, rightLocationIndex: number): void {
    let translateX = this.context.dimensions.width - this.context.margin.right + this.rightAxesWidth;
    const translateY = this.context.margin.top;
    const rangeHeight = this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom;
    let axisHeights = [];
    let axisStarts = [];
    for (let i = 0; i < rightScaleCount; i++) { // default to not split
      axisHeights.push(rangeHeight);
      axisStarts.push(0);
    }
    if (rightScaleCount > 1 && data.splitRightScaleRange) {
      axisHeights = this.getRightAxisHeights(data);
      axisStarts = this.getRightAxisStarts(data, axisHeights);
      translateX = this.context.dimensions.width - this.context.margin.right;
      scaleData.scale.range([axisStarts[rightLocationIndex] + axisHeights[rightLocationIndex] -
        this.splitGutterVertical, axisStarts[rightLocationIndex]]);
    } else {
      scaleData.scale.range([rangeHeight, 0]);
    }
    const newAxis = d3.axisRight(scaleData.scale);
    if (scaleData.type === D3ScaleType.Point ||
      scaleData.type === D3ScaleType.Band) {
      this.setExplicitTickValues(newAxis, scaleData.scale, axisHeights[rightLocationIndex], scaleData.hideLabels);
    } else if (scaleData.type === D3ScaleType.Log) {
      if (scaleData.hideLabels) {
        newAxis.ticks([]);
      } else {
        newAxis.ticks(axisHeights[rightLocationIndex] / 25, d3.format('d'));
      }
    } else {
      if (scaleData.hideLabels) {
        newAxis.ticks([]);
      } else {
        newAxis.ticks(null);
      }
    }
    this.axes.push(newAxis);
    const trueScaleIndex = this.getTrueScaleIndex(data, Model.ChartAxisLocation.Right, rightLocationIndex);
    const svgGroup = this.drawAxis(newAxis, translateX, translateY, 'axis right', trueScaleIndex);
    svgGroup.style('color', this.context.fontColor);
    if ((!data.splitRightScaleRange || this.isSideScaleActive(data, trueScaleIndex)) &&
      !this.isScaleHidden(data, trueScaleIndex)) {
      svgGroup.style('opacity', 1);
    } else {
      svgGroup.style('opacity', 0);
    }
    if (!data.splitRightScaleRange) {
      this.rightAxesWidth += svgGroup.node().getBBox().width + 5;
    } else {
      this.rightAxesWidth = Math.max(this.rightAxesWidth, svgGroup.node().getBBox().width + 5);
    }
    this.getGridLinesY(data, scaleData, newAxis, trueScaleIndex);
  }

  drawAxis(axis: any, translateX: number, translateY: number, cssClasses: string, trueScaleIndex: number): any {
    const svgGroup = this.context.svg.append('g')
      .attr('transform', 'translate(0,' + translateY + ')')
      .attr('class', cssClasses);
    svgGroup.append('g')
      .attr('transform', 'translate(' + translateX + ',0)')
      .call(axis);
    svgGroup.selectAll('.tick')
      .on('click', (event: any, d: any) => {
        this.context.axisClick.emit({
          scaleIndex: trueScaleIndex,
          tickValue: d
        });
      });
    return svgGroup;
  }

  drawBottomSliders(
    svgGroup: any,
    scaleData: Model.ChartScale,
    data: Model.ChartData,
    sliderGroups: any[],
    wrapperClass: string,
    mainSliders: boolean,
    bottomScaleIndex: number
  ): void {
    const  that = this;

    // Draw handles
    const axisGroup = svgGroup.select('g');
    const chartHeight = this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom;
    // .filter(group => {
    //   return data.scales[group.xScale] === scaleData && group.sliderPointsX && group.sliderPointsX.length > 0;
    // });
    axisGroup.selectAll(`g.${wrapperClass}`).remove();
    const handlesWrap = axisGroup.selectAll(`g.${wrapperClass}`)
      .data(sliderGroups);
    const handlesWrapEnter = handlesWrap.enter().append('g')
      .attr('class', wrapperClass)
      .attr('dataGroupIndex', (d, groupIndex) => {
        return groupIndex;
      })
      .style('display', (d) => {
        return (typeof d.xScale === 'undefined' || d.xScale === bottomScaleIndex) ? '' : 'none';
      });

    const handleOpacity = (data.hideRangeSliders) ? 0 : 1;
    const dividerOpacity = (data.hideRangeDividers) ? 0 : 1;

    const handles = handlesWrapEnter.selectAll('g.handle')
      .data((d) => d.sliderPointsX || []);
    const handlesEnter = handles.enter().append('g')
      .attr('class', 'handle')
      .attr('point-index', (d, i) => {
        return i;
      })
      .attr('transform', (d) => {
        let handleX = (d) ? scaleData.scale(d) : 0;
        handleX = (typeof handleX === 'undefined') ? 0 : handleX;
        return `translate(${handleX},0)`;
      });
    handlesEnter.append('rect').attr('class', 'inner')
      .attr('x', -6)
      .attr('y', 0)
      .attr('width', 12)
      .attr('height', 20)
      .attr('fill', '#777')
      .style('opacity', handleOpacity);
    handlesEnter.append('line').attr('class', 'mark-1')
      .attr('y1', 0)
      .attr('y2', 10)
      .style('stroke', '#fff')
      .style('stroke-opacity', 1)
      .style('shape-rendering', 'unset')
      .attr('transform', 'translate(1,1)')
      .style('opacity', handleOpacity);
    handlesEnter.append('line').attr('class', 'mark-2')
      .attr('y1', 0)
      .attr('y2', 10)
      .style('stroke', '#000')
      .style('stroke-opacity', 1)
      .style('shape-rendering', 'unset')
      .style('opacity', handleOpacity);
    handlesEnter.append('rect').attr('class', 'border-1')
      .attr('x', -5)
      .attr('y', 1)
      .attr('width', 11)
      .attr('height', 19)
      .style('stroke', '#fff')
      .style('stroke-width', 1)
      .style('fill', '#fff')
      .style('fill-opacity', .5)
      .style('opacity', handleOpacity);
    handlesEnter.append('rect').attr('class', 'border-2')
      .attr('x', -6)
      .attr('y', 0)
      .attr('width', 12)
      .attr('height', 20)
      .style('stroke', '#111')
      .style('stroke-width', 1)
      .style('opacity', handleOpacity);
    handlesEnter.append('line').attr('class', 'divider')
      .attr('y1', 0)
      .attr('y2', -chartHeight)
      .style('stroke', '#777')
      .style('stroke-linecap', 'round')
      .style('stroke-dasharray', '2, 5')
      .style('stroke-opacity', 1)
      .style('shape-rendering', 'unset')
      .style('opacity', dividerOpacity);
    handles.transition()
      .attr('transform', (d) => {
        const handleX = (d) ? scaleData.scale(d) : 0;
        return `translate(${handleX},0)`;
      });

    if (!mainSliders) {
      handlesWrapEnter.each(function(p, j): void {
        d3.select(this)
          .selectAll('rect.inner')
          .attr('fill', that.context.colorScheme(j));
        d3.select(this)
          .selectAll('line.divider')
          .attr('stroke', that.context.colorScheme(j));
      });
    }

    const findRangeIndex = (rangeValues, x) => {
      let index = null;
      let midPoint;
      if (!x || x <= rangeValues[0]) {
        index = 0;
      } else if (x >= rangeValues[rangeValues.length - 1]) {
        index = rangeValues.length - 1;
      } else {
        for (let i = 0; i < rangeValues.length - 1; i++) {
          if (x >= rangeValues[i] && x <= rangeValues[i + 1]) {
            index = i;
            break;
          }
        }
        midPoint = (rangeValues[index] + rangeValues[index + 1]) / 2;
        if (x > midPoint) {
          index++;
        }
      }
      return rangeValues[index];
    };

    const getNearestPoint = (groupIndex: number, pointIndex: number, d: any, x: number) => {
      // Get main line domain
      let domainValues: any = [];
      let sliderDomain: any = [];
      if (mainSliders) {
        domainValues = scaleData.scale.domain();
        sliderDomain = scaleData.sliderPointsX || [];
      } else {
        data.groups[groupIndex].lines.some((line: Model.ChartLine) => {
          if (line.type === Model.ChartLineType.Main) {
            domainValues = line.points.map(point => point[0]);
          }
          return line.type === Model.ChartLineType.Main;
        });
        sliderDomain = data.groups[groupIndex].sliderPointsX || [];
      }
      const rangeValues = domainValues.map((val) => scaleData.scale(val));
      let searchRangeValues = rangeValues;
      const sliderRange = sliderDomain.map((val) => scaleData.scale(val));
      if (sliderDomain.length > 1) {
        if (pointIndex === 0) {
          searchRangeValues = rangeValues.filter((val) => val < sliderRange[pointIndex + 1]);
        } else if (pointIndex === sliderDomain.length - 1) {
          searchRangeValues = rangeValues.filter((val) => val > sliderRange[pointIndex - 1]);
        } else {
          searchRangeValues = rangeValues.filter((val) => val > sliderRange[pointIndex - 1] && val < sliderRange[pointIndex + 1]);
        }
      }

      const newRangeValue = findRangeIndex(searchRangeValues, x);
      let newDomainValue = domainValues[0];
      rangeValues.some((val, index) => {
        if (val === newRangeValue) {
          newDomainValue = domainValues[index];
        }
        return val === newRangeValue;
      });
      return {
        newDomainValue,
        newRangeValue
      };

      // return newDomainValue (for saving) and newRangeValue (for drawing)
    };

    const started = function(startEvent: any): void {
      const handle = d3.select(this);
      handle.classed('dragging', true);

      startEvent.on('drag', dragged).on('end', ended);

      function dragged(draggedEvent: any, d): void {
        const pointIndex = parseInt(this.attributes['point-index'].value);
        const groupIndex = parseInt(this.parentNode.attributes['dataGroupIndex'].value, 10);
        const newPoint = getNearestPoint(groupIndex, pointIndex, d, draggedEvent.x);
        const handleX = (typeof newPoint.newRangeValue === 'undefined') ? 0 : newPoint.newRangeValue;
        handle.attr('transform', `translate(${handleX},0)`);
      }

      function ended(endedEvent: any, d): void {
        const pointIndex = parseInt(this.attributes['point-index'].value);
        const groupIndex = parseInt(this.parentNode.attributes['dataGroupIndex'].value, 10);
        const newPoint = getNearestPoint(groupIndex, pointIndex, d, endedEvent.x);
        // const handleX = (typeof newPoint.newRangeValue === 'undefined') ? 0 : newPoint.newRangeValue;
        if (mainSliders) {
          scaleData.sliderPointsX[pointIndex] = newPoint.newDomainValue;
          that.context.updateSliderX.emit({groupIndex: null, pointIndex, newDomainValue: newPoint.newDomainValue});
        } else {
          data.groups[groupIndex].sliderPointsX[pointIndex] = newPoint.newDomainValue;
          that.context.updateSliderX.emit({groupIndex, pointIndex, newDomainValue: newPoint.newDomainValue});
        }
        handle.classed('dragging', false);
      }
    };

    handlesEnter.call(d3.drag().on('start', started));

  }

  redrawAxes(data: Model.ChartData): void {
    this.grid = [];
    // this.buildScaleDomains(data);
    this.redrawBottomAxes(data);
    this.redrawLeftAxes(data);
    this.redrawRightAxes(data);
    this.drawGrid();
    if (!this.context.miniChart) {
      this.redrawLabels(data);
    }
  }

  wrapText(textNode: any, bandwidth: number): void {
    textNode.each(function(): void {
      const text = d3.select(this);
      if (text.data() && text.data()[0]) {
        const words = (text.data()[0].toString()).split(new RegExp('[ ]', 'g')).reverse();
        // const words = 'This should get wrapped around. It\'s too long.'.split(new RegExp('[-&/ ]', 'g')).reverse();
        let word: string = null;
        let splitPos = 0;
        let keepChars = '';
        let line = [];
        let lineNumber = 0;
        const lineHeight = 1.1;
        const y = text.attr('y');
        const dy = parseFloat(text.attr('dy'));
        text.style('display', 'unset');
        let tspan = text.text(null).append('tspan').attr('x', 0).attr('y', y).attr('dy', dy + 'em');
        let tlength = 0;
        // eslint-disable-next-line no-cond-assign
        while (word = words.pop()) {
          line.push(word);
          tspan.text(line.join(' '));
          tlength = (tspan.node() as any).getComputedTextLength();
          if (tlength > bandwidth) {
            if (line.length > 1) {
              line.pop();
              tspan.text(line.join(' '));
              line = [word];
            } else {
              // Split the word; Someday add split at &, - or /
              splitPos = (bandwidth / tlength) * word.length - 1;
              keepChars = word.substring(0, (bandwidth / tlength) * word.length - 1);
              words.push(word.substring(splitPos));
              tspan.text(keepChars);
              line = [];
            }
            tspan = text.append('tspan').attr('x', 0).attr('y', y).attr('dy', `${++lineNumber * lineHeight + dy}em`).text(line.join(' '));
          }
        }
        const wrappedNode = text.node().cloneNode();
        wrappedNode.classList.add('wrapped');
        Array.from(text.node().children).forEach(tspanChild => {
          wrappedNode.appendChild(tspanChild);
        });
        text.node().parentNode.appendChild(wrappedNode);
        text.style('display', 'none');
      }
    });
  }

  labelWrapperBottom(svgGroup: any, scaleData: any, axisWidth: number): void {
    const tickGroups = svgGroup.selectAll('g.tick');
    const gapCount = Math.floor(axisWidth / 80);
    let bandwidth = axisWidth / gapCount;
    if (bandwidth > 150) {
      bandwidth = 150;
    }
    if (bandwidth > this.context.dimensions.width) {
      bandwidth = this.context.dimensions.width;
    }
    tickGroups.selectAll('text.wrapped').remove();
    tickGroups.selectAll('text')
      .call(this.wrapText, bandwidth);
  }

  redrawBottomAxes(data: Model.ChartData): void {
    let bottomAxesHeight = 0;
    let bottomScaleIndex = -1;
    const bottomScaleCount = this.getScaleCount(data, Model.ChartAxisLocation.Bottom);
    const svgGroups = d3.select(this.context.svg).node().selectAll('g.axis.bottom');
    data.scales.forEach((scaleData, idx) => {
      if (scaleData.location === Model.ChartAxisLocation.Bottom) {
        let scaleDomain = scaleData.domain;
        if (!scaleData.lockDomain) {
          if (scaleData.domainCustom) {
            scaleDomain = scaleData.domainCustom;
          } else {
            scaleDomain = this.buildScaleDomain(data, scaleData, idx);
          }
        }
        scaleData.scale.domain(scaleDomain);
        bottomScaleIndex += 1;
        const bottomRangeWidth = this.context.dimensions.width - this.context.margin.left - this.context.margin.right;
        const translateX = this.context.margin.left;
        let translateY = this.context.dimensions.height - this.context.margin.bottom + bottomAxesHeight;
        let axisWidths = [];
        let axisStarts = [];
        for (let i = 0; i < bottomScaleCount; i++) {
          axisWidths.push(bottomRangeWidth);
          axisStarts.push(0);
        }
        if (bottomScaleCount > 1 && (data.splitBottomScaleRange || data.singleLegendMode)) {
          axisWidths = this.getBottomAxisWidths(data);
          axisStarts = this.getBottomAxisStarts(data, axisWidths);
          translateY = this.context.dimensions.height - this.context.margin.bottom;
          scaleData.scale.range([axisStarts[bottomScaleIndex], axisStarts[bottomScaleIndex] + axisWidths[bottomScaleIndex]]);
        } else {
          scaleData.scale.range([0, bottomRangeWidth]);
        }
        const axis = this.axes[idx];
        axis.scale(scaleData.scale);
        if (scaleData.type === D3ScaleType.Point ||
          scaleData.type === D3ScaleType.Band) {
          this.setExplicitTickValues(axis, scaleData.scale, axisWidths[bottomScaleIndex], scaleData.hideLabels);
        } else if (scaleData.type === D3ScaleType.Log) {
          if (scaleData.hideLabels) {
            axis.ticks([]);
          } else {
            axis.tickFormat(d3.format('d'));
          }
        } else {
          if (scaleData.hideLabels) {
            axis.ticks([]);
          } else {
            if (scaleData.tickValues && scaleData.tickFormat) {
              axis.scale(scaleData.scale)
              .tickValues(scaleData.tickValues)
              .tickFormat((d, i) => scaleData.tickFormat[i]);    
            } else {
              axis.ticks(null);
            }
          }
        }
        const svgGroup = d3.select(svgGroups.nodes()[bottomScaleIndex]);
        svgGroup.style('color', this.context.fontColor);
        const trueScaleIndex = this.getTrueScaleIndex(data, Model.ChartAxisLocation.Bottom, bottomScaleIndex);
        if (this.isBottomScaleActive(data, trueScaleIndex) &&
          !this.isScaleHidden(data, trueScaleIndex)) {
          svgGroup.style('opacity', 1);
        } else {
          svgGroup.style('opacity', 0);
        }
        this.redrawAxis(axis, svgGroup, translateX, translateY);
        this.labelWrapperBottom(svgGroup, scaleData, axisWidths[bottomScaleIndex]);

        if (this.isBottomScaleActive(data, bottomScaleIndex)) {
          bottomAxesHeight += svgGroup.node().getBBox().height + 5;
        }
        this.getGridLinesX(data, scaleData, axis, trueScaleIndex);

        this.drawBottomSliders(svgGroup,
          scaleData,
          data,
          data.groups.map(group => (group.disabled) ? new Model.ChartGroup() : group),
          'handlesWrap',
          false,
          bottomScaleIndex);
        if (!data.splitBottomScaleRange) {
          // Not rendering the overall drag range across multiple bottom scales
          this.drawBottomSliders(svgGroup, scaleData, data, [scaleData], 'handlesMainWrap', true, bottomScaleIndex);
        }
      }
    });
  }

  redrawLeftAxes(data: Model.ChartData): void {
    let leftAxesWidth = 0;
    let leftScaleIndex = -1;
    const leftScaleCount = this.getScaleCount(data, Model.ChartAxisLocation.Left);
    const svgGroups = d3.select(this.context.svg).node().selectAll('g.axis.left');
    data.scales.forEach((scaleData, idx) => {
      if (scaleData.location === Model.ChartAxisLocation.Left) {
        let scaleDomain = scaleData.domain;
        if (!scaleData.lockDomain) {
          if (scaleData.domainCustom) {
            scaleDomain = scaleData.domainCustom;
          } else {
            scaleDomain = this.buildScaleDomain(data, scaleData, idx);
          }
        }
        scaleData.scale.domain(scaleDomain);
        leftScaleIndex += 1;
        let translateX = this.context.margin.left - leftAxesWidth;
        const translateY = this.context.margin.top;
        const rangeHeight = this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom;
        let axisHeights = [];
        let axisStarts = [];
        for (let i = 0; i < leftScaleCount; i++) { // default to not split
          axisHeights.push(rangeHeight);
          axisStarts.push(0);
        }
        if (leftScaleCount > 1 && data.splitLeftScaleRange) {
          axisHeights = this.getLeftAxisHeights(data);
          axisStarts = this.getLeftAxisStarts(data, axisHeights);
          translateX = this.context.margin.left;
          scaleData.scale.range([axisStarts[leftScaleIndex] + axisHeights[leftScaleIndex] -
            this.splitGutterVertical, axisStarts[leftScaleIndex]]);
        } else {
          scaleData.scale.range([rangeHeight, 0]);
        }
        const axis = this.axes[idx];
        axis.scale(scaleData.scale);
        if (scaleData.type === D3ScaleType.Point ||
          scaleData.type === D3ScaleType.Band) {
          this.setExplicitTickValues(axis, scaleData.scale, axisHeights[leftScaleIndex], scaleData.hideLabels);
        } else if (scaleData.type === D3ScaleType.Log) {
          if (scaleData.hideLabels) {
            axis.ticks([]);
          } else {
            axis.ticks(axisHeights[leftScaleIndex] / 25, d3.format('d'));
          }
        } else {
          if (scaleData.hideLabels) {
            axis.ticks([]);
          } else {
            if (scaleData.tickValues && scaleData.tickFormat) {
              axis.scale(scaleData.scale)
              .tickValues(scaleData.tickValues)
              .tickFormat((d, i) => scaleData.tickFormat[i]);    
            } else {
              axis.ticks(null);
            }
          }
        }
        const group = d3.select(svgGroups.nodes()[leftScaleIndex]);
        group.style('color', this.context.fontColor);
        const trueScaleIndex = this.getTrueScaleIndex(data, Model.ChartAxisLocation.Left, leftScaleIndex);
        if ((!data.splitLeftScaleRange || this.isSideScaleActive(data, trueScaleIndex)) &&
          !this.isScaleHidden(data, trueScaleIndex)) {
          group.style('opacity', 1);
        } else {
          group.style('opacity', 0);
        }
        this.redrawAxis(axis, group, translateX, translateY);
        if (!data.splitLeftScaleRange) {
          leftAxesWidth += group.node().getBBox().width + 5;
        } else {
          leftAxesWidth = Math.max(this.leftAxesWidth, group.node().getBBox().width + 5);
        }
        this.getGridLinesY(data, scaleData, axis, trueScaleIndex);
      }
    });
  }

  redrawRightAxes(data: Model.ChartData): void {
    let rightAxesWidth = 0;
    let rightScaleIndex = -1;
    const rightScaleCount = this.getScaleCount(data, Model.ChartAxisLocation.Right);
    const svgGroups = d3.select(this.context.svg).node().selectAll('g.axis.right');
    data.scales.forEach((scaleData, idx) => {
      if (scaleData.location === Model.ChartAxisLocation.Right) {
        let scaleDomain = scaleData.domain;
        if (!scaleData.lockDomain) {
          if (scaleData.domainCustom) {
            scaleDomain = scaleData.domainCustom;
          } else {
            scaleDomain = this.buildScaleDomain(data, scaleData, idx);
          }
        }
        scaleData.scale.domain(scaleDomain);
        rightScaleIndex += 1;
        let translateX = this.context.dimensions.width - this.context.margin.right + rightAxesWidth;
        const translateY = this.context.margin.top;
        const rangeHeight = this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom;
        let axisHeights = [];
        let axisStarts = [];
        for (let i = 0; i < rightScaleCount; i++) { // default to not split
          axisHeights.push(rangeHeight);
          axisStarts.push(0);
        }
        if (rightScaleCount > 1 && data.splitRightScaleRange) {
          axisHeights = this.getRightAxisHeights(data);
          axisStarts = this.getRightAxisStarts(data, axisHeights);
          translateX = this.context.dimensions.width - this.context.margin.right;
          scaleData.scale.range([axisStarts[rightScaleIndex] + axisHeights[rightScaleIndex] -
            this.splitGutterVertical, axisStarts[rightScaleIndex]]);
        } else {
          scaleData.scale.range([rangeHeight, 0]);
        }
        const axis = this.axes[idx];
        axis.scale(scaleData.scale);
        if (scaleData.type === D3ScaleType.Point ||
          scaleData.type === D3ScaleType.Band) {
          this.setExplicitTickValues(axis, scaleData.scale, axisHeights[rightScaleIndex], scaleData.hideLabels);
        } else if (scaleData.type === D3ScaleType.Log) {
          if (scaleData.hideLabels) {
            axis.ticks([]);
          } else {
            axis.ticks(axisHeights[rightScaleIndex] / 25, d3.format('d'));
          }
        } else {
          if (scaleData.hideLabels) {
            axis.ticks([]);
          } else {
            if (scaleData.tickValues && scaleData.tickFormat) {
              axis.scale(scaleData.scale)
              .tickValues(scaleData.tickValues)
              .tickFormat((d, i) => scaleData.tickFormat[i]);    
            } else {
              axis.ticks(null);
            }
          }
        }
        const group = d3.select(svgGroups.nodes()[rightScaleIndex]);
        group.style('color', this.context.fontColor);
        const trueScaleIndex = this.getTrueScaleIndex(data, Model.ChartAxisLocation.Right, rightScaleIndex);
        if ((!data.splitRightScaleRange || this.isSideScaleActive(data, trueScaleIndex)) &&
          !this.isScaleHidden(data, trueScaleIndex)) {
          group.style('opacity', 1);
        } else {
          group.style('opacity', 0);
        }
        this.redrawAxis(axis, group, translateX, translateY);
        if (!data.splitRightScaleRange) {
          rightAxesWidth += group.node().getBBox().width + 5;
        } else {
          rightAxesWidth = Math.max(this.rightAxesWidth, group.node().getBBox().width + 5);
        }
        this.getGridLinesY(data, scaleData, axis, trueScaleIndex);
      }
    });
  }

  redrawAxis(axis: any, group: any, translateX: number, translateY: number): void {
    group
      .transition()
      .attr('transform', 'translate(0,' + translateY + ')');
    group.select('g')
      .transition()
      .attr('transform', 'translate(' + translateX + ',0)')
      .call(axis);
  }

  setLabels(data: Model.ChartData): void {
    this.bottomLabels = [];
    this.leftLabels = [];
    this.rightLabels = [];
    data.scales.forEach((scaleData, scaleIndex) => {
      if (scaleData.label && scaleData.label > '') {
        switch (scaleData.location) {
          case Model.ChartAxisLocation.Bottom:
            if (this.isBottomScaleActive(data, scaleIndex)) {
              if (!(data.splitBottomScaleRange && this.bottomLabels.includes(scaleData.label))) {
                this.bottomLabels.push(scaleData.label);
              }
            }
            break;
          case Model.ChartAxisLocation.Left:
            if (this.isSideScaleActive(data, scaleIndex)) {
              if (!(data.splitLeftScaleRange && this.leftLabels.includes(scaleData.label))) {
                this.leftLabels.push(scaleData.label);
              }
            }
            break;
          case Model.ChartAxisLocation.Right:
            if (this.isSideScaleActive(data, scaleIndex)) {
              if (!(data.splitRightScaleRange && this.rightLabels.includes(scaleData.label))) {
                this.rightLabels.push(scaleData.label);
              }
            }
            break;
        }
      }
    });
  }

  drawLabels(data: Model.ChartData): void {
    this.setLabels(data);
    this.textTransformRatio = 1;
    if (this.bottomLabels.length > 0) {
      const bottomLabel = this.bottomLabels.join(' / ');
      const svgGroup = this.context.svg.append('g')
        .attr('class', 'axis label bottom');
      const group = svgGroup.selectAll('text.axisLabel')
        .data([bottomLabel]);
      group.enter().append('text')
        .attr('class', 'axisLabel')
        .attr('text-anchor', 'middle')
        .style('font-size', this.context.axisFontSize)
        .text(d => d);
      const bottomX = (this.context.dimensions.width - this.context.margin.right) / 2 + (this.context.margin.left / 2);
      const bottomY = this.context.dimensions.height - this.context.margin.bottom + this.bottomAxesHeight - 10;
      svgGroup.attr('transform', `translate(${bottomX},${bottomY}) scale(${this.textTransformRatio})`);
      if (svgGroup.node()) {
        this.bottomAxesHeight += svgGroup.node().getBBox().height + 10;
        if (svgGroup.node().getBBox().width > this.context.dimensions.width - this.context.margin.left - this.context.margin.right) {
          this.reduceFontSize(svgGroup, this.context.dimensions.width - this.context.margin.left - this.context.margin.right);
        }
      }
    }
    if (this.leftLabels.length > 0) {
      const leftLabel = this.leftLabels.reverse().join(' / ');
      const svgGroup = this.context.svg.append('g')
        .attr('class', 'axis label left');
      const group = svgGroup.selectAll('text.axisLabel')
        .data([leftLabel]);
      group.enter().append('text')
        .attr('class', 'axisLabel')
        .attr('transform', 'rotate(-90)')
        .attr('text-anchor', 'middle')
        .style('font-size', this.context.axisFontSize)
        .text(d => d);
      const leftX = this.context.margin.left - this.leftAxesWidth + svgGroup.node().getBBox().width;
      const leftY = (this.context.dimensions.height - this.context.margin.bottom) / 2 + (this.context.margin.top / 2);
      svgGroup.attr('transform', `translate(${leftX},${leftY}) scale(${this.textTransformRatio})`);
      if (svgGroup.node()) {
        this.leftAxesWidth += svgGroup.node().getBBox().width + 10;
        if (svgGroup.node().getBBox().height > this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom) {
          this.reduceFontSize(svgGroup, this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom);
        }
      }
    }
    if (this.rightLabels.length > 0) {
      const rightLabel = this.rightLabels.join(' / ');
      const svgGroup = this.context.svg.append('g')
        .attr('class', 'axis label right');
      const group = svgGroup.selectAll('text.axisLabel')
        .data([rightLabel]);
      group.enter().append('text')
        .attr('class', 'axisLabel')
        .attr('transform', 'rotate(90)')
        .attr('text-anchor', 'middle')
        .style('font-size', this.context.axisFontSize)
        .text(d => d);
      if (svgGroup.node()) {
        const rightX = this.context.dimensions.width - this.context.margin.right + this.rightAxesWidth - svgGroup.node().getBBox().width;
        const rightY = (this.context.dimensions.height - this.context.margin.bottom) / 2 + (this.context.margin.top / 2);
        svgGroup.attr('transform', `translate(${rightX},${rightY}) scale(${this.textTransformRatio})`);
        this.rightAxesWidth += svgGroup.node().getBBox().width + 10;
        if (svgGroup.node().getBBox().height > this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom) {
          this.reduceFontSize(svgGroup, this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom);
        }
      }
    }
  }

  redrawLabels(data: Model.ChartData): void {
    this.setLabels(data);
    this.textTransformRatio = 1;
    this.context.svg.selectAll('g.axis.label').attr('font-size', null);
    if (this.bottomLabels.length > 0) {
      const bottomLabel = this.bottomLabels.join(' / ');
      const svgGroup = this.context.svg.select('g.axis.label.bottom');
      const group = svgGroup.selectAll('text.axisLabel')
        .data([bottomLabel]);
      group.exit().remove();
      group.transition()
        .text(d => d);
      const bottomX = (this.context.dimensions.width - this.context.margin.right) / 2 + (this.context.margin.left / 2);
      const bottomY = this.context.dimensions.height - this.context.margin.bottom + this.bottomAxesHeight - 10;
      svgGroup.attr('transform', `translate(${bottomX},${bottomY}) scale(${this.textTransformRatio})`);
      if (svgGroup.node() &&
        svgGroup.node().getBBox().width > this.context.dimensions.width - this.context.margin.left - this.context.margin.right) {
        this.reduceFontSize(svgGroup, this.context.dimensions.width - this.context.margin.left - this.context.margin.right);
      }
    }
    if (this.leftLabels.length > 0) {
      const leftLabel = this.leftLabels.reverse().join(' / ');
      const svgGroup = this.context.svg.select('g.axis.label.left');
      const group = svgGroup.selectAll('text.axisLabel')
        .data([leftLabel]);
      group.exit().remove();
      group.transition()
        .text(d => d);
      if (svgGroup.node()) {
        const leftX = this.context.margin.left - this.leftAxesWidth + svgGroup.node().getBBox().width;
        const leftY = (this.context.dimensions.height - this.context.margin.bottom) / 2 + (this.context.margin.top / 2);
        svgGroup.attr('transform', `translate(${leftX},${leftY}) scale(${this.textTransformRatio})`);
        if (svgGroup.node().getBBox().height > this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom) {
          this.reduceFontSize(svgGroup, this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom);
        }
      }
    }
    if (this.rightLabels.length > 0) {
      const rightLabel = this.rightLabels.join(' / ');
      const svgGroup = this.context.svg.select('g.axis.label.right');
      const group = svgGroup.selectAll('text.axisLabel')
        .data([rightLabel]);
      group.exit().remove();
      group.transition()
        .text(d => d);
      if (svgGroup.node()) {
        const rightX = this.context.dimensions.width - this.context.margin.right + this.rightAxesWidth - svgGroup.node().getBBox().width;
        const rightY = (this.context.dimensions.height - this.context.margin.bottom) / 2 + (this.context.margin.top / 2);
        svgGroup.attr('transform', `translate(${rightX},${rightY}) scale(${this.textTransformRatio})`);
        if (svgGroup.node().getBBox().height > this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom) {
          this.reduceFontSize(svgGroup, this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom);
        }
      }
    }
  }

  setExplicitTickValues(axis: any, scale: any, axisWidth: number, hideLabels = false): void {
    if (axisWidth === Infinity) {
      axisWidth = this.context.dimensions.width - this.context.margin.left - this.context.margin.right;
    }
    const tickValues = [];
    if (!hideLabels) {
      const tickCount = Math.ceil(axisWidth / 80);
      const tickGap = Math.ceil(scale.domain().length / tickCount);
      for (let i = 0; i < scale.domain().length; i = i + tickGap) {
        tickValues.push(scale.domain()[i]);
      }
    }
    axis.tickValues(tickValues);
  }

  getTickValues(scaleData: Model.ChartScale, axis: any): any {
    if (axis.tickValues()) {
      return axis.tickValues();
    }
    return scaleData.scale.ticks();
  }

  getGridLinesX(data: Model.ChartData, scaleData: Model.ChartScale, axis: any, trueScaleIndex: number): void {
    if (scaleData.grid && this.isBottomScaleActive(data, trueScaleIndex)) {
      const lineLength = this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom;
      let shift = 0;
      if (scaleData.type === D3ScaleType.Point ||
        scaleData.type === D3ScaleType.Band) {
        shift = (scaleData.scale.bandwidth() / 2);
      }
      const tickValues = this.getTickValues(scaleData, axis);
      tickValues.forEach(tick => {
        const x = scaleData.scale(tick) + shift;
        this.grid.push({
          points: [[x, 0], [x, lineLength]]
        });
      });
    }
  }

  getGridLinesY(data: Model.ChartData, scaleData: Model.ChartScale, axis: any, trueScaleIndex: number): void {
    if (scaleData.grid && this.isSideScaleActive(data, trueScaleIndex)) {
      const lineLength = this.context.dimensions.width - this.context.margin.left - this.context.margin.right;
      let shift = 0;
      if (scaleData.type === D3ScaleType.Point ||
        scaleData.type === D3ScaleType.Band) {
        shift = (scaleData.scale.bandwidth() / 2);
      }
      const tickValues = this.getTickValues(scaleData, axis);
      tickValues.forEach(tick => {
        const y = scaleData.scale(tick) + shift;
        this.grid.push({
          points: [[0, y], [lineLength, y]]
        });
      });
    }
  }

  drawGrid(): void {
    const gridWrap = d3.select(this.context.svg).node().selectAll('g.gridWrap')
      .data([this.grid]);
    gridWrap.enter().append('g')
      .attr('class', 'gridWrap')
      .attr('transform', `translate(${this.context.margin.left},${this.context.margin.top})`);
    gridWrap.transition()
      .attr('transform', `translate(${this.context.margin.left},${this.context.margin.top})`);

    const lines = gridWrap.selectAll('.grid-line')
      .data(this.grid);
    lines.enter().append('g')
      .style('stroke-opacity', 1)
      .attr('class', 'grid-line');
    lines.exit().transition()
      .style('stroke-opacity', 1e-6)
      .remove();
    lines.transition()
      .style('stroke-opacity', 1);

    const paths = gridWrap.selectAll('.grid-line').selectAll('path')
      .data((d: any) => [d.points]);
    paths.enter().append('path')
      .style('fill', 'none')
      .style('stroke', this.context.fontColor)
      .style('stroke-width', .1)
      .attr('d', d3.line()
        .x((d: any) => d[0])
        .y((d: any) => d[1])
      );
    paths.exit().remove();
    paths.transition()
      .style('stroke', this.context.fontColor)
      .attr('d', d3.line()
        .x((d: any) => d[0])
        .y((d: any) => d[1])
      );
  }

  reduceFontSize(svgGroup: any, max: number): void {
    if (svgGroup.node()) {
      const box = svgGroup.node().getBBox();
      if ((box.height > 0 || box.width > 0) &&
        (box.height > max || box.width > max)) {
        const newTransformRatio = max / Math.max(box.height, box.width);
        this.context.svg.selectAll('g.axis.label').nodes().forEach((node) => {
          const transform = node.attributes['transform'].value;
          const newValue = transform
            .replace(`scale(${this.textTransformRatio})`, `scale(${newTransformRatio})`);
          node.attributes['transform'].value = newValue;
        });
        this.textTransformRatio = newTransformRatio;
      }
    }
  }

  getThresholdLines(data: Model.ChartData): any[] {
    const thresholdLines: any[] = [];
    data.scales.forEach((scaleData: Model.ChartScale, trueScaleIndex) => {
      if ((scaleData.location === Model.ChartAxisLocation.Left || scaleData.location === Model.ChartAxisLocation.Right) &&
        scaleData.thresholds && scaleData.thresholds.length > 0 && this.isSideScaleActive(data, trueScaleIndex)) {
        const lineLength = this.context.dimensions.width - this.context.margin.left - this.context.margin.right;
        scaleData.thresholds.forEach(threshold => {
          // precalculate the line and push it to the stack
          const y = scaleData.scale(threshold.value);
          thresholdLines.push({
            color: threshold.color,
            x1: 0,
            y1: y,
            x2: lineLength,
            y2: y
          });
        });
      }
    });
    return thresholdLines;
  }

  drawThresholds(data: Model.ChartData): void {
    const lines = this.getThresholdLines(data);

    // Add wrapper for the threshold lines
    const thresholdWrap = d3.select(this.context.svg).node().selectAll('g.thresholdWrap')
      .data([lines]);
    thresholdWrap.enter().append('g')
      .attr('class', 'thresholdWrap')
      .attr('transform', `translate(${this.context.margin.left},${this.context.margin.top})`);
    thresholdWrap.transition()
      .attr('transform', `translate(${this.context.margin.left},${this.context.margin.top})`);

    // Add individual lines
    const tethers = thresholdWrap.selectAll('.threshold')
      .data(lines);
    tethers.enter().append('g')
      .style('stroke-opacity', 1)
      .style('fill', (d) => d.color)
      .style('stroke', (d) => d.color)
      .attr('class', (d, i) => 'threshold threshold-' + i);
    tethers.exit().transition()
      .style('stroke-opacity', 1e-6)
      .remove();
    tethers
      .style('fill', (d) => d.color)
      .style('stroke', (d) => d.color)
      .style('stroke-opacity', 1);

    // Add svg paths to the tether
    const paths = thresholdWrap.selectAll('.threshold').selectAll('path')
      .data((d: any) => {
        return [d];
      });
    paths.enter().append('path')
      .attr('d', (d) => {
        return `M${d.x1},${d.y1}L${d.x2},${d.y2}`;
      });
    paths.exit().remove();
    paths
      .attr('d', (d) => {
        return `M${d.x1},${d.y1}L${d.x2},${d.y2}`;
      });
  }

  getEventLines(data: Model.ChartData): any[] {
    const eventLines: any[] = [];
    data.scales.forEach((scaleData: Model.ChartScale, trueScaleIndex) => {
      if (scaleData.location === Model.ChartAxisLocation.Bottom &&
        scaleData.events && scaleData.events.length > 0 && this.isBottomScaleActive(data, trueScaleIndex)) {
        const lineLength = this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom;
        scaleData.events.forEach(event => {
          // precalculate the line and push it to the stack
          const x = scaleData.scale(event.value);
          eventLines.push({
            color: event.color,
            x1: x,
            y1: 0,
            x2: x,
            y2: lineLength
          });
        });
      }
    });
    return eventLines;
  }

  drawEvents(data: Model.ChartData): void {
    const lines = this.getEventLines(data);

    // Add wrapper for the threshold lines
    const eventWrap = d3.select(this.context.svg).node().selectAll('g.eventWrap')
      .data([lines]);
    eventWrap.enter().append('g')
      .attr('class', 'eventWrap')
      .attr('transform', `translate(${this.context.margin.left},${this.context.margin.top})`);
    eventWrap.transition()
      .attr('transform', `translate(${this.context.margin.left},${this.context.margin.top})`);

    // Add individual lines
    const tethers = eventWrap.selectAll('.event')
      .data(lines);
    tethers.enter().append('g')
      .style('stroke-opacity', 1)
      .style('fill', (d) => d.color)
      .style('stroke', (d) => d.color)
      .attr('class', (d, i) => 'event event-' + i);
    tethers.exit().transition()
      .style('stroke-opacity', 1e-6)
      .remove();
    tethers
      .style('fill', (d) => d.color)
      .style('stroke', (d) => d.color)
      .style('stroke-opacity', 1);

    // Add svg paths to the tether
    const paths = eventWrap.selectAll('.event').selectAll('path')
      .data((d: any) => {
        return [d];
      });
    paths.enter().append('path')
      .attr('d', (d) => {
        return `M${d.x1},${d.y1}L${d.x2},${d.y2}`;
      });
    paths.exit().remove();
    paths
      .attr('d', (d) => {
        return `M${d.x1},${d.y1}L${d.x2},${d.y2}`;
      });  }
}
