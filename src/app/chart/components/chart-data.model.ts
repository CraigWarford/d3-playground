import { D3ScaleType, ChartTitleAlign } from './enums';

export enum ChartAxisLocation {
  Bottom = 0,
  Left = 1,
  Right = 2
}

export enum ChartLineType {
  Main = 0,
  UCL = 1,
  LCL = 2,
  Mean = 3,
  Count = 4,
  Ghost = 5,
  CL = 6
}

export const ChartLineTypeClasses = [
  'main',
  'ucl',
  'lcl',
  'mean',
  'count',
  'ghost',
  'cl'
];

export class ChartThreshold {
  constructor(
    public value: any = null,
    public label = '',
    public color = '#777',
    public boxLeftPercent = 100,
    public clientWidth = 25,
    public clientHeight = 25
  ) {}
}

export class ChartEvent {
  constructor(
    public value: any = null,
    public label = '',
    public color = '#777',
  ) {}
}

export class ChartScale {
  constructor(
    public label = '',
    public location = ChartAxisLocation.Bottom,
    public type: D3ScaleType = null,
    public scale: any = null,
    public grid = false,
    public domain: any[] = [],
    public domainAll: any[] = [],
    public domainCustom: any[] = null,
    public lockDomain = false,
    public sliderPointsX: any = [],
    public sliderPointsY: any = [],
    public hideLabels = false,
    public hideScale = false,
    public forceDisplay = false,
    public thresholds: ChartThreshold[] = [],
    public events: ChartEvent[] = [],
    public tickValues: any = [],
    public tickFormat: any = []
  ) {}
}

export class ChartBar {
  constructor(
    public showAltLabel = false,
    public displayAsPercentage = false,
    public points: any = [],
    public selectXValues: any[] = [],
    public maxBarWidth = 0
  ) {}
}

export class ChartBox {
  constructor(
    public points: any = [],
    public hideLabels = false,
    public hideOutlierLabels = false,
    public selectXValues: any[] = [],
    public colorXValues: any = {},
    public maxBoxWidth = 0
  ) {}
}

export class ChartPie {
  constructor(
    public values: any[] = []
  ) {}
}

export class ChartLine {
  constructor(
    public type = ChartLineType.Main,
    public label = '',
    public points: any = [],
    public pointFilters: any = [],
    public visible = true,
    public ghostFirstPoint = false,
    public ghostLastPoint = false,
    public blockLine = false,
    public noLine = false,
    public noPoints = false,
    public color = '',
    public fill = false,
    public fillOpacity = '1',
    public tails: any = [],
    public allowNullValues = false,
    public colorXValues: any = {}
  ) {}
}

export const DefaultBoxOffsetLeft = 0;
export const DefaultBoxOffsetTop = -30;

export class ChartNote {
  constructor(
    public boxOffsetLeft = 100,
    public boxOffsetTop = 100,
    public clientWidth = 25,
    public clientHeight = 25,
    public details = '',
    public lockToXAxis = false,
    public lockToYAxis = false,
    public pointIndex = 0,
    public scaleX = 0,
    public scaleY = 0,
    public title = '',
    public point: any = [],
    public shiftLocked = false,
    public showPointData = false
  ) {}
}

export class ChartGroup {
  constructor(
    public label = '',
    public disabled = false,
    public xScale = 0,
    public yScale = 1,
    public bars: ChartBar = null,
    public boxes: ChartBox = null,
    public lines: ChartLine[] = [],
    public pie: ChartPie = null,
    public notes: ChartNote[] = [],
    public sliderPointsX: any = [],
    public sliderPointsY: any = [],
    public color = ''
  ) {}
}

export class ChartData {
  constructor(
    public title = '',
    public subtitle = '',
    public titleAlign = ChartTitleAlign.Left,
    public showLegend = true,
    public decimals = 4,
    public scales: ChartScale[] = [],
    public groups: ChartGroup[] = [],
    public sharedMeanPoints: any[] = null,
    public sharedMeanGroup: number = null,
    public splitBottomScaleRange = false,
    public splitLeftScaleRange = false,
    public splitRightScaleRange = false,
    public xScaleGutter = 20,
    public yScaleGutter = 20,
    public singleLegendMode = false,
    public groupOrder: number[] = [],
    public hideBarLabels = false,
    public hideRangeSliders = false,
    public hideRangeDividers = false
  ) {}
}
