import * as d3 from 'd3';
import * as Model from './chart-data.model';

export class D3Pie {
  context: any;

  constructor(context: any) {
    this.context = context; // d3 will choke if svg is not referenced through the original context
  }

  clearPie() {
    d3.select(this.context.svg).node().selectAll('g.pieWrap').remove();
  }

  drawPie(data: Model.ChartData) {
    const that = this;
    let visibleGroupCount = 0;
    let visibleIndex = 0;
    data.groups.forEach(group => {
      if (!group.disabled) {
        visibleGroupCount += 1;
      }
    });

    data.groups.forEach((group, groupIndex) => {
      if (!group.disabled) {
        visibleIndex += 1;
      }

      const pieHorzMargin = 40;
      const pieVertMargin = 20;
      const width = this.context.dimensions.width / visibleGroupCount;
      const drawWidth = width - (pieHorzMargin * 2);
      const height = this.context.dimensions.height - this.context.margin.top;
      const drawHeight = height - (pieVertMargin * 2);

      const radius = Math.min(drawWidth, drawHeight) / 2;

      const left = width * (visibleIndex - 1);
      const pieCenterX = left + width / 2 - this.context.margin.left;
      const pieCenterY = height / 2;

      // 276.25

      const arc = d3.arc()
        .outerRadius(radius - 10)
        .innerRadius(0);
      
      const labelArc = d3.arc()
        .outerRadius(radius)
        .innerRadius(radius);

      const d3Group = d3.select(this.context.svg).node().selectAll(`g.group-wrap-${groupIndex}`);

      const pieWrap = d3Group.selectAll('g.pieWrap').data([group]);
      pieWrap.enter().append('g')
        .attr('class', 'pieWrap')
        .style('opacity', d => (d.disabled) ? '0': '1')
        .attr('font-size', this.context.labelFontSize)
        .attr('transform', 'translate(' + pieCenterX + ',' + pieCenterY + ')');
      pieWrap.transition()
        .style('opacity', d => (d.disabled) ? '0': '1')
        .attr('transform', 'translate(' + pieCenterX + ',' + pieCenterY + ')');
      pieWrap.exit().remove();

      pieWrap.selectAll('text').remove();
      const arcValues = group.pie.values.map(point => point[1]);
      // const arcData = d3.pie().sort(null)(arcValues);
      const arcData = d3.pie()(arcValues);

      const paths = pieWrap.selectAll('path')
        .data(arcData);
      paths.enter().append('g')
        .attr('class', 'arc')
        .on('mouseover', function(): void {
          d3.select(this).classed('hover', true);
        })
        .on('mouseout', function(): void {
          d3.select(this).classed('hover', false);
        })
        .append('path')
        .data(arcData)
        .attr('d', (d) => {
          return arc(d);
        })
        .style('fill', function(d, i) {
          this._arc = d;
          return that.context.colors[i % 10];
        });
      paths.exit().remove();
      paths.transition()
        .duration(250)
        .attr('d', (d) => {
          return arc(d);
        })
        .style('fill', function(d, i) {
          this._arc = d;
          return that.context.colors[i % 10];
        })
        .attrTween('d', function(d) {
          const i = d3.interpolateObject(this._arc, d);
          this._arc = i(0);
          return function(t) {
            return arc(i(t));
          };
        });

      let lastLabelY = -1000;
      const arcs = pieWrap.selectAll('.arc').data(arcData);
      arcs.exit().remove();
      arcs.append('text')
        .attr('transform', d => {
          return 'translate(' + labelArc.centroid(d) + ')';
        })
        .attr('text-anchor', d => {
          if (labelArc.centroid(d)[0] < 0) {
            return 'end';
          }
          return 'start';
        })
        .attr('dy', '.35em')
        .style('opacity', d => {
          let opacity = '1';
          const thisLabelY = labelArc.centroid(d)[1];
          if (Math.abs(thisLabelY - lastLabelY) < (parseInt(this.context.labelFontSize) * 2)) {
            opacity = '0';
          }
          lastLabelY = thisLabelY;
          return opacity;
        })
        .text((d, i) => {
          return group.pie.values[i][0];
        });

    });
  }

}
