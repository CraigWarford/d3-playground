export enum D3ScaleType {
  Band, // Primarily for bar charts (discrete => discrete)
  Linear, // Simple mapping of numeric values (continuous => continuous)
  Log, // scale using logarithmic values (continuous => continuous)
  Ordinal, // scale using specific values for domain and range (discrete => discrete)
  Point, // Equal spacing for all values (discrete => discrete)
  Quantile, // divvys the domain scale based on quantiles for the whole set (continuous => discrete)
  Quantize, // divvys the domain scale into equal segments based on the number of range values (continuous => discrete)
  Pow, // scale using a specified exponent (continuous => continuous)
  Sequential, // requires an interpolator.  Useful for color selection (continuous => continuous)
  Sqrt, // scale using square root value (scalePow.exponent(0.5)) (continuous => continuous)
  Threshold, // divvys the domain scale using threshold values (continous => discrete)
  Time // Mapping of time based values (continuous => continuous)
}

export enum ChartTitleAlign {
  Center = 0,
  Left = 1,
  Right = 2
}
