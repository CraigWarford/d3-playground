import * as d3 from 'd3';
import * as Model from './chart-data.model';

export class D3Labels {
  context: any;
  margin: any;
  dimensions: any;

  // TODO: Make a way to read the label height from the CSS styling *before* rendering the labels
  private barLabelHeight = 16;
  private boxLabelHeight = 16;
  private holdBackgroundColor = '';

  constructor(context: any) {
    this.context = context; // d3 will choke if svg is not referenced through the original context
  }

  drawLabels(data: Model.ChartData) {
    const that = this;
    let visibleGroupCount = 0;
    let visibleIndex = 0;

    // insertText: Create an SVG group element for the text and pass it as the nodeContext
    const insertText = function (nodeContext: any, value = '', textAnchor = 'middle'): SVGRect {
      const node = d3.select(nodeContext).node();
      let bbox: SVGRect = null;
      if (typeof node.childNodes['0'] === 'undefined' &&
        typeof node.childNodes['1'] === 'undefined') {
        const txt = document.createElementNS('http://www.w3.org/2000/svg', 'text');
        txt.setAttributeNS(null, 'class', 'label');
        txt.setAttributeNS(null, 'text-anchor', textAnchor);
        txt.setAttributeNS(null, 'font-size', that.context.labelFontSize);
        txt.innerHTML = value;
        node.appendChild(txt);

        bbox = txt.getBBox();
        const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        rect.setAttributeNS(null, 'fill', that.context.backgroundColor);
        rect.setAttributeNS(null, 'fill-opacity', '.7');
        if (textAnchor === 'middle') {
          rect.setAttributeNS(null, 'x', (-bbox.width / 2).toString());
        } else {
          rect.setAttributeNS(null, 'x', '0');
        }
        rect.setAttributeNS(null, 'y', (-bbox.height + 2).toString());
        rect.setAttributeNS(null, 'width', bbox.width.toString());
        rect.setAttributeNS(null, 'height', bbox.height.toString());
        node.insertBefore(rect, node.childNodes[0]);
      } else {
        node.childNodes['1'].innerHTML = value;
        bbox = node.childNodes['1'].getBBox();
        if (textAnchor === 'middle') {
          node.childNodes['0'].setAttributeNS(null, 'x', (-bbox.width / 2).toString());
        } else {
          node.childNodes['0'].setAttributeNS(null, 'x', '0');
        }
        node.childNodes['0'].setAttributeNS(null, 'y', (-bbox.height + 2).toString());
        node.childNodes['0'].setAttributeNS(null, 'width', bbox.width.toString());
        node.childNodes['0'].setAttributeNS(null, 'height', bbox.height.toString());
        node.childNodes['0'].setAttributeNS(null, 'fill', that.context.backgroundColor);
      }
      return bbox;
    };

    data.groups.forEach(group => {
      if (!group.disabled) {
        visibleGroupCount += 1;
      }
    });
    data.groups.forEach((group, groupIndex) => {
      if (typeof group.xScale === 'undefined' || isNaN(group.xScale) ||
        typeof group.yScale === 'undefined' || isNaN(group.yScale)) {
        return;
      }
      if (!group.disabled) {
        visibleIndex += 1;
      }
      // Each group shares the x and y scales and color setting
      const xScale = data.scales[group.xScale].scale;
      const yScale = data.scales[group.yScale].scale;
      // const color = this.context.colorScheme(groupIndex);

      // Find the wrapper for this group
      const d3Group = d3.select(this.context.svg).node().selectAll(`g.text-wrap-${groupIndex}`);
      const isPareto = data.groups[0].bars && data.groups[0].lines && data.groups[0].lines[0];

      // Draw bar chart labels
      if (!data.hideBarLabels && group.bars && group.bars.points && group.bars.points.length > 0) {
        const visibleBars = (group.disabled) ? [] : group.bars.points;
        // Get total count for calculating altLabels
        const groupBarTotal = group.bars.points
          .map(point => point[1])
          .reduce((a, b) => a + b);

        const getBarWidth = () => {
          let width = (xScale.bandwidth() < 50) ? xScale.bandwidth() : 50;
          if (width - (10 * (visibleGroupCount - 1)) > 10) {
            width = width - (10 * (visibleGroupCount - 1));
          }
          return width;
        };

        const getBarLabelTransform = (d: any, labelHeight = 20) => {
          const translateX = xScale(d[0]) + (xScale.bandwidth() / 2) + (10 * (visibleIndex - 1));
          let translateY = yScale(d[1]);
          if (group.bars.displayAsPercentage) {
            translateY = yScale(d[1] * 100 / groupBarTotal);
          }
          if (translateY - (labelHeight + 10) < 0) {
            translateY += (labelHeight + 5);
          } else {
            translateY -= 10;
          }
          return { translateX: translateX, translateY: translateY };
        };

        const getBarAltLabelTransform = (d: any, labelHeight = 20) => {
          const translateX = xScale(d[0]) + (xScale.bandwidth() / 2) + (10 * (visibleIndex - 1));
          let translateY = yScale(d[1]);
          if (group.bars.displayAsPercentage) {
            translateY = yScale(d[1] * 100 / groupBarTotal);
          }
          if (translateY - (labelHeight + 10) * 2 < 0) {
            if (translateY - (labelHeight + 10) < 0) {
              translateY += (labelHeight * 2) + 15;
            } else {
              translateY += (labelHeight + 5);
            }
          } else {
            translateY -= (labelHeight + 10 + 5);
          }
          return { translateX: translateX, translateY: translateY };
        };

        // Add wrapper for the bar labels
        const labelWrap = d3Group.selectAll('g.labelWrap').data([group]);
        labelWrap.enter().append('g')
          .attr('class', 'labelWrap')
          .attr('transform', `translate(${this.context.margin.left},${this.context.margin.top})`);

        if (getBarWidth() >= 40 && visibleGroupCount === 1 && !isPareto) {
          const barLabelWrap = labelWrap.selectAll('g.barLabelWrap')
            .data(visibleBars);
          barLabelWrap.enter().append('g')
            .attr('class', 'barLabelWrap')
            // .attr('fill', color) // Makes label text match bar color
            .attr('transform', (d) => {
              const transform: any = getBarLabelTransform(d, this.barLabelHeight);
              return `translate(${transform.translateX},${transform.translateY})`;
            })
            .each(function (d) {
              let value = that.context.formatForDisplay(d[1], data.decimals).toString();
              if (group.bars.displayAsPercentage) {
                value = (that.context.formatForDisplay((d[1] * 100 / groupBarTotal), 1)).toString() + '%';
              }
              that.barLabelHeight = insertText(this, value).height;
            });
          barLabelWrap.exit().remove();
          barLabelWrap.transition()
            .attr('transform', (d) => {
              const transform: any = getBarLabelTransform(d, this.barLabelHeight);
              return `translate(${transform.translateX},${transform.translateY})`;
            })
            .each(function (d) {
              let value = that.context.formatForDisplay(d[1], data.decimals).toString();
              if (group.bars.displayAsPercentage) {
                value = (that.context.formatForDisplay((d[1] * 100 / groupBarTotal), 1)).toString() + '%';
              }
              that.barLabelHeight = insertText(this, value).height;
            });

          if (group.bars.showAltLabel) {
            const barAltLabelWrap = labelWrap.selectAll('g.barAltLabelWrap')
              .data(visibleBars);
            barAltLabelWrap.enter().append('g')
              .attr('class', 'barAltLabelWrap')
              // .attr('fill', color) // Makes label text match bar text
              .attr('transform', (d) => {
                const transform: any = getBarAltLabelTransform(d, this.barLabelHeight);
                return `translate(${transform.translateX},${transform.translateY})`;
              })
              .each(function (d) {
                const value = `(${d.altLabel})`;
                that.barLabelHeight = insertText(this, value).height;
              });
            barAltLabelWrap.exit().remove();
            barAltLabelWrap.transition()
              .attr('transform', (d) => {
                const transform: any = getBarAltLabelTransform(d, this.barLabelHeight);
                return `translate(${transform.translateX},${transform.translateY})`;
              })
              .each(function (d) {
                const value = `(${d.altLabel})`;
                that.barLabelHeight = insertText(this, value).height;
              });
          } else {
            labelWrap.selectAll('g.barAltLabelWrap').remove();
          }

        } else {
          labelWrap.selectAll('g.barLabelWrap').remove();
          labelWrap.selectAll('g.barAltLabelWrap').remove();
        }

      }

      // Draw box plot labels
      if (group.boxes && group.boxes.points && group.boxes.points.length > 0) {
        let visibleBoxes = (group.disabled) ? [] : group.boxes.points;
        visibleBoxes = visibleBoxes.filter(box => {
          return box.length > 1;
        });

        // data plot ordinal values
        const label = 0;
        const upper_quartile = 1;
        const lower_quartile = 2;
        const median = 3;
        const upper_whisker = 4;
        const lower_whisker = 5;

        const getBandWidth = () => {
          return (xScale.bandwidth() < 50) ? xScale.bandwidth() : 50;
        };

        const getBoxWidth = () => {
          let width = getBandWidth();
          if (width - (10 * (visibleGroupCount - 1)) > 10) {
            width = width - (10 * (visibleGroupCount - 1));
          }
          return width;
        };

        let leftAdjust = (xScale.bandwidth() - getBandWidth()) / 2 + getBoxWidth() + 5;
        let textAlign = 'left';

        if (getBoxWidth() < 40) {
          leftAdjust = (xScale.bandwidth() - getBandWidth() + getBoxWidth()) / 2;
          textAlign = 'middle';
        }

        // Add wrapper for the box labels
        const labelWrap = d3Group.selectAll('g.labelWrap').data([group]);
        labelWrap.enter().append('g')
          .attr('class', 'labelWrap')
          .attr('transform', `translate(${this.context.margin.left},${this.context.margin.top})`);
        labelWrap.selectAll('text.box').remove();
        labelWrap.selectAll('rect.box-note-rect').remove();

        if (getBoxWidth() > 30 && visibleGroupCount === 1) {
          if (!group.boxes.hideLabels) {
            const uqBoxLabelWrap = labelWrap.selectAll('g.uqBoxLabelWrap')
              .data(visibleBoxes);
            uqBoxLabelWrap.enter().append('g')
              .attr('class', 'uqBoxLabelWrap')
              // .attr('fill', color) // Makes label text match box color
              .attr('transform', (d) => {
                const translateX = xScale(d[label]) + leftAdjust + (10 * (visibleIndex - 1));
                const translateY = yScale(d[upper_quartile]) + (that.boxLabelHeight / 2) - 3;
                return `translate(${translateX},${translateY})`;
              })
              .each(function (d) {
                const value = that.context.formatForDisplay(d[upper_quartile], data.decimals).toString();
                that.boxLabelHeight = insertText(this, value, textAlign).height;
              });
            uqBoxLabelWrap.exit().remove();
            uqBoxLabelWrap.transition()
              .attr('transform', (d) => {
                const translateX = xScale(d[label]) + leftAdjust + (10 * (visibleIndex - 1));
                const translateY = yScale(d[upper_quartile]) + (that.boxLabelHeight / 2) - 3;
                return `translate(${translateX},${translateY})`;
              })
              .each(function (d) {
                const value = that.context.formatForDisplay(d[upper_quartile], data.decimals).toString();
                that.boxLabelHeight = insertText(this, value, textAlign).height;
              });

            const lqBoxLabelWrap = labelWrap.selectAll('g.lqBoxLabelWrap')
              .data(visibleBoxes);
            lqBoxLabelWrap.enter().append('g')
              .attr('class', 'lqBoxLabelWrap')
              // .attr('fill', color) // Makes label text match box color
              .attr('transform', (d) => {
                const translateX = xScale(d[label]) + leftAdjust + (10 * (visibleIndex - 1));
                const translateY = yScale(d[lower_quartile]) + (that.boxLabelHeight / 2) - 3;
                return `translate(${translateX},${translateY})`;
              })
              .each(function (d) {
                const value = that.context.formatForDisplay(d[lower_quartile], data.decimals).toString();
                that.boxLabelHeight = insertText(this, value, textAlign).height;
              });
            lqBoxLabelWrap.exit().remove();
            lqBoxLabelWrap.transition()
              .attr('transform', (d) => {
                const translateX = xScale(d[label]) + leftAdjust + (10 * (visibleIndex - 1));
                const translateY = yScale(d[lower_quartile]) + (that.boxLabelHeight / 2) - 3;
                return `translate(${translateX},${translateY})`;
              })
              .each(function (d) {
                const value = that.context.formatForDisplay(d[lower_quartile], data.decimals).toString();
                that.boxLabelHeight = insertText(this, value, textAlign).height;
              });

            const mBoxLabelWrap = labelWrap.selectAll('g.mBoxLabelWrap')
              .data(visibleBoxes);
            mBoxLabelWrap.enter().append('g')
              .attr('class', 'mBoxLabelWrap')
              // .attr('fill', color) // Makes label text match box color
              .attr('transform', (d) => {
                const translateX = xScale(d[label]) + leftAdjust + (10 * (visibleIndex - 1));
                const translateY = yScale(d[median]) + (that.boxLabelHeight / 2) - 3;
                return `translate(${translateX},${translateY})`;
              })
              .each(function (d) {
                const value = that.context.formatForDisplay(d[median], data.decimals).toString();
                that.boxLabelHeight = insertText(this, value, textAlign).height;
              });
            mBoxLabelWrap.exit().remove();
            mBoxLabelWrap.transition()
              .attr('transform', (d) => {
                const translateX = xScale(d[label]) + leftAdjust + (10 * (visibleIndex - 1));
                const translateY = yScale(d[median]) + (that.boxLabelHeight / 2) - 3;
                return `translate(${translateX},${translateY})`;
              })
              .each(function (d) {
                const value = that.context.formatForDisplay(d[median], data.decimals).toString();
                that.boxLabelHeight = insertText(this, value, textAlign).height;
              });

            const uwBoxLabelWrap = labelWrap.selectAll('g.uwBoxLabelWrap')
              .data(visibleBoxes);
            uwBoxLabelWrap.enter().append('g')
              .attr('class', 'uwBoxLabelWrap')
              // .attr('fill', color) // Makes label text match box color
              .attr('transform', (d) => {
                const translateX = xScale(d[label]) + leftAdjust + (10 * (visibleIndex - 1));
                const translateY = yScale(d[upper_whisker]) + (that.boxLabelHeight / 2) - 3;
                return `translate(${translateX},${translateY})`;
              })
              .each(function (d) {
                const value = that.context.formatForDisplay(d[upper_whisker], data.decimals).toString();
                that.boxLabelHeight = insertText(this, value, textAlign).height;
              });
            uwBoxLabelWrap.exit().remove();
            uwBoxLabelWrap.transition()
              .attr('transform', (d) => {
                const translateX = xScale(d[label]) + leftAdjust + (10 * (visibleIndex - 1));
                const translateY = yScale(d[upper_whisker]) + (that.boxLabelHeight / 2) - 3;
                return `translate(${translateX},${translateY})`;
              })
              .each(function (d) {
                const value = that.context.formatForDisplay(d[upper_whisker], data.decimals).toString();
                that.boxLabelHeight = insertText(this, value, textAlign).height;
              });

            const lwBoxLabelWrap = labelWrap.selectAll('g.lwBoxLabelWrap')
              .data(visibleBoxes);
            lwBoxLabelWrap.enter().append('g')
              .attr('class', 'lwBoxLabelWrap')
              // .attr('fill', color) // Makes label text match box color
              .attr('transform', (d) => {
                const translateX = xScale(d[label]) + leftAdjust + (10 * (visibleIndex - 1));
                const translateY = yScale(d[lower_whisker]) + (that.boxLabelHeight / 2) - 3;
                return `translate(${translateX},${translateY})`;
              })
              .each(function (d) {
                const value = that.context.formatForDisplay(d[lower_whisker], data.decimals).toString();
                that.boxLabelHeight = insertText(this, value, textAlign).height;
              });
            lwBoxLabelWrap.exit().remove();
            lwBoxLabelWrap.transition()
              .attr('transform', (d) => {
                const translateX = xScale(d[label]) + leftAdjust + (10 * (visibleIndex - 1));
                const translateY = yScale(d[lower_whisker]) + (that.boxLabelHeight / 2) - 3;
                return `translate(${translateX},${translateY})`;
              })
              .each(function (d) {
                const value = that.context.formatForDisplay(d[lower_whisker], data.decimals).toString();
                that.boxLabelHeight = insertText(this, value, textAlign).height;
              });
          }

          // Add outlier points
          if (!group.boxes.hideOutlierLabels) {
            const outlierIndices = [];
            visibleBoxes.forEach((points: Array<any>) => {
              if (points.length > 6) {
                for (let y = 6; y < points.length; y++) {
                  outlierIndices.push([points[label], points[y]]);
                }
              }
            });
            const oBoxLabelWrap = labelWrap.selectAll('g.oBoxLabelWrap')
              .data(outlierIndices);
            oBoxLabelWrap.enter().append('g')
              .attr('class', 'oBoxLabelWrap')
              // .attr('fill', color) // Makes label text match box color
              .attr('transform', (d) => {
                const translateX = xScale(d[label]) + leftAdjust + (10 * (visibleIndex - 1));
                const translateY = yScale(d[1]) + (that.boxLabelHeight / 2) - 3;
                return `translate(${translateX},${translateY})`;
              })
              .each(function (d) {
                const value = that.context.formatForDisplay(d[1], data.decimals).toString();
                that.boxLabelHeight = insertText(this, value, textAlign).height;
              });
            oBoxLabelWrap.exit().remove();
            oBoxLabelWrap.transition()
              .attr('transform', (d) => {
                const translateX = xScale(d[label]) + leftAdjust + (10 * (visibleIndex - 1));
                const translateY = yScale(d[1]) + (that.boxLabelHeight / 2) - 3;
                return `translate(${translateX},${translateY})`;
              })
              .each(function (d) {
                const value = that.context.formatForDisplay(d[1], data.decimals).toString();
                that.boxLabelHeight = insertText(this, value, textAlign).height;
              });
          }

        } else {
          labelWrap.selectAll('g.uqBoxLabelWrap').remove();
          labelWrap.selectAll('g.lqBoxLabelWrap').remove();
          labelWrap.selectAll('g.mBoxLabelWrap').remove();
          labelWrap.selectAll('g.uwBoxLabelWrap').remove();
          labelWrap.selectAll('g.lwBoxLabelWrap').remove();
          labelWrap.selectAll('g.oBoxLabelWrap').remove();
        }
      }
    });
  }

}
