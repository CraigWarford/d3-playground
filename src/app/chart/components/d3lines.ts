import * as d3 from 'd3';
import * as Model from './chart-data.model';
import * as _ from 'lodash';
import { D3ScaleType } from './enums';

export class D3Lines {
  context: any;
  margin: any;
  dimensions: any;

  private ghostRadius = 3;
  private dotRadius = 2.5;

  constructor(context: any) {
    this.context = context; // d3 will choke if svg is not referenced through the original context
  }

  clearLines(): void {
    d3.select(this.context.svg).node().selectAll('g.voronoi.lines').remove();
    d3.select(this.context.svg).node().selectAll('g.linesWrap').remove();
  }


  drawLines(data: Model.ChartData): void {
    const that = this;

    //#region Extended Lines
    // Extended mean has to be drawn for all groups, goes outside the group drawing
    let sharedMeanColor = '#777';
    const meanStart = 0;
    const meanEnd = this.context.dimensions.width - this.context.margin.left - this.context.margin.right;
    let labelStart = data.scales[0].domain[0] || -1;
    if (typeof data.sharedMeanGroup !== 'undefined' &&
      data.sharedMeanGroup !== null &&
      !isNaN(data.sharedMeanGroup)) {
      sharedMeanColor = data.groups[data.sharedMeanGroup].color || this.context.colorScheme(data.sharedMeanGroup);
      labelStart = (data.groups[data.sharedMeanGroup].lines[0]) ?
        data.groups[data.sharedMeanGroup].lines[0].points[0][0] : 0;
    }
    const extSharedPoints = [];
    const extLabelPoint = [];
    const fixedLinePoints = [];
    if (data.sharedMeanPoints && data.sharedMeanPoints.length > 1) {
      extSharedPoints.push([meanStart, data.sharedMeanPoints[0][1]]);
      extSharedPoints.push([meanEnd, data.sharedMeanPoints[1][1]]);
      extLabelPoint.push([labelStart, data.sharedMeanPoints[0][1]]);
      if (data.sharedMeanGroup === null && data.scales[0].sliderPointsX.length > 1) {
        fixedLinePoints.push([data.scales[0].sliderPointsX[0], data.sharedMeanPoints[0][1]]);
        fixedLinePoints.push([data.scales[0].sliderPointsX[1], data.sharedMeanPoints[1][1]]);
      }
      if (typeof data.sharedMeanGroup !== 'undefined' &&
      data.sharedMeanGroup !== null &&
      !isNaN(data.sharedMeanGroup) &&
      !data.groups[data.sharedMeanGroup].disabled &&
      data.groups[data.sharedMeanGroup].sliderPointsX.length > 1) {
        fixedLinePoints.push([data.groups[data.sharedMeanGroup].sliderPointsX[0], data.sharedMeanPoints[0][1]]);
        fixedLinePoints.push([data.groups[data.sharedMeanGroup].sliderPointsX[1], data.sharedMeanPoints[1][1]]);
      }
    }

    // Find the wrapper for this group
    const dataWrap = d3.select(this.context.svg).node().selectAll(`g.dataWrap`);

    if (data.sharedMeanPoints || extSharedPoints.length > 0) {
      // Add wrapper for the lines
      const extLinesWrap = dataWrap.selectAll('g.extLinesWrap')
        .data(() => {
          return [data.sharedMeanPoints];
        });
      extLinesWrap.enter().append('g')
        .attr('class', 'extLinesWrap');

      // Add individual lines
      const extLines = extLinesWrap.selectAll('.line')
        .data(() => {
          return [data.sharedMeanPoints];
        });
      extLines.enter().append('g')
        .attr('class', 'line')
        .style('stroke-opacity', 1)
        .style('fill', sharedMeanColor)
        .style('stroke', sharedMeanColor)
        .style('stroke-linecap', 'round');
      extLines.exit().transition()
        .style('stroke-opacity', 1e-6)
        .remove();
      extLines.transition()
        .style('stroke-opacity', 1)
        .style('fill', sharedMeanColor)
        .style('stroke', sharedMeanColor);

      // Add individual labels
      const extLabels = extLinesWrap.selectAll('text.lineLabel')
        .data(extLabelPoint);
      extLabels.enter().append('text')
        .style('stroke-opacity', 1)
        .style('fill', sharedMeanColor)
        // .style('stroke', color)
        .style('stroke-width', '.5')
        .attr('class', 'lineLabel')
        .attr('text-anchor', 'left')
        .attr('x', (d: any) => {
          const meanXScale = (typeof data.sharedMeanGroup !== 'undefined' &&
          data.sharedMeanGroup !== null &&
          !isNaN(data.sharedMeanGroup)) ? data.groups[data.sharedMeanGroup].xScale : 0;
          return (data.scales[meanXScale].scale(d[0])) ? data.scales[meanXScale].scale(d[0]) + 5 : 5;
        })
        .attr('y', (d: any) => {
          const meanYScale = (typeof data.sharedMeanGroup !== 'undefined' &&
          data.sharedMeanGroup !== null &&
          !isNaN(data.sharedMeanGroup)) ? data.groups[data.sharedMeanGroup].yScale : 1;
          return (data.scales[meanYScale].scale(d[1])) ? data.scales[meanYScale].scale(d[1]) - 5 : 0;
        })
        .style('font-size', this.context.lineLabelFontSize)
        .text(d => {
          return 'MEAN: ' + that.context.formatForDisplay(d[1], data.decimals);
        });
      extLabels.exit().transition()
        .style('stroke-opacity', 1e-6)
        .remove();
      extLabels.transition()
        .style('stroke-opacity', 1)
        .style('fill', sharedMeanColor)
        .attr('x', (d: any) => {
          const meanXScale = (typeof data.sharedMeanGroup !== 'undefined' &&
          data.sharedMeanGroup !== null &&
          !isNaN(data.sharedMeanGroup)) ? data.groups[data.sharedMeanGroup].xScale : 0;
          return (data.scales[meanXScale].scale(d[0])) ? data.scales[meanXScale].scale(d[0]) + 5 : 5;
        })
        .attr('y', (d: any) => {
          const meanYScale = (typeof data.sharedMeanGroup !== 'undefined' &&
          data.sharedMeanGroup !== null &&
          !isNaN(data.sharedMeanGroup)) ? data.groups[data.sharedMeanGroup].yScale : 1;
          return (data.scales[meanYScale].scale(d[1])) ? data.scales[meanYScale].scale(d[1]) - 5 : 0;
        })
        .text(d => {
          return 'MEAN: ' + that.context.formatForDisplay(d[1], data.decimals);
        });

      // Add svg paths to the line
      const extPaths = extLinesWrap.selectAll('.line').selectAll('path.ext')
        .data([extSharedPoints]);
      extPaths.enter().append('path')
        .classed('ext', true)
        .attr('fill', 'none')
        .attr('stroke-width', '2px')
        .attr('stroke-opacity', '0.7')
        .style('stroke-dasharray', '3,5')
        .attr('stroke-linecap', 'round')
        .attr('d', d3.line()
          .x((d: any) => d[0])
          .y((d: any) => {
            const meanYScale = (typeof data.sharedMeanGroup !== 'undefined' &&
            data.sharedMeanGroup !== null &&
            !isNaN(data.sharedMeanGroup)) ? data.groups[data.sharedMeanGroup].yScale : data.groups.length;
            return data.scales[meanYScale].scale(d[1]);
          })
          .defined((d: any) => !isNaN(d[0]))
        );
      extPaths.exit().remove();
      extPaths.transition()
        .attr('d', d3.line()
          .x((d: any) => d[0])
          .y((d: any) => {
            const meanYScale = (typeof data.sharedMeanGroup !== 'undefined' &&
            data.sharedMeanGroup !== null &&
            !isNaN(data.sharedMeanGroup)) ? data.groups[data.sharedMeanGroup].yScale : data.groups.length;
            return data.scales[meanYScale].scale(d[1]);
          })
          .defined((d: any) => !isNaN(d[0]))
        );
      const fixedPaths = extLinesWrap.selectAll('.line').selectAll('path.fixed')
        .data([fixedLinePoints]);
      fixedPaths.enter().append('path')
        .classed('fixed', true)
        .attr('fill', 'none')
        .attr('stroke-width', '2.5px')
        .attr('stroke-opacity', '0.7')
        .attr('stroke-linecap', 'round')
        .attr('d', d3.line()
          .x((d: any) => {
            const meanXScale = (typeof data.sharedMeanGroup !== 'undefined' &&
            data.sharedMeanGroup !== null &&
            !isNaN(data.sharedMeanGroup)) ? data.groups[data.sharedMeanGroup].xScale : 0;
            const val = data.scales[meanXScale].scale(d[0]);
            return (typeof val === 'undefined') ? 0 : val;
          })
          .y((d: any) => {
            const meanYScale = (typeof data.sharedMeanGroup !== 'undefined' &&
            data.sharedMeanGroup !== null &&
            !isNaN(data.sharedMeanGroup)) ? data.groups[data.sharedMeanGroup].yScale : data.groups.length;
            return data.scales[meanYScale].scale(d[1]);
          })
        );
      fixedPaths.exit().remove();
      fixedPaths.transition()
        .attr('d', d3.line()
          .x((d: any) => {
            const meanXScale = (typeof data.sharedMeanGroup !== 'undefined' &&
            data.sharedMeanGroup !== null &&
            !isNaN(data.sharedMeanGroup)) ? data.groups[data.sharedMeanGroup].xScale : 0;
            const val = data.scales[meanXScale].scale(d[0]);
            return (typeof val === 'undefined') ? 0 : val;
          })
          .y((d: any) => {
            const meanYScale = (typeof data.sharedMeanGroup !== 'undefined' &&
            data.sharedMeanGroup !== null &&
            !isNaN(data.sharedMeanGroup)) ? data.groups[data.sharedMeanGroup].yScale : data.groups.length;
            return data.scales[meanYScale].scale(d[1]);
          })
        );

    }

    //#endregion

    let visibleGroupCount = 0;
    let visibleIndex = 0;
    data.groups.forEach(group => {
      if (!group.disabled) {
        visibleGroupCount += 1;
      }
    });
    data.groups.forEach((group, groupIndex) => {
      if (!group.disabled) {
        visibleIndex += 1;
      }
      // Get ghost points
      let mainPoints = [];
      let ghostPoints = [];
      let noLine = false;
      group.lines = group.lines || [];
      group.lines.forEach(line => {
        if (line.type === Model.ChartLineType.Main) {
          noLine = line.noLine;
          mainPoints = line.points.slice(0, line.points.length);
        }
      });
      group.lines.forEach(line => {
        if (line.type === Model.ChartLineType.Ghost) {
          line.noLine = noLine; // Bring this over with the main line setting, since Ghost is a subset of that
          let startIndex = 1;
          if (line.points[0][0] === mainPoints[0][0] &&
            line.points[0][1] === mainPoints[0][1]) {
            startIndex = 0;
          }
          let endIndex = line.points.length - 1;
          if (line.points[endIndex][0] === mainPoints[mainPoints.length - 1][0] &&
            line.points[endIndex][1] === mainPoints[mainPoints.length - 1][1]) {
            endIndex = line.points.length;
          }
          ghostPoints = ghostPoints.concat(line.points.slice(startIndex, endIndex));
        }
      });
      const isGhostPoint = (point) => {
        let isGhost = false;
        ghostPoints.some(ghost => {
          if (ghost[0] === point[0] && ghost[1] === point[1]) {
            isGhost = true;
          }
          return (ghost[0] === point[0] && ghost[1] === point[1]);
        });
        return isGhost;
      };

      // Set indices for later location of points on svg
      group.lines = group.lines.map((line, lineIndex) => {
        line.points = line.points.map((point, pointIndex) => {
          point.groupIndex = groupIndex;
          point.lineIndex = lineIndex;
          point.pointIndex = pointIndex;
          point.filter = (line.pointFilters) ? line.pointFilters[pointIndex] : [];
          if (line.type === Model.ChartLineType.Main) {
            if (line.colorXValues && line.colorXValues[point[0]]) {
              point.color = line.colorXValues[point[0]];
            } else {
              point.color = null;
            }
            if (isGhostPoint(point)) {
              point.ghost = true;
              if (pointIndex === 0 && !line.ghostFirstPoint) {
                point.ghost = false;
              }
              if (pointIndex === line.points.length - 1 && !line.ghostLastPoint) {
                point.ghost = false;
              }
            }
          }
          return point;
        });
        return line;
      });

      // Each group shares the x and y scales and color setting
      const xScale = data.scales[group.xScale].scale;
      const yScale = data.scales[group.yScale].scale;
      const color = (group.color) ? group.color : this.context.colorScheme(groupIndex);
      let uclLabel = '';
      let lclLabel = '';
      group.lines.forEach(line => {
        if (line.type === Model.ChartLineType.UCL) {
          if (line.points.length < 2 ||
            line.points[0][1] === null ||
            line.points[1][1] === null) {
            uclLabel = '';
          } else {
            uclLabel = line.label;
          }
        }
        if (line.type === Model.ChartLineType.LCL) {
          if (line.points.length < 2 ||
            line.points[0][1] === null ||
            line.points[1][1] === null) {
            lclLabel = '';
          } else {
            lclLabel = line.label;
          }
        }
      });
      group.lines.map(line => {
        if (line.points.length >= 2 &&
          (yScale(line.points[0][1]) < 0 ||
          yScale(line.points[1][1]) < 0)) {
          return '';
        }
        return 'UCL';
      });

      let barGap = (xScale.bandwidth) ? 6 : 0;
      if (xScale.bandwidth && xScale.bandwidth() < 20) { barGap = 4; }
      if (xScale.bandwidth && xScale.bandwidth() < 10) { barGap = 0; }
      const maxBoxWidth = 50 + (25 * (visibleGroupCount - 1)) - barGap;
      const getBandWidth = () => {
        if (!xScale.bandwidth) {
          return 0;
        }
        return (xScale.bandwidth() < maxBoxWidth + barGap) ? xScale.bandwidth() - barGap : maxBoxWidth;
      };
      const getBoxWidth = () => {
        if (!xScale.bandwidth) {
          return 0;
        }
        return getBandWidth() * (2 / (visibleGroupCount + 1));
      };
      const getBoxLeft = (boxIndex: number) => {
        if (!xScale.bandwidth) {
          return maxBoxWidth / 2;
        }
        if (xScale.bandwidth() < maxBoxWidth + barGap) {
          return getBandWidth() * (boxIndex - 1) / (visibleGroupCount + 1);
        } else {
          return (getBandWidth() * (boxIndex - 1) / (visibleGroupCount + 1)) + ((xScale.bandwidth() - getBandWidth()) / 2);
        }
      };
      const getBandAdjust = () => {
        let bandAdjust = 0;
        if (data.scales[group.xScale].type === D3ScaleType.Band) {
          bandAdjust += (barGap / 2) + getBoxLeft(visibleIndex) + (getBoxWidth() / 2);
        }
        return bandAdjust;
      };

      // Control label positioning for line labels
      let uclY: number = null;
      let meanY: number = null;
      let lclY: number = null;
      const getLabelPosition = (d: any) => {
        let x = -1000;
        let y = -1000;
        if (d.points && d.points.length > 0) {
          x = (d.points[0].noScale) ? d.points[0][0] : xScale(d.points[0][0]) + 5;
          y = yScale(d.points[0][1]) - 5;
          // Collision check/adjust
          // TODO: Set minSpread from font size
          const minSpread = 15;
          switch (d.type) {
            case Model.ChartLineType.UCL:
              uclY = y;
              if (meanY !== null && uclY > meanY - minSpread) {
                y = uclY = meanY - minSpread;
              } else if (lclY !== null && uclY > lclY - (minSpread * 2)) {
                y = uclY = lclY - (minSpread * 2);
              }
              if (uclY < 0 || d.points[0][1] === null) {
                uclY = null;
                y = -1000;
              }
              break;
            case Model.ChartLineType.Mean:
              meanY = y;
              if (uclY !== null && uclY > meanY - minSpread) {
                y = meanY = uclY + minSpread;
              }
              if (lclY !== null && lclY < meanY + minSpread) {
                y = meanY = lclY - minSpread;
              }
              if (d.points[0][1] === null) {
                meanY = null;
                y = -1000;
              }
              break;
            case Model.ChartLineType.LCL:
              lclY = y;
              if (meanY !== null && lclY < meanY + minSpread) {
                y = lclY = meanY + minSpread;
              } else if (uclY !== null && lclY < uclY + (minSpread * 2)) {
                y = lclY = uclY + (minSpread * 2);
              }
              if (y > this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom - 20 ||
                d.points[0][1] === null) {
                lclY = null;
                y = -1000;
              }
              break;
          }
        }
        return { x, y };
      };

      // Find the wrapper for this group
      const d3Group = d3.select(this.context.svg).node().selectAll(`g.group-wrap-${groupIndex}`);

      // Add wrapper for the lines
      const linesWrap = d3Group.selectAll('g.linesWrap').data([group]);
      linesWrap.enter().append('g')
        .attr('class', 'linesWrap');

      const visibleLines = (group.disabled) ? [] : group.lines.filter(line => {
        return line.visible && !line.blockLine;
      });

      // Create additional lines for blocked limit lines
      if (!group.disabled) {
        group.lines.forEach(line => {
          if ((line.type === Model.ChartLineType.UCL || line.type === Model.ChartLineType.LCL) &&
            line.blockLine && line.visible) {
            const blockLine = _.cloneDeep(line);
            // TODO: Modify the points; scale the x values now
            blockLine.points = [];
            line.points.forEach((point, index) => {
              const point1 = _.cloneDeep(point);
              point1.groupIndex = point.groupIndex;
              point1.lineIndex = point.lineIndex;
              point1.pointIndex = point.pointIndex;
              point1.noScale = true;
              const nextPoint = (index < line.points.length - 1) ? line.points[index + 1] : null;
              if (nextPoint) {
                const x1 = (xScale(point[0])) ? xScale(point[0]) + getBandAdjust() : 0 + getBandAdjust();
                const x2 = (xScale(nextPoint[0])) ? xScale(nextPoint[0]) + getBandAdjust() : 0 + getBandAdjust();
                point1[0] = x1;
                blockLine.points.push(point1);
                const point2 = _.cloneDeep(point1);
                point2.groupIndex = point.groupIndex;
                point2.lineIndex = point.lineIndex;
                point2.pointIndex = point.pointIndex;
                point2.noScale = true;
                point2[0] += (x2 - x1) / 2;
                blockLine.points.push(point2);
                const point3 = _.cloneDeep(point2);
                point3.groupIndex = point.groupIndex;
                point3.lineIndex = point.lineIndex;
                point3.pointIndex = point.pointIndex;
                point3.noScale = true;
                point3[1] = nextPoint[1];
                blockLine.points.push(point3);
                const point4 = _.cloneDeep(point3);
                point4.groupIndex = point.groupIndex;
                point4.lineIndex = point.lineIndex;
                point4.pointIndex = point.pointIndex;
                point4.noScale = true;
                point4[0] = x2;
                blockLine.points.push(point4);
              }
            });
            visibleLines.push(blockLine);
          }
        });
      }

      // Add individual line groups
      const lines = linesWrap.selectAll('.line')
        .data(visibleLines, (d: any) => d.type);
      lines.enter().append('g')
        .style('stroke-opacity', 1)
        .style('stroke-dasharray', d => {
          if (Model.ChartLineTypeClasses[d.type] === 'ucl' ||
            Model.ChartLineTypeClasses[d.type] === 'lcl') {
            return '5,5';
          }
          if (Model.ChartLineTypeClasses[d.type] === 'mean' &&
            (typeof data.sharedMeanGroup === null || !isNaN(data.sharedMeanGroup)) &&
            data.groups[groupIndex].sliderPointsX.length > 1) {
            return '5,5';
          }
          if (Model.ChartLineTypeClasses[d.type] === 'ghost') {
            return '2,5';
          }
        })
        .style('fill', d => d.color || color)
        .style('stroke', d => d.color || color)
        .attr('clip-path', d => {
          if (Model.ChartLineTypeClasses[d.type] === 'ucl' ||
          Model.ChartLineTypeClasses[d.type] === 'lcl' ||
          Model.ChartLineTypeClasses[d.type] === 'cl' ||
          Model.ChartLineTypeClasses[d.type] === 'mean') {
            return `url(#${this.context.clipPathId})`;
          }
          return '';
        })
        .style('stroke-linecap', 'round')
        .attr('class', (d, i) => Model.ChartLineTypeClasses[d.type] + ' line line-' + i)
        .classed('hover', (d) =>
          d.hover
        );
      lines.exit().transition()
        .style('stroke-opacity', 1e-6)
        .remove();
      lines.transition()
        .style('stroke-opacity', 1)
        .style('fill', d => d.color || color)
        .style('stroke', d => d.color || color);

      // Add individual labels
      const labels = linesWrap.selectAll('text.lineLabel')
        .data(visibleLines, (d: any) => d.type);
      labels.enter().append('text')
        .style('stroke-opacity', 1)
        .style('fill', d => d.color || color)
        // .style('stroke', d => d.color || color)
        .style('stroke-width', '.5')
        .attr('class', 'lineLabel')
        .attr('text-anchor', 'left')
        .attr('x', d => getLabelPosition(d).x)
        .attr('y', d => getLabelPosition(d).y)
        .style('font-size', this.context.lineLabelFontSize)
        .text(d => {
          if (Model.ChartLineTypeClasses[d.type] === 'ucl') {
            return uclLabel;
          }
          if (Model.ChartLineTypeClasses[d.type] === 'lcl') {
            return lclLabel;
          }
          return (d.label) ? d.label : '';
        });
      labels.exit().transition()
        .style('stroke-opacity', 1e-6)
        .remove();
      labels.transition()
        .style('stroke-opacity', 1)
        .style('fill', d => d.color || color)
        .attr('x', d => getLabelPosition(d).x)
        .attr('y', d => getLabelPosition(d).y)
        .text(d => {
          if (Model.ChartLineTypeClasses[d.type] === 'ucl') {
            return uclLabel;
          }
          if (Model.ChartLineTypeClasses[d.type] === 'lcl') {
            return lclLabel;
          }
          return (d.label) ? d.label : '';
        });


      // Add normal svg paths to the line (straight segments)
      const paths = linesWrap.selectAll('.line').selectAll('path.normal')
        .data(
          (d: any) => {
            if (Model.ChartLineTypeClasses[d.type] === 'main' && !d.allowNullValues) {
              return [d.points.filter(point => {
                return point[1] !== null;
              })];
            }
            return [d.points];
          },
          (d: any) => [d.type]
        );
      paths.enter().append('path')
        .classed('normal', true)
        .attr('fill', (d: any) => {
          if (typeof d[0] !== 'undefined' &&
            data.groups[d[0].groupIndex].lines[d[0].lineIndex] &&
            data.groups[d[0].groupIndex].lines[d[0].lineIndex].fill) {
            return '';
          }
          return 'none';
        })
        .attr('fill-opacity', (d: any) => {
          if (typeof d[0] !== 'undefined' &&
            data.groups[d[0].groupIndex].lines[d[0].lineIndex] &&
            data.groups[d[0].groupIndex].lines[d[0].lineIndex].fillOpacity) {
            return data.groups[d[0].groupIndex].lines[d[0].lineIndex].fillOpacity;
          }
          return '1';
        })
        .attr('stroke-width', function(): string {
          if (this.parentElement.classList.contains('mean') ||
            this.parentElement.classList.contains('ucl') ||
            this.parentElement.classList.contains('lcl') ||
            this.parentElement.classList.contains('cl')) {
            return '2px';
          }
          return '2.5px';
        })
        .attr('stroke-opacity', (d: any) => {
          if (typeof d[0] !== 'undefined' &&
            data.groups[d[0].groupIndex].lines[d[0].lineIndex] &&
            data.groups[d[0].groupIndex].lines[d[0].lineIndex].noLine) {
            return '0';
          }
          return '1';
        })
        .attr('stroke-linecap', 'round')
        .attr('d', d3.line()
          .x((d: any) => {
            if (typeof d !== 'undefined' &&
              data.groups[d.groupIndex].lines[d.lineIndex] &&
              data.groups[d.groupIndex].lines[d.lineIndex].blockLine) {
              return d[0];
            } else {
              return (xScale(d[0])) ? xScale(d[0]) + getBandAdjust() : 0 + getBandAdjust();
            }
          })
          .y((d: any) => (yScale(d[1])) ? Math.max(yScale(d[1]), -1000) : 0)
          .defined((d: any) => {
            if (d.ghost) {
              return false;
            }
            return (d[1] || d[1] === 0);
          })
        );
      paths.exit().remove();
      paths.transition()
        .attr('fill', (d: any) => {
          if (typeof d[0] !== 'undefined' &&
            data.groups[d[0].groupIndex].lines[d[0].lineIndex] &&
            data.groups[d[0].groupIndex].lines[d[0].lineIndex].fill) {
            return '';
          }
          return 'none';
        })
        .attr('fill-opacity', (d: any) => {
          if (typeof d[0] !== 'undefined' &&
            data.groups[d[0].groupIndex].lines[d[0].lineIndex] &&
            data.groups[d[0].groupIndex].lines[d[0].lineIndex].fillOpacity) {
            return data.groups[d[0].groupIndex].lines[d[0].lineIndex].fillOpacity;
          }
          return '1';
        })
        .attr('stroke-opacity', (d: any) => {
          if (typeof d[0] !== 'undefined' &&
            data.groups[d[0].groupIndex].lines[d[0].lineIndex] &&
            data.groups[d[0].groupIndex].lines[d[0].lineIndex].noLine) {
            return '0';
          }
          return '1';
        })
        .attr('d', d3.line()
          .x((d: any) => {
            if (typeof d !== 'undefined' &&
              data.groups[d.groupIndex].lines[d.lineIndex] &&
              data.groups[d.groupIndex].lines[d.lineIndex].blockLine) {
              return d[0];
            } else {
              return (xScale(d[0])) ? xScale(d[0]) + getBandAdjust() : 0 + getBandAdjust();
            }
          })
          .y((d: any) => (yScale(d[1])) ? Math.max(yScale(d[1]), -1000) : 0)
          .defined((d: any) => {
            if (d.ghost) {
              return false;
            }
            return (d[1] || d[1] === 0);
          })
        );

      // Add solid paths to the line for extended mean
      let mean = 0;
      let hasCenterLine = false;
      group.lines.forEach(line => {
        if (Model.ChartLineTypeClasses[line.type] === 'mean') {
          mean = line.points[0][1];
        }
        if (Model.ChartLineTypeClasses[line.type] === 'cl') {
          hasCenterLine = true;
        }
      });
      if (!hasCenterLine) {
        let solidPoints = [];
        if (data.sharedMeanGroup === null ||
          (data.sharedMeanGroup !== null &&
          !isNaN(data.sharedMeanGroup) &&
          data.sharedMeanGroup !== groupIndex)) {
          solidPoints = [
            [group.sliderPointsX[0], mean],
            [group.sliderPointsX[1], mean]
          ];
        }
        const solidPaths = linesWrap.selectAll('.line').selectAll('path.ext')
          .data([solidPoints]);
        solidPaths.enter().append('path')
          .classed('ext', true)
          .attr('fill', 'none')
          .attr('stroke-width', '2.5px')
          .attr('stroke-opacity', '0.7')
          .attr('stroke-linecap', 'round')
          .attr('d', d3.line()
            .x((d: any) =>  (xScale(d[0])) ? xScale(d[0]) + getBandAdjust() : 0 + getBandAdjust())
            .y((d: any) => {
              return (yScale(d[1])) ? Math.max(yScale(d[1]), -1000) : 0;
            })
          );
        solidPaths.exit().remove();
        solidPaths.transition()
          .attr('d', d3.line()
            .x((d: any) => (xScale(d[0])) ? xScale(d[0]) + getBandAdjust() : 0 + getBandAdjust())
            .y((d: any) => {
              return (yScale(d[1])) ? Math.max(yScale(d[1]), -1000) : 0;
            })
          );
      }

      // Add points to the main line
      const points = linesWrap.selectAll('.line.main').selectAll('circle.point')
        .data((d: any) => {
          if (d.noPoints) { return []; }
          return d.points.filter((point) => ( point[1] || point[1] === 0));
        });
      points.enter().append('circle')
        .attr('class', function(d: any, i): string {
          d.circle = this;
          const ghostClass = (isGhostPoint(d)) ? ' ghost' : '';
          return 'point point-' + i + ghostClass;
        })
        .attr('cx', (d: any) => (xScale(d[0])) ? xScale(d[0]) + getBandAdjust() : 0 + getBandAdjust())
        .attr('cy', (d: any) => (yScale(d[1])) ? yScale(d[1]) : 0)
        .attr('filterx', (d: any) => {
          return (d) ? d[0].toLowerCase() : '';
        })
        .attr('filtery', (d: any) => {
          return (d) ? d[1] : '';
        })
        .attr('groupIndex', (d: any) => d.groupIndex)
        .attr('fill', (d: any) => {
          if (d.ghost) {
            return 'none';
          }
          return (d.color) ? d.color : color;
        })
        .attr('stroke', (d: any) => {
          return (d.color) ? d.color : color;
        })
        .attr('r', (d: any) => {
          return (d.ghost) ? that.ghostRadius : that.dotRadius;
        });
      points.exit().remove();
      points
        .attr('class', function(d: any, i): string {
          d.circle = this;
          const ghostClass = (isGhostPoint(d)) ? ' ghost' : '';
          return 'point point-' + i + ghostClass;
        });
      points.transition()
        .attr('cx', (d: any) => (xScale(d[0])) ? xScale(d[0]) + getBandAdjust() : 0 + getBandAdjust())
        .attr('cy', (d: any) => (yScale(d[1])) ? yScale(d[1]) : 0)
        .attr('filterx', (d: any) => {
          return (d) ? d[0].toLowerCase() : '';
        })
        .attr('filtery', (d: any) => {
          return (d) ? d[1] : '';
        })
        .attr('groupIndex', (d: any) => d.groupIndex)
        .attr('fill', (d: any) => {
          if (d.ghost) {
            return 'none';
          }
          return (d.color) ? d.color : color;
        })
        .attr('stroke', (d: any) => {
          return (d.color) ? d.color : color;
        })
        .attr('r', (d: any) => {
          return (d.ghost) ? that.ghostRadius : that.dotRadius;
        });

    });
  }

  drawVoronoi(data: Model.ChartData): void {
    const that = this;
    // Build the voronoi map
    const allPoints: Array<any> = d3.merge(
      data.groups.map(group => {
        const mainLines = (group.disabled) ? [] : group.lines.filter(line => line.type === Model.ChartLineType.Main && !line.noPoints);
        return d3.merge(mainLines.map(line => (group.disabled) ? [] : line.points));
      })
    );

    let minY = 0;
    const bars = data.groups.map(group => group.bars).filter(barsObject => typeof barsObject !== 'undefined'  && barsObject !== null);
    if (bars.length > 0) {
      // Assume this is a Pareto chart; set a lower boundary for the voronoi at the lowest point on any line
      const range = this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom;
      minY = Infinity;
      allPoints.forEach(point => {
        const thisY = range - data.scales[data.groups[point.groupIndex].yScale].scale(point[1]);
        minY = (thisY < minY) ? thisY : minY;
      });
    }

    const scaledPoints: any[] = [];
    allPoints.forEach(point => {
      const xScale = data.scales[data.groups[point.groupIndex].xScale];
      const yScale = data.scales[data.groups[point.groupIndex].yScale];
      const bandAdjust = (xScale.type === D3ScaleType.Band) ? xScale.scale.bandwidth() / 2 : 0;
      const scaledX = xScale.scale(point[0]) + bandAdjust + (Math.random() * 1e-3);
      // IMPORTANT: Need to trap null y values, so they don't change to NaN and throw off the voronoi functions.
      const scaledY = (point[1] !== null) ? yScale.scale(point[1]) + (Math.random() * 1e-3) : null;
      scaledPoints.push([scaledX, scaledY]);
    });

    const delaunay = d3.Delaunay.from(scaledPoints);
    const voronoi = delaunay.voronoi([0, 0, this.context.dimensions.width - this.context.margin.left - this.context.margin.right,
      this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom - minY]);
    const cells = allPoints.map((d, i) => [d, voronoi.cellPolygon(i)]);
    const cellRemap = cells.map(cell => {
      const polygon = cell[1] || [0, 0];
      polygon.data = cell[0];
      return polygon;
    });

    this.context.svg.select('g.voronoi.lines').remove();
    const voronoiGroup = this.context.svg.select('g.dataWrap').append('g')
      .attr('class', 'voronoi lines');
    voronoiGroup.selectAll('path')
      .data(cellRemap)
      .enter().append('path')
      .attr('fill', 'none')
      .attr('d', (d) => d ? 'M' + d.join('L') + 'Z' : null)
      .attr('datax', (d: any) => {
        return (d) ? d.data[0] : '';
      })
      .attr('datay', (d: any) => {
        return (d) ? d.data[1] : '';
      })
      .on('mouseover', function(event: any, d: any): void {
        that.context.svg.selectAll('.hover').classed('hover', false);
        that.context.focusPoint = d;
        if (d.data.circle) {
          d3.select(d.data.circle)
            .classed('hover', true);
          const tempScale = data.scales[data.groups[d.data.groupIndex].xScale];
          const bandAdjust = (tempScale.type === D3ScaleType.Band) ?
            tempScale.scale.bandwidth() / 2 : 0;
          that.context.showFocusNote(data, d, false, true, bandAdjust, this);
        }
      })
      .on('mouseout', (event: any, d: any) => {
        this.context.focusPoint = null;
        // Allow mouseover event for text to fire first
        setTimeout(() => {
          if (!this.context.noteHasFocus) {
            if (this.context.focusPoint !== d) {
              if (d.data.circle &&
                !d3.select(d.data.circle).classed('signal')) {
                d3.select(d.data.circle)
                  .classed('hover', false);
              }
            }
            if (this.context.focusPoint === null) {
              this.context.hideFocusNote();
            }
          }
        });
      })
      .on('click', function(event: any, d: any): void {
        if (that.context.miniChart) {
          if (d.data.circle) {
            d3.select(d.data.circle)
              .classed('hover', true);
            const tempScale = data.scales[data.groups[d.data.groupIndex].xScale];
            const bandAdjust = (tempScale.type === D3ScaleType.Band) ?
              tempScale.scale.bandwidth() / 2 : 0;
            that.context.showFocusNote(data, d, false, true, bandAdjust);
          }
        } else {
          if (d.data.circle) {
            that.context.svg.selectAll('.selected').classed('selected', false);
            d3.select(d.data.circle)
              .classed('selected', true);
          }
        }
        const xScale = data.scales[data.groups[d.data.groupIndex].xScale];
        const yScale = data.scales[data.groups[d.data.groupIndex].yScale];
        const pointData: any = {
          x: d.data[0],
          y: d.data[1],
          altText: (d.data[2]) ? d.data[2] : null,
          filter: (d.data.filter) ? d.data.filter : null,
          scaleX: xScale.scale(d.data[0]),
          scaleY: yScale.scale(d.data[1]),
          group: d.data.groupIndex,
          line: d.data.lineIndex,
          point: d.data.pointIndex,
          d3mouse: d3.pointer(event),
          d3event: {clientX: d.data.circle.getBoundingClientRect().x},
          voronoiElement: this
        };
        that.context.pointClick.emit(pointData);
      })
      .exit().remove();

  }

}
