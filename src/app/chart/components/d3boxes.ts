import * as d3 from 'd3';
import * as Model from './chart-data.model';
import { D3ScaleType } from './enums';

export class D3Boxes {
  context: any;
  margin: any;
  dimensions: any;

  constructor(context: any) {
    this.context = context; // d3 will choke if svg is not referenced through the original context
  }

  clearBoxes() {
    d3.select(this.context.svg).node().selectAll('g.voronoi.boxes').remove();
    d3.select(this.context.svg).node().selectAll('g.boxesWrap').remove();
  }

  drawBoxes(data: Model.ChartData) {
    // const fadeOpacity = (this.context.totalLineCount() > 0) ? 0.2 : 0;
    const fadeOpacity = 0;
    let visibleGroupCount = 0;
    let visibleIndex = 0;
    data.groups.forEach(group => {
      if (!group.disabled) {
        visibleGroupCount += 1;
      }
    });
    data.groups.forEach((group, groupIndex) => {
      if (!group.disabled) {
        visibleIndex += 1;
      }
      if (group.boxes && group.boxes.points && group.boxes.points.length > 0) {
        // Set indices for later location of points on svg
        group.boxes.points = group.boxes.points.map((box, boxIndex) => {
          box.groupIndex = groupIndex;
          box.boxIndex = boxIndex;
          return box;
        });

        // Each group shares the x and y scales and color setting
        const xScale = data.scales[group.xScale].scale;
        const yScale = data.scales[group.yScale].scale;
        const color = (group.color) ? group.color : this.context.colorScheme(groupIndex);

        // data plot ordinal values
        const label = 0;
        const upper_quartile = 1;
        const lower_quartile = 2;
        const median = 3;
        const upper_whisker = 4;
        const lower_whisker = 5;

        let barGap = 6;
        if (xScale.bandwidth() < 20) { barGap = 4; }
        if (xScale.bandwidth() < 10) { barGap = 0; }
        const maxBoxWidth = group.boxes.maxBoxWidth || 50 + (25 * (visibleGroupCount - 1)) - barGap;
        const getBandWidth = () => {
          let bandwidth = xScale.bandwidth();
          if (bandwidth === 0) {
            const range = xScale.range();
            const domain = xScale.domain();
            if (range.length === 2 && domain.length >= 2) {
              bandwidth = Math.abs(range[1] - range[0]) / (domain.length - 1);
            }
          }
          return (bandwidth < maxBoxWidth + barGap) ? bandwidth - barGap : maxBoxWidth;
        };
        const getBoxWidth = () => {
          return getBandWidth() * (2 / (visibleGroupCount + 1));
        };
        const getBoxLeft = (boxIndex: number) => {
          if (xScale.bandwidth() < maxBoxWidth + barGap) {
            return getBandWidth() * (boxIndex - 1) / (visibleGroupCount + 1);
          } else {
            return (getBandWidth() * (boxIndex - 1) / (visibleGroupCount + 1)) + ((xScale.bandwidth() - getBandWidth()) / 2);
          }
        };
        const leftAdjust = barGap / 2;
        const getCenterLine = (d: Array<any>) => {
          return {
            y1: (d[upper_whisker]) ? yScale(d[upper_whisker]) : yScale(d[lower_quartile]),
            y2: (d[lower_whisker]) ? yScale(d[lower_whisker]) : yScale(d[upper_quartile])
          };
        };

        // Find the wrapper for this group
        const d3Group = d3.select(this.context.svg).node().selectAll(`g.group-wrap-${groupIndex}`);

        // Add wrapper for the boxes
        const boxesWrap = d3Group.selectAll('g.boxesWrap').data([group]);
        boxesWrap.enter().append('g')
          .attr('class', 'boxesWrap');

        let visibleBoxes = (group.disabled) ? [] : group.boxes.points;
        visibleBoxes = visibleBoxes.filter(box => {
          return box.length > 1;
        });

        // Add center lines first
        const center = boxesWrap.selectAll('line.center')
          .data(visibleBoxes);
        center.enter().insert('line', 'rect')
          .attr('class', 'center')
          .attr('stroke', this.context.fontColor)
          .attr('x1', d => xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) + (getBoxWidth() / 2))
          .attr('x2', d => xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) + (getBoxWidth() / 2))
          .attr('y1', d => getCenterLine(d).y1)
          .attr('y2', d => getCenterLine(d).y2);
        center.exit().remove();
        center.transition()
          .attr('x1', d => xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) + (getBoxWidth() / 2))
          .attr('x2', d => xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) + (getBoxWidth() / 2))
          .attr('y1', d => getCenterLine(d).y1)
          .attr('y2', d => getCenterLine(d).y2);

        // Add individual boxes
        const minBoxHeight = 3;
        const boxes = boxesWrap.selectAll('.box')
          .data(visibleBoxes);
        boxes.enter().append('rect')
          .attr('class', 'box')
          .attr('x', d => xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex))
          .attr('y', d => {
            let y = yScale(d[upper_quartile]);
            const mean = yScale(d[median]);
            if ((mean - y) < minBoxHeight) {
              y = mean - minBoxHeight;
            }
            return y;
          })
          .attr('width', getBoxWidth())
          .attr('height', d => {
            const mean = yScale(d[median]);
            let y1 = (d[lower_quartile] === null) ? mean + minBoxHeight : yScale(d[lower_quartile]);
            let y2 = (d[upper_quartile] === null) ? mean - minBoxHeight : yScale(d[upper_quartile]);
            if ((y1 - mean) < minBoxHeight) {
              y1 = mean + minBoxHeight;
            }
            if ((mean - y2) < minBoxHeight) {
              y2 = mean - minBoxHeight;
            }
            return y1 - y2;
          })
          .attr('fill', d => {
            if (group.boxes.colorXValues && group.boxes.colorXValues[d[0]]) {
              return group.boxes.colorXValues[d[0]];
            }
            return color;
          })
          .attr('opacity', 1)
          .attr('stroke', '#000');
        boxes.exit().remove();
        boxes.transition()
          .attr('x', d => xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex))
          .attr('y', d => {
            let y = yScale(d[upper_quartile]);
            const mean = yScale(d[median]);
            if ((mean - y) < minBoxHeight) {
              y = mean - minBoxHeight;
            }
            return y;
          })
          .attr('width', getBoxWidth())
          .attr('height', d => {
            const mean = yScale(d[median]);
            let y1 = (d[lower_quartile] === null) ? mean + minBoxHeight : yScale(d[lower_quartile]);
            let y2 = (d[upper_quartile] === null) ? mean - minBoxHeight : yScale(d[upper_quartile]);
            if ((y1 - mean) < minBoxHeight) {
              y1 = mean + minBoxHeight;
            }
            if ((mean - y2) < minBoxHeight) {
              y2 = mean - minBoxHeight;
            }
            return y1 - y2;
          })
          .attr('fill', d => {
            if (group.boxes.colorXValues && group.boxes.colorXValues[d[0]]) {
              return group.boxes.colorXValues[d[0]];
            }
            return color;
          });

        const faders = boxesWrap.selectAll('.boxfade')
          .data(visibleBoxes);
        faders.enter().append('rect')
          .attr('class', function(d: any) {
            d.box = this;
            return 'boxfade';
          })
          .attr('x', d => xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex))
          .attr('y', d => {
            let y = yScale(d[upper_quartile]);
            const mean = yScale(d[median]);
            if ((mean - y) < minBoxHeight) {
              y = mean - minBoxHeight;
            }
            return y;
          })
          .attr('width', getBoxWidth())
          .attr('height', d => {
            const mean = yScale(d[median]);
            let y1 = (d[lower_quartile] === null) ? mean + minBoxHeight : yScale(d[lower_quartile]);
            let y2 = (d[upper_quartile] === null) ? mean - minBoxHeight : yScale(d[upper_quartile]);
            if ((y1 - mean) < minBoxHeight) {
              y1 = mean + minBoxHeight;
            }
            if ((mean - y2) < minBoxHeight) {
              y2 = mean - minBoxHeight;
            }
            return y1 - y2;
          })
          .attr('fill', d => {
            if (group.boxes.selectXValues && group.boxes.selectXValues.includes(d[0])) {
              return 'url(#pattern-cubes)';
            }
            return '#fff';
          })
          .attr('opacity', d => {
            if (group.boxes.selectXValues && group.boxes.selectXValues.includes(d[0])) {
              return 1;
            }
            return fadeOpacity;
          })
          .attr('stroke', 'none');
        faders.exit().remove();
        faders.transition()
          .attr('x', d => xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex))
          .attr('y', d => {
            let y = yScale(d[upper_quartile]);
            const mean = yScale(d[median]);
            if ((mean - y) < minBoxHeight) {
              y = mean - minBoxHeight;
            }
            return y;
          })
          .attr('width', getBoxWidth())
          .attr('height', d => {
            const mean = yScale(d[median]);
            let y1 = (d[lower_quartile] === null) ? mean + minBoxHeight : yScale(d[lower_quartile]);
            let y2 = (d[upper_quartile] === null) ? mean - minBoxHeight : yScale(d[upper_quartile]);
            if ((y1 - mean) < minBoxHeight) {
              y1 = mean + minBoxHeight;
            }
            if ((mean - y2) < minBoxHeight) {
              y2 = mean - minBoxHeight;
            }
            return y1 - y2;
          })
          .attr('fill', d => {
            if (group.boxes.selectXValues && group.boxes.selectXValues.includes(d[0])) {
              return 'url(#pattern-cubes)';
            }
            return '#fff';
          })
          .attr('opacity', d => {
            if (group.boxes.selectXValues && group.boxes.selectXValues.includes(d[0])) {
              return 1;
            }
            return fadeOpacity;
          });
        faders
          .on('click', (event: any, d: any) => {
            const _xScale = data.scales[data.groups[d.groupIndex].xScale];
            const _yScale = data.scales[data.groups[d.groupIndex].yScale];
            const barData: any = {
              box_data: d,
              scale_label: _xScale.scale(d[label]),
              scale_upper_quartile: _yScale.scale(d[upper_quartile]),
              scale_lower_quartile: _yScale.scale(d[lower_quartile]),
              scale_median: _yScale.scale(d[median]),
              scale_upper_whisker: _yScale.scale(d[upper_whisker]),
              scale_lower_whisker: _yScale.scale(d[lower_whisker]),
              scale_outliers: [],
              group: d.groupIndex,
              box: d.boxIndex
            };
            for (let y = 6; y < d.length; y++) {
              barData.scale_outliers.push(_yScale.scale(d[y]));
            }
            this.context.boxClick.emit(barData);
          });

        // Add median line
        const medianLine = boxesWrap.selectAll('line.median')
          .data(visibleBoxes);
        medianLine.enter().append('line')
          .attr('class', 'median')
          .attr('stroke', '#000')
          .attr('x1', d => (d[median]) ? xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) + 1 : null)
          .attr('x2', d => (d[median]) ? xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) + getBoxWidth() - 1 : null)
          .attr('y1', d => (d[median]) ? yScale(d[median]) : null)
          .attr('y2', d => (d[median]) ? yScale(d[median]) : null);
        medianLine.exit().remove();
        medianLine.transition()
          .attr('x1', d => (d[median]) ? xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) + 1 : null)
          .attr('x2', d => (d[median]) ? xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) + getBoxWidth() - 1 : null)
          .attr('y1', d => (d[median]) ? yScale(d[median]) : null)
          .attr('y2', d => (d[median]) ? yScale(d[median]) : null);

        // Add upper quartile whisker
        const whiskerTop = boxesWrap.selectAll('line.whiskertop')
          .data(visibleBoxes);
        whiskerTop.enter().append('line')
          .attr('class', 'whiskertop')
          .attr('stroke', this.context.fontColor)
          .attr('x1', d => (d[upper_whisker]) ? xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) : null)
          .attr('x2', d => (d[upper_whisker]) ? xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) + getBoxWidth() : null)
          .attr('y1', d => (d[upper_whisker]) ? yScale(d[upper_whisker]) : null)
          .attr('y2', d => (d[upper_whisker]) ? yScale(d[upper_whisker]) : null);
        whiskerTop.exit().remove();
        whiskerTop.transition()
          .attr('x1', d => (d[upper_whisker]) ? xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) : null)
          .attr('x2', d => (d[upper_whisker]) ? xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) + getBoxWidth() : null)
          .attr('y1', d => (d[upper_whisker]) ? yScale(d[upper_whisker]) : null)
          .attr('y2', d => (d[upper_whisker]) ? yScale(d[upper_whisker]) : null);

        // Add lower quartile whisker
        const whiskerBottom = boxesWrap.selectAll('line.whiskerbottom')
          .data(visibleBoxes);
        whiskerBottom.enter().append('line')
          .attr('class', 'whiskerbottom')
          .attr('stroke', this.context.fontColor)
          .attr('x1', d => (d[lower_whisker]) ? xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) : null)
          .attr('x2', d => (d[lower_whisker]) ? xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) + getBoxWidth() : null)
          .attr('y1', d => (d[lower_whisker]) ? yScale(d[lower_whisker]) : null)
          .attr('y2', d => (d[lower_whisker]) ? yScale(d[lower_whisker]) : null);
        whiskerBottom.exit().remove();
        whiskerBottom.transition()
          .attr('x1', d => (d[lower_whisker]) ? xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) : null)
          .attr('x2', d => (d[lower_whisker]) ? xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) + getBoxWidth() : null)
          .attr('y1', d => (d[lower_whisker]) ? yScale(d[lower_whisker]) : null)
          .attr('y2', d => (d[lower_whisker]) ? yScale(d[lower_whisker]) : null);

        // Add outlier points
        const outlierIndices = [];
        visibleBoxes.forEach((points: Array<any>) => {
          if (points.length > 6) {
            for (let y = 6; y < points.length; y++) {
              outlierIndices.push([points[label], points[y]]);
            }
          }
        });
        const outlier = boxesWrap.selectAll('circle.outlier')
          .data(outlierIndices);
        outlier.enter().append('circle')
          .attr('class', 'outlier')
          .attr('fill', d => {
            if (group.boxes.colorXValues && group.boxes.colorXValues[d[0]]) {
              return group.boxes.colorXValues[d[0]];
            }
            return color;
          })
          .attr('stroke', this.context.fontColor)
          .attr('r', 5)
          .attr('cx', d => xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) + (getBoxWidth() / 2))
          .attr('cy', d => yScale(d[1]));
        outlier.exit().remove();
        outlier.transition()
          .attr('cx', d => xScale(d[label]) + leftAdjust + getBoxLeft(visibleIndex) + (getBoxWidth() / 2))
          .attr('cy', d => yScale(d[1]));

      }
    });
  }

  drawVoronoi(data: Model.ChartData) {
    const that = this;
    // Build the voronoi map
    const allPoints: Array<any> = d3.merge(
      data.groups.map(group => {
        if (group.disabled) {
          return [];
        }
        const boxPoints = [];
        if (group.boxes && group.boxes.points) {
          group.boxes.points.forEach(box => {
            for (let i = 1; i < box.length; i++) {
              const newPoint: any = [box[0], box[i]];
              newPoint.groupIndex = box.groupIndex;
              newPoint.boxIndex = box.boxIndex;
              newPoint.pointIndex = i - 1;
              newPoint.isOutlier = (i > 4);
              boxPoints.push(newPoint);
            }
          });
        }
        return boxPoints;
      })
    );

    const scaledPoints: any[] = [];
    allPoints.forEach(point => {
      const xScale = data.scales[data.groups[point.groupIndex].xScale];
      const yScale = data.scales[data.groups[point.groupIndex].yScale];
      const bandAdjust = (xScale.type === D3ScaleType.Band) ? xScale.scale.bandwidth() / 2 : 0;
      const scaledX = xScale.scale(point[0]) + bandAdjust + (Math.random() * 1e-3);
      // IMPORTANT: Need to trap null y values, so they don't change to NaN and throw off the voronoi functions.
      const scaledY = (point[1] !== null) ? yScale.scale(point[1]) + (Math.random() * 1e-3) : null;
      scaledPoints.push([scaledX, scaledY]);
    });

    const delaunay = d3.Delaunay.from(scaledPoints);
    const voronoi = delaunay.voronoi([0, 0, this.context.dimensions.width - this.context.margin.left - this.context.margin.right,
      this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom]);
    const cells = allPoints.map((d, i) => [d, voronoi.cellPolygon(i)]);
    const cellRemap = cells.map(cell => {
      const polygon = cell[1];
      polygon.data = cell[0];
      return polygon;
    });
  
    this.context.svg.select('g.voronoi.boxes').remove();
    const voronoiGroup = this.context.svg.select('g.dataWrap').append('g')
      .attr('class', 'voronoi boxes');
    voronoiGroup.selectAll('path')
      .data(cellRemap)
      .enter().append('path')
      .attr('fill', 'none')
      .attr('d', (d) => d ? 'M' + d.join('L') + 'Z' : null)
      .attr('datax', (d: any) => {
        return (d) ? d.data[0] : '';
      })
      .attr('datay', (d: any) => {
        return (d) ? d.data[1] : '';
      })
      .on('mouseover', function(event: any, d: any) {
        that.context.svg.selectAll('.hover').classed('hover', false);
        that.context.focusPoint = d;
        if (d.data.circle) {
          d3.select(d.data.circle)
            .classed('hover', true);
        }
        if (
          (d.data.isOutlier && data.groups[d.data.groupIndex].boxes.hideOutlierLabels) ||
          (!d.data.isOutlier && data.groups[d.data.groupIndex].boxes.hideLabels)
        ) {
          const _xScale = data.scales[data.groups[d.data.groupIndex].xScale];
          const bandAdjust = (_xScale.type === D3ScaleType.Band) ?
            _xScale.scale.bandwidth() / 2 : 0;
          that.context.showFocusNote(data, d, false, true, bandAdjust, this, true);
        }
      })
      .on('mouseout', (event: any, d: any) => {
        this.context.focusPoint = null;
        // Allow mouseover event for text to fire first
        setTimeout(() => {
          if (!this.context.noteHasFocus) {
            if (this.context.focusPoint !== d) {
              if (d.data.circle &&
                !d3.select(d.data.circle).classed('signal')) {
                d3.select(d.data.circle)
                  .classed('hover', false);
              }
            }
            if (this.context.focusPoint === null) {
              this.context.hideFocusNote();
            }
          }
        });
      })
      .on('click', function(event: any, d: any) {
        if (that.context.miniChart) {
          if (d.data.circle) {
            d3.select(d.data.circle)
              .classed('hover', true);
            const _xScale = data.scales[data.groups[d.data.groupIndex].xScale];
            const bandAdjust = (_xScale.type === D3ScaleType.Band) ?
              _xScale.scale.bandwidth() / 2 : 0;
            that.context.showFocusNote(data, d, false, true, bandAdjust, this, true);
          }
        } else {
          if (d.data.circle) {
            that.context.svg.selectAll('.selected').classed('selected', false);
            d3.select(d.data.circle)
              .classed('selected', true);
          }
        }
        const xScale = data.scales[data.groups[d.data.groupIndex].xScale];
        const yScale = data.scales[data.groups[d.data.groupIndex].yScale];
        const pointData: any = {
          x: d.data[0],
          y: d.data[1],
          altText: (d.data[2]) ? d.data[2] : null,
          filter: (d.data.filter) ? d.data.filter : null,
          scaleX: xScale.scale(d.data[0]),
          scaleY: yScale.scale(d.data[1]),
          groupIndex: d.data.groupIndex,
          boxIndex: d.data.boxIndex,
          pointIndex: d.data.pointIndex,
          d3mouse: d3.pointer(event),
          d3event: event,
          voronoiElement: this
        };
        that.context.pointClick.emit(pointData);
      })
      .exit().remove();

  }

}
