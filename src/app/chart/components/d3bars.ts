import * as d3 from 'd3';
import * as Model from './chart-data.model';

export class D3Bars {
  context: any;
  margin: any;
  dimensions: any;

  constructor(context: any) {
    this.context = context; // d3 will choke if svg is not referenced through the original context
  }

  clearBars(): void {
    d3.select(this.context.svg).node().selectAll('g.barsWrap').remove();
  }

  drawBars(data: Model.ChartData): void {
    const that = this;
    const fadeOpacity = (this.context.totalLineCount() > 0) ? 0.2 : 0;
    let visibleGroupCount = 0;
    let visibleIndex = 0;
    data.groups.forEach(group => {
      if (!group.disabled) {
        visibleGroupCount += 1;
      }
    });
    data.groups.forEach((group, groupIndex) => {
      if (!group.disabled) {
        visibleIndex += 1;
      }
      if (group.bars && group.bars.points && group.bars.points.length > 0) {
        // Get total count for calculating altLabels
        const groupBarTotal = group.bars.points
          .map(point => point[1])
          .reduce((a, b) => a + b);

        // Set indices for later location of points on svg
        group.bars.points = group.bars.points.map((point, pointIndex) => {
          point.groupIndex = groupIndex;
          point.pointIndex = pointIndex;
          if (group.bars.displayAsPercentage) {
            point.altLabel = this.context.formatForDisplay(point[1], data.decimals).toString();
          } else {
            point.altLabel = (this.context.formatForDisplay((point[1] * 100 / groupBarTotal), 1)).toString() + '%';
          }
          return point;
        });

        // Each group shares the x and y scales and color setting
        const xScale = data.scales[group.xScale].scale;
        const yScale = data.scales[group.yScale].scale;
        const color = (group.color) ? group.color : this.context.colorScheme(groupIndex);

        let barGap = 6;
        if (xScale.bandwidth() < 20) { barGap = 4; }
        if (xScale.bandwidth() < 10) { barGap = 0; }
        let barWidthBreakpoint = 50;
        if (xScale.bandwidth() > 66) {
          barWidthBreakpoint = Math.round(xScale.bandwidth() * .75);
        }
        const maxBarWidth = group.bars.maxBarWidth || barWidthBreakpoint + (25 * (visibleGroupCount - 1)) - barGap;
        const getBandWidth = () => {
          let bandwidth = xScale.bandwidth();
          if (bandwidth === 0) {
            const range = xScale.range();
            const domain = xScale.domain();
            if (range.length === 2 && domain.length >= 2) {
              bandwidth = Math.abs(range[1] - range[0]) / (domain.length - 1);
            }
          }
          return (bandwidth < maxBarWidth + barGap) ? bandwidth - barGap : maxBarWidth;
        };
        // getBarWidth adjusts the width of the bars based on the number of groups visible
        const getBarWidth = () => {
          return getBandWidth() * (2 / (visibleGroupCount + 1));
        };
        // getBarLeft helps to control overlapping the bars for different groups, shifting them left/right
        const getBarLeft = (barIndex: number) => {
          if (xScale.bandwidth() < maxBarWidth + barGap) {
            return getBandWidth() * (barIndex - 1) / (visibleGroupCount + 1);
          } else {
            return (getBandWidth() * (barIndex - 1) / (visibleGroupCount + 1)) + ((xScale.bandwidth() - barGap - getBandWidth()) / 2);
            // return (xScale.bandwidth() - barGap - getBandWidth()) / 2;
          }
        };
        const leftAdjust = barGap / 2;

        // Find the wrapper for this group
        const d3Group = d3.select(this.context.svg).node().selectAll(`g.group-wrap-${groupIndex}`);
        const isPareto = data.groups[0].bars && data.groups[0].lines && data.groups[0].lines[0];

        // Add wrapper for the bars
        const barsWrap = d3Group.selectAll('g.barsWrap').data([group]);
        barsWrap.enter().append('g')
          .attr('class', 'barsWrap');

        const visibleBars = (group.disabled) ? [] : group.bars.points;

        // Add individual bars
        const minBarHeight = 3;
        const bars = barsWrap.selectAll('.bar')
          .data(visibleBars);
        bars.enter().append('rect')
          .attr('class', 'bar')
          .attr('x', d => xScale(d[0]) + leftAdjust + getBarLeft(visibleIndex))
          .attr('y', d => {
            let y = yScale(d[1]);
            const yLimit = this.context.dimensions.height - this.context.margin.bottom - this.context.margin.top - minBarHeight;
            if (group.bars.displayAsPercentage) {
              y = yScale(d[1] * 100 / groupBarTotal);
            }
            if (y > yLimit) {
              y = yLimit;
            }
            return y;
          })
          .attr('width', getBarWidth())
          .attr('height', d => {
            let height = this.context.dimensions.height - yScale(d[1]) - this.context.margin.bottom - this.context.margin.top;
            if (group.bars.displayAsPercentage) {
              height = this.context.dimensions.height -
                yScale(d[1] * 100 / groupBarTotal) - this.context.margin.bottom - this.context.margin.top;
            }
            if (height < minBarHeight) {
              height = minBarHeight;
            }
            return height;
          })
          .attr('fill', (d: any) => {
            return (d.color) ? d.color : color;
          })
          .attr('opacity', 1)
          .attr('stroke', '#000');
        bars.exit().remove();
        bars.transition()
          .attr('x', d => xScale(d[0]) + leftAdjust + getBarLeft(visibleIndex))
          .attr('y', d => {
            let y = yScale(d[1]);
            const yLimit = this.context.dimensions.height - this.context.margin.bottom - this.context.margin.top - minBarHeight;
            if (group.bars.displayAsPercentage) {
              y = yScale(d[1] * 100 / groupBarTotal);
            }
            if (y > yLimit) {
              y = yLimit;
            }
            return y;
          })
          .attr('width', getBarWidth())
          .attr('height', d => {
            let height = this.context.dimensions.height - yScale(d[1]) - this.context.margin.bottom - this.context.margin.top;
            if (group.bars.displayAsPercentage) {
              height = this.context.dimensions.height -
                yScale(d[1] * 100 / groupBarTotal) - this.context.margin.bottom - this.context.margin.top;
            }
            if (height < minBarHeight) {
              height = minBarHeight;
            }
            return height;
          })
          .attr('fill', (d: any) => {
            return (d.color) ? d.color : color;
          });

        // Add fader overlay (for showing underneath lines with same color)
        const faders = barsWrap.selectAll('.barfade')
          .data(visibleBars);
        faders.enter().append('rect')
          .attr('class', function(d: any): string {
            d.bar = this;
            return 'barfade';
          })
          .attr('x', d => xScale(d[0]) + leftAdjust + getBarLeft(visibleIndex))
          .attr('y', d => {
            let y = yScale(d[1]);
            const yLimit = this.context.dimensions.height - this.context.margin.bottom - this.context.margin.top - minBarHeight;
            if (group.bars.displayAsPercentage) {
              y = yScale(d[1] * 100 / groupBarTotal);
            }
            if (y > yLimit) {
              y = yLimit;
            }
            return y;
          })
          .attr('width', getBarWidth())
          .attr('height', d => {
            let height = this.context.dimensions.height - yScale(d[1]) - this.context.margin.bottom - this.context.margin.top;
            if (group.bars.displayAsPercentage) {
              height = this.context.dimensions.height -
                yScale(d[1] * 100 / groupBarTotal) - this.context.margin.bottom - this.context.margin.top;
            }
            if (height < minBarHeight) {
              height = minBarHeight;
            }
            return height;
          })
          .attr('fill', d => {
            if (group.bars.selectXValues && group.bars.selectXValues.includes(d[0])) {
              return 'url(#pattern-cubes)';
            }
            return '#fff';
          })
          .attr('opacity', d => {
            if (group.bars.selectXValues && group.bars.selectXValues.includes(d[0])) {
              return 1;
            }
            return fadeOpacity;
          })
          .attr('stroke', 'none');
        faders.exit().remove();
        faders.transition()
          .attr('x', d => xScale(d[0]) + leftAdjust + getBarLeft(visibleIndex))
          .attr('y', d => {
            let y = yScale(d[1]);
            const yLimit = this.context.dimensions.height - this.context.margin.bottom - this.context.margin.top - minBarHeight;
            if (group.bars.displayAsPercentage) {
              y = yScale(d[1] * 100 / groupBarTotal);
            }
            if (y > yLimit) {
              y = yLimit;
            }
            return y;
          })
          .attr('width', getBarWidth())
          .attr('height', d => {
            let height = this.context.dimensions.height - yScale(d[1]) - this.context.margin.bottom - this.context.margin.top;
            if (group.bars.displayAsPercentage) {
              height = this.context.dimensions.height -
                yScale(d[1] * 100 / groupBarTotal) - this.context.margin.bottom - this.context.margin.top;
            }
            if (height < minBarHeight) {
              height = minBarHeight;
            }
            return height;
          })
          .attr('fill', d => {
            if (group.bars.selectXValues && group.bars.selectXValues.includes(d[0])) {
              return 'url(#pattern-cubes)';
            }
            return '#fff';
          })
          .attr('opacity', d => {
            if (group.bars.selectXValues && group.bars.selectXValues.includes(d[0])) {
              return 1;
            }
            return fadeOpacity;
          });
        barsWrap.selectAll('rect.barfade')
          .on('mouseover', function(event: any, d: any): void {
            that.context.svg.selectAll('.hover').classed('hover', false);
            that.context.focusPoint = d;
            if (getBarWidth() < 40 || visibleGroupCount > 1 || isPareto || data.hideBarLabels) {
              d3.select(d.bar).classed('hover', true);
              const bandAdjust = leftAdjust + getBarLeft(d.groupIndex) + (getBarWidth() / 2);
              that.context.showFocusNote(data, {data: d}, group.bars.showAltLabel, false, bandAdjust, this);
            }
          })
          .on('click', (event: any, d: any) => {
            if (this.context.miniChart) {
              if (getBarWidth() < 40 || visibleGroupCount > 1 || isPareto || data.hideBarLabels) {
                d3.select(d.bar).classed('hover', true);
                const bandAdjust = leftAdjust + getBarLeft(d.groupIndex) + (getBarWidth() / 2);
                this.context.showFocusNote(data, {data: d}, group.bars.showAltLabel, false, bandAdjust);
              }
            }
            const barData: any = {
              x: d[0],
              y: d[1],
              scaleX: data.scales[data.groups[d.groupIndex].xScale].scale(d[0]),
              scaleY: data.scales[data.groups[d.groupIndex].yScale].scale(d[1]),
              group: d.groupIndex,
              point: d.pointIndex,
              altLabel: d.altLabel
            };
            this.context.barClick.emit(barData);
          })
          .on('mouseout', (event: any, d: any) => {
            this.context.focusPoint = null;
            // Allow mouseover event for text to fire first
            setTimeout(() => {
              if (!this.context.noteHasFocus) {
                if (this.context.focusPoint !== d) {
                  d3.select(d.bar).classed('hover', false);
                }
                if (this.context.focusPoint === null) {
                  this.context.hideFocusNote();
                }
              }
            });
          });

      }
    });
  }

}
