import * as d3 from 'd3';
import * as Model from './chart-data.model';

export class D3Legend {
  context: any;
  margin: any;
  dimensions: any;

  constructor(context: any) {
    this.context = context; // d3 will choke if svg is not referenced through the original context
  }

  drawLegend(data: Model.ChartData, titleHeight: number, titleWidth: number): any {
    data.groups.forEach((group, index) => {
      (group as any).index = index;
    });

    this.context.svg.selectAll('g.legendWrap').remove();
    const legendWrap = this.context.svg.append('g')
      .attr('class', 'legendWrap')
      .attr('dominant-baseline', 'hanging');

    const series = legendWrap.selectAll('.series')
      .data(data.groups);
    const seriesEnter = series.enter()
      .append('g')
      .attr('class', 'series')
      .classed('disabled', d => d.disabled)
      .on('click', (event: any, d) => {
        if (data.singleLegendMode) {
          data.groups.forEach((group, groupIndex) => {
            group.disabled = (groupIndex === d.index) ? false : true;
          });
        } else {
          data.groups[d.index].disabled = !data.groups[d.index].disabled;
          let oneStillOn = false;
          data.groups.forEach((group) => {
            if (!group.disabled) {
              oneStillOn = true;
            }
          });
          if (!oneStillOn) {
            data.groups[d.index].disabled = false;
          }
        }
        this.context.legendClick.emit(data);
        this.context.redrawChart();
      })
      .on('mouseout', (event: any, d) => {
        const dataGroup = this.context.svg.select(`g.group-wrap-${d.index}`);
        dataGroup.classed('hover', false);
      })
      .on('mouseover', (event: any, d) => {
        const dataGroup = this.context.svg.select(`g.group-wrap-${d.index}`);
        dataGroup.classed('hover', true);
      });
    seriesEnter.append('circle')
      .style('fill', (d: any, i) => data.groups[i].color || this.context.colorScheme(i))
      .style('stroke', (d: any, i) => data.groups[i].color || this.context.colorScheme(i))
      .attr('r', 5);
    seriesEnter.append('text')
      .text(d => d.label)
      .attr('text-anchor', 'start')
      .attr('dy', '-.38em')
      .attr('dx', '10')
      .style('font-size', this.context.legendFontSize);
    series.exit().remove();

    let x = 0;
    let y = 0;
    const maxWidth = this.context.dimensions.width - this.context.margin.left - this.context.margin.right;
    let currentWidth = 0;
    // if (data.titleAlign === Model.ChartTitleAlign.Left) {
    //   maxWidth = this.context.dimensions.width - this.context.margin.left - this.context.margin.right - (titleWidth * 2) - 10;
    // } else if (data.titleAlign === Model.ChartTitleAlign.Right) {
    //   maxWidth = this.context.dimensions.width - this.context.margin.left - this.context.margin.right - (titleWidth * 2);
    // }
    seriesEnter
      .attr('transform', function(): string {
        const node: any = d3.select(this).select('text').node();
        const newWidth = node.getComputedTextLength() + 28;
        x = currentWidth;
        if (currentWidth + newWidth > maxWidth) {
          x = 0;
          y += 20;
          currentWidth = newWidth;
        } else {
          currentWidth += newWidth;
        }
        return `translate(${x},${y})`;
      });

    const box = legendWrap.node().getBBox();
    const translateX = (this.context.dimensions.width + this.context.margin.left - box.width) / 2;
    // Start with setting for left/right aligned title
    let translateY = (titleHeight + box.height) / 2;
    // If title is center aligned, or collides with legend, shift it down.
    if (!data.titleAlign || titleWidth + box.width / 2 > maxWidth / 2) {
      translateY = titleHeight + 10 + (box.height / 2);
    }
    legendWrap.attr('transform', `translate(${translateX},${translateY})`);

    return box;
  }
}
