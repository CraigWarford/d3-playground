import * as d3 from 'd3';
import * as Model from './chart-data.model';
import { D3ScaleType } from './enums';

export class D3Tethers {
  context: any;
  margin: any;
  dimensions: any;

  private dotRadius = 2.5;

  constructor(context: any) {
    this.context = context; // d3 will choke if svg is not referenced through the original context
  }

  drawTethers(data: Model.ChartData) {
    let visibleGroupCount = 0;
    let visibleIndex = 0;
    data.groups.forEach(group => {
      if (!group.disabled) {
        visibleGroupCount += 1;
      }
    });
    data.groups.forEach((group, groupIndex) => {
      if (typeof group.xScale === 'undefined' || isNaN(group.xScale) ||
        typeof group.yScale === 'undefined' || isNaN(group.yScale)) {
        return;
      }
      if (!group.disabled) {
        visibleIndex += 1;
      }
      // Each group shares the x and y scales and color setting
      const xScale = data.scales[group.xScale].scale;
      const yScale = data.scales[group.yScale].scale;
      // const color = this.context.colorScheme(groupIndex);
      let barGap = (xScale.bandwidth) ? 6 : 0;
      if (xScale.bandwidth && xScale.bandwidth() < 20) { barGap = 4; }
      if (xScale.bandwidth && xScale.bandwidth() < 10) { barGap = 0; }
      const maxBoxWidth = 50 + (25 * (visibleGroupCount - 1)) - barGap;
      const getBandWidth = () => {
        if (!xScale.bandwidth) {
          return 0;
        }
        return (xScale.bandwidth() < maxBoxWidth + barGap) ? xScale.bandwidth() - barGap : maxBoxWidth;
      };
      const getBoxWidth = () => {
        if (!xScale.bandwidth) {
          return 0;
        }
        return getBandWidth() * (2 / (visibleGroupCount + 1));
      };
      const getBoxLeft = (boxIndex: number) => {
        if (!xScale.bandwidth) {
          return maxBoxWidth / 2;
        }
        if (xScale.bandwidth() < maxBoxWidth + barGap) {
          return getBandWidth() * (boxIndex - 1) / (visibleGroupCount + 1);
        } else {
          return (getBandWidth() * (boxIndex - 1) / (visibleGroupCount + 1)) + ((xScale.bandwidth() - getBandWidth()) / 2);
        }
      };
      const getBandAdjust = () => {
        let bandAdjust = 0;
        if (data.scales[group.xScale].type === D3ScaleType.Band) {
          bandAdjust += (barGap / 2) + getBoxLeft(visibleIndex) + (getBoxWidth() / 2);
        }
        return bandAdjust;
      };

      // Find the wrapper for this group
      const d3Group = d3.select(this.context.svg).node().selectAll(`g.group-wrap-${groupIndex}`);

      const visibleNotes = (group.disabled) ? [] : group.notes || [];

      // Add wrapper for the tethers
      const tethersWrap = d3Group.selectAll('g.tethersWrap').data([visibleNotes]);
      tethersWrap.enter().append('g')
        .attr('class', 'tethersWrap');

      // Add individual tethers
      const tethers = tethersWrap.selectAll('.tether')
        .data(visibleNotes);
      tethers.enter().append('g')
        .style('stroke-opacity', 1)
        .style('fill', this.context.fontColor)
        .style('stroke', this.context.fontColor)
        .attr('class', (d, i) => 'tether tether-' + i);
      tethers.exit().transition()
        .style('stroke-opacity', 1e-6)
        .remove();
      tethers
        .style('fill', this.context.fontColor)
        .style('stroke', this.context.fontColor)
        .style('stroke-opacity', 1);

      // Add svg paths to the tether
      const paths = tethersWrap.selectAll('.tether').selectAll('path')
        .data((d: any) => {
          return [d];
        });
      paths.enter().append('path')
        .attr('d', (d) => {
          const pointX = (d.lockToYAxis) ? 0 : xScale(d.point[0]) + getBandAdjust();
          const pointY = (d.lockToXAxis) ?
            this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom :
            yScale(d.point[1]);
          const boxX = (!d.lockToYAxis && d.shiftLocked) ?
            xScale(d.point[0]) + getBandAdjust() :
            xScale(d.point[0]) + getBandAdjust() + d.boxOffsetLeft;
          const boxY = (d.lockToYAxis && d.shiftLocked) ?
            yScale(d.point[1]) :
            yScale(d.point[1]) + d.boxOffsetTop;
          return `M${pointX},${pointY}L${boxX},${boxY}`;
        });
      paths.exit().remove();
      paths
        .attr('d', (d) => {
          const pointX = (d.lockToYAxis) ? 0 : xScale(d.point[0]) + getBandAdjust();
          const pointY = (d.lockToXAxis) ?
            this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom :
            yScale(d.point[1]);
          const boxX = (!d.lockToYAxis && d.shiftLocked) ?
            xScale(d.point[0]) + getBandAdjust() :
            xScale(d.point[0]) + getBandAdjust() + d.boxOffsetLeft;
          const boxY = (d.lockToYAxis && d.shiftLocked) ?
            yScale(d.point[1]) :
            yScale(d.point[1]) + d.boxOffsetTop;
          return `M${pointX},${pointY}L${boxX},${boxY}`;
        });
      // paths
      // .attr('d', (d) => {
      //   const pointX = (d.lockToYAxis) ? 0 : xScale(d.point[0]) + getBandAdjust();
      //   const pointY = (d.lockToXAxis) ?
      //     this.context.dimensions.height - this.context.margin.top - this.context.margin.bottom :
      //     yScale(d.point[1]);
      //   let boxX = xScale(d.point[0]) + d.boxOffsetLeft + (d.clientWidth / 2);
      //   if (boxX + (d.clientWidth / 2) > this.context.dimensions.width - this.context.margin.right) {
      //     boxX = this.context.dimensions.width - this.context.margin.right - d.clientWidth;
      //   }
      //   const boxY = yScale(d.point[1]) + d.boxOffsetTop + (d.clientHeight / 2);
      //   return `M${pointX},${pointY}L${boxX},${boxY}`;
      // });

    });
  }

}
