import { Component, OnInit, OnChanges, Input, Output, ViewChild, ElementRef,
  EventEmitter, Renderer2, AfterViewChecked } from '@angular/core';

import * as _ from 'lodash';
import * as d3 from 'd3';
import * as Model from './components';
import * as shortid from 'shortid';
import { D3Legend, D3Axes, D3Bars, D3Boxes, D3Tethers, D3Lines, D3Labels } from './components';
import { D3Pie } from './components/d3pie';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit, OnChanges, AfterViewChecked {
  @ViewChild('chart', { static: true }) private domChartRef: ElementRef;
  @Input() private lockAspect = true;
  @Input() private aspect = .5;
  @Input() public data: Model.ChartData;
  @Input() public miniChart = false;
  @Input() public margin = {top: 5, right: 30, bottom: 0, left: 30};
  @Input() public minMargin = {top: 0, right: 0, bottom: 50, left: 0};

  @Output() axisClick: EventEmitter<any> = new EventEmitter();
  @Output() pointClick: EventEmitter<any> = new EventEmitter();
  @Output() barClick: EventEmitter<any> = new EventEmitter();
  @Output() boxClick: EventEmitter<any> = new EventEmitter();
  @Output() legendClick: EventEmitter<any> = new EventEmitter();
  @Output() noteClick: EventEmitter<any> = new EventEmitter();
  @Output() noteMove: EventEmitter<any> = new EventEmitter();
  @Output() noteDelete: EventEmitter<any> = new EventEmitter();
  @Output() thresholdClick: EventEmitter<any> = new EventEmitter();
  @Output() thresholdMove: EventEmitter<any> = new EventEmitter();
  @Output() thresholdDelete: EventEmitter<any> = new EventEmitter();
  @Output() dataChange: EventEmitter<any> = new EventEmitter();
  @Output() colorSet: EventEmitter<any> = new EventEmitter();
  @Output() renderComplete: EventEmitter<any> = new EventEmitter();
  @Output() updateSliderX: EventEmitter<any> = new EventEmitter();
  @Output() titleChange: EventEmitter<any> = new EventEmitter();
  @Output() axisTitleChange: EventEmitter<any> = new EventEmitter();

  private fullMargin = {top: 5, right: 30, bottom: 0, left: 30};
  private miniChartMargin = {top: 5, right: 15, bottom: 0, left: 30};
  private domChart: HTMLElement;
  private chartInitialized = false;
  private holdData: Model.ChartData = null;
  private holdMiniChart = false;
  public dimensions: any = {};
  public svg: any;
  public svgStyle: any;
  private clipPathId: string = null;

  private d3Legend: D3Legend;
  private d3Axes: D3Axes;
  private d3Bars: D3Bars;
  private d3Boxes: D3Boxes;
  private d3Pie: D3Pie;
  private d3Tethers: D3Tethers;
  private d3Lines: D3Lines;
  private d3Labels: D3Labels;
  // private colors: any[] = [
  //   '#5194E3', '#6BA751', '#D1BA49',
  //   '#658DD7', '#54A1A0', '#AFB44C',
  //   '#7986CB', '#499EC8', '#8DAD4E',
  //   '#154989'];
  private colors: any[] = [
    '#5194E3', '#6BA751', '#D1BA49',
    '#7986CB', '#499EC8', '#8DAD4E',
    '#54A1A0', '#AFB44C', '#658DD7',
    '#154989'];
  private colorScheme: any;
  private fontFamily = '';
  private fontColor = '#000';
  private backgroundColor = '#fff';
  private titleFontSize = '18px';
  private subtitleFontSize = '14px';
  private legendFontSize = '14px';
  private axisFontSize = '14px';
  private labelFontSize = '14px';
  private lineLabelFontSize = '12px';
  private hoverFontSize = '12px';
  private annotationFontSize = '14px';

  private dataWrap: any = null;
  private noteWrap: any = null;
  focusnote: any = null;
  focusPoint: any = null;
  noteHasFocus = false;

  private dragNode = null;
  private dragStartX = 0;
  private dragStartY = 0;

  constructor(private renderer: Renderer2) {
    // Instantiate component instances for building charts
    this.d3Legend = new D3Legend(this);
    this.d3Axes = new D3Axes(this);
    this.d3Bars = new D3Bars(this);
    this.d3Boxes = new D3Boxes(this);
    this.d3Pie = new D3Pie(this);
    this.d3Tethers = new D3Tethers(this);
    this.d3Lines = new D3Lines(this);
    this.d3Labels = new D3Labels(this);
    this.getStyles();
    this.clipPathId = 'myClipPath-' + shortid.generate();
  }

  getStyles(): void {
    // Check DOM for CSS elements that define output styles (allows CSS control of appearance)
    const swatches = document.getElementsByClassName('d3-color');
    if (swatches.length > 0) {
      this.colors = [];
      Array.from(swatches).filter((node: HTMLElement) => {
        const style = window.getComputedStyle(node);
        this.colors.push(style.color);
      });
    }
    this.colorScheme = d3.scaleOrdinal(this.colors);
    const d3FontStyle = document.getElementsByClassName('d3-font-family');
    if (d3FontStyle.length > 0) {
      Array.from(d3FontStyle).filter((node: HTMLElement) => {
        const style = window.getComputedStyle(node);
        this.fontFamily = style.fontFamily;
        this.fontColor = style.color;
        this.backgroundColor = style.backgroundColor;
      });
    }
    // const d3TitleStyle = document.getElementsByClassName('d3-title-style');
    // if (d3TitleStyle.length > 0) {
    //   Array.from(d3TitleStyle).filter((node: HTMLElement) => {
    //     const style = window.getComputedStyle(node);
    //     this.titleFontSize = style.fontSize;
    //   });
    // }
    // const d3SubtitleStyle = document.getElementsByClassName('d3-subtitle-style');
    // if (d3SubtitleStyle.length > 0) {
    //   Array.from(d3TitleStyle).filter((node: HTMLElement) => {
    //     const style = window.getComputedStyle(node);
    //     this.subtitleFontSize = style.fontSize;
    //   });
    // }
    // const d3LegendStyle = document.getElementsByClassName('d3-legend-style');
    // if (d3LegendStyle.length > 0) {
    //   Array.from(d3LegendStyle).filter((node: HTMLElement) => {
    //     const style = window.getComputedStyle(node);
    //     this.legendFontSize = style.fontSize;
    //   });
    // }
    // const d3AxisStyle = document.getElementsByClassName('d3-axis-style');
    // if (d3AxisStyle.length > 0) {
    //   Array.from(d3AxisStyle).filter((node: HTMLElement) => {
    //     const style = window.getComputedStyle(node);
    //     this.axisFontSize = style.fontSize;
    //   });
    // }
    // const d3LabelStyle = document.getElementsByClassName('d3-label-style');
    // if (d3LabelStyle.length > 0) {
    //   Array.from(d3LabelStyle).filter((node: HTMLElement) => {
    //     const style = window.getComputedStyle(node);
    //     this.labelFontSize = style.fontSize;
    //   });
    // }
    // const d3LineStyle = document.getElementsByClassName('d3-line-style');
    // if (d3LineStyle.length > 0) {
    //   Array.from(d3LineStyle).filter((node: HTMLElement) => {
    //     const style = window.getComputedStyle(node);
    //     this.lineLabelFontSize = style.fontSize;
    //   });
    // }
    // const d3HoverStyle = document.getElementsByClassName('d3-hover-style');
    // if (d3HoverStyle.length > 0) {
    //   Array.from(d3HoverStyle).filter((node: HTMLElement) => {
    //     const style = window.getComputedStyle(node);
    //     this.hoverFontSize = style.fontSize;
    //   });
    // }
    // const d3AnnotationStyle = document.getElementsByClassName('d3-annotation-style');
    // if (d3AnnotationStyle.length > 0) {
    //   Array.from(d3AnnotationStyle).filter((node: HTMLElement) => {
    //     const style = window.getComputedStyle(node);
    //     this.annotationFontSize = style.fontSize;
    //   });
    // }
  }

  setSVGStyles(): void {
    if (this.fontFamily) {
      this.svg.style('font-family', this.fontFamily);
    }
    this.svg.style('background-color', this.backgroundColor);
    this.svg.style('fill', this.fontColor);
  }

  ngOnInit(): void {
    // Attach to window events so drawing occurs when everything is sized appropriately
    window.addEventListener('load', () => {
      setTimeout(() => {
        this.redrawChart();
      }, 200);
    });
    window.addEventListener('resize', () => {
      setTimeout(() => {
        this.redrawChart();
      }, 200);
    });
    // Notify host of the colors configured for use, so they can be matched in code, if desired
    this.colorSet.emit({chartColors: this.colors});
  }

  ngOnChanges(): void {
    if (!this.data) {
      return;
    }
    this.initSVGElement();
    if (this.chartInitialized) {
      if (this.domChart.clientWidth !== this.dimensions.width ||
        this.holdMiniChart !== this.miniChart ||
        this.checkMarginChanges()) {
        // A data change caused the chart container to resize or margins to resize
        this.svg.remove();
        this.svg = null;
        if (!this.lockAspect) {
          this.renderer.setStyle(this.domChart, 'height', '');
        }
        this.margin = (this.miniChart) ? _.cloneDeep(this.miniChartMargin) : _.cloneDeep(this.fullMargin);
        this.initSVGElement();
        this.initChart();
        this.d3Axes.redrawLabels(this.data);
      }
      this.redrawChart();
      this.holdMiniChart = this.miniChart;
    }
  }

  ngAfterViewChecked(): void {
    // CAREFUL! Don't want this to trigger all sorts of redraws
    // This is really just to catch that rare instance where some sort of delay prevented chart initialization.
    if (this.data && this.domChart.clientWidth > 0) {
      if (!this.chartInitialized) {
        this.initSVGElement();
        this.initChart();
        this.redrawChart();
      }
    }
  }

  checkMarginChanges(): boolean {
    let marginDataChanged = false;
    if (this.holdData) {
      if (
        (this.holdData.title > '' && this.data.title === '') ||
        (this.holdData.title === '' && this.data.title > '') ||
        (this.holdData.showLegend !== this.data.showLegend)
      ) {
        marginDataChanged = true;
      }
      const labels = this.getAxisLabels(this.data);
      const holdLabels = this.getAxisLabels(this.holdData);
      if (
        (holdLabels.left > '' && labels.left === '') ||
        (holdLabels.left === '' && labels.left > '') ||
        (holdLabels.right > '' && labels.right === '') ||
        (holdLabels.right === '' && labels.right > '') ||
        (holdLabels.bottom > '' && labels.bottom === '') ||
        (holdLabels.bottom === '' && labels.bottom > '')
      ) {
        marginDataChanged = true;
      }
    }
    return marginDataChanged;
  }

  // If there are multiple axes, this will concatenate their labels
  getAxisLabels(data: Model.ChartData): any {
    const labels = { left: '', right: '', bottom: ''};
    data.scales.forEach(scale => {
      switch (scale.location) {
        case Model.ChartAxisLocation.Left:
          labels.left += scale.label;
          break;
        case Model.ChartAxisLocation.Right:
          labels.right += scale.label;
          break;
        case Model.ChartAxisLocation.Bottom:
          labels.bottom += scale.label;
          break;
      }
    });
    return labels;
  }

  initSVGElement(): void {
    if (!this.domChart) {
      this.domChart = this.domChartRef.nativeElement;
      this.setDimensions();
    }
    if (!this.svg) {
      this.svg = d3.select(this.domChart).insert('svg', ':first-child');
      this.svg.attr('xmlns', 'http://www.w3.org/2000/svg');
      this.svg.append('style');
      this.setSVGStyles();
    }
  }

  setClipPath(): void {
    let defs = this.svg.select('defs');
    if (!defs) {
      defs = this.svg.append('defs');
    }
    defs.selectAll('clipPath').remove();
    defs.append('clipPath')
      .attr('id', this.clipPathId)
      // .attr('clipPathUnits', 'objectBoundingBox')
      .append('rect')
      .attr('x', 0)
      .attr('y', 0)
      .attr('width', Math.max(0, this.dimensions.width - this.margin.left - this.margin.right))
      .attr('height', Math.max(0, this.dimensions.height - this.margin.top - this.margin.bottom));
  }

  setBackgroundPatterns(): void {
    let defs = this.svg.select('defs');
    if (!defs) {
      defs = this.svg.append('defs');
    }
    defs.selectAll('pattern').remove();
    const pattern = defs.append('pattern')
      .attr('id', 'pattern-cubes')
      .attr('x', 0)
      .attr('y', 126)
      .attr('patternUnits', 'userSpaceOnUse')
      .attr('width', 10)
      .attr('height', 16)
      .attr('viewBox', '0 0 10 16');

    const cube = pattern.append('g').attr('id', 'cube');
    cube.append('path').attr('class', 'left-shade').attr('d', 'M0 0l5 3v5l-5 -3z');
    cube.append('path').attr('class', 'right-shade').attr('d', 'M10 0l-5 3v5l5 -3');

    pattern.append('use')
      .attr('x', 5)
      .attr('y', 8)
      .attr('xlink:href', '#cube');
    pattern.append('use')
      .attr('x', -5)
      .attr('y', 8)
      .attr('xlink:href', '#cube');
  }

  // This function sets up the required chart elements on the SVG
  initChart(): void {
    this.setDimensions();
    // this.setClipPath()
    this.svg.append('defs')
      .append('clipPath')
      .attr('id', this.clipPathId)
      // .attr('clipPathUnits', 'objectBoundingBox')
      .append('rect')
      .attr('x', 0)
      .attr('y', 0)
      .attr('width', Math.max(0, this.dimensions.width - this.margin.left - this.margin.right))
      .attr('height', Math.max(0, this.dimensions.height - this.margin.top - this.margin.bottom));
    if (!this.chartInitialized) {
      this.margin = (this.miniChart) ? _.cloneDeep(this.miniChartMargin) : _.cloneDeep(this.fullMargin);
    }
    if (!this.miniChart) {
      const titleBox = this.drawTitle();
      if (this.data.showLegend) {
        const legendBox = this.d3Legend.drawLegend(this.data, titleBox.height, titleBox.width);
        if (!this.data.titleAlign) {
          // Centered means stacked title and legend
          this.margin.top = titleBox.height + legendBox.height + 20;
        } else {
          this.margin.top = Math.max(titleBox.height, legendBox.height) + 20;
        }
      } else {
        this.margin.top = titleBox.height + 20;
      }
    }
    this.d3Axes.drawAxes(this.data);
    this.d3Axes.drawThresholds(this.data);
    this.d3Axes.drawEvents(this.data);
    this.drawGroups();
    if (this.totalBarCount() > 0) {
      this.d3Bars.drawBars(this.data);
    } else {
      this.d3Bars.clearBars();
    }
    if (this.totalBoxCount() > 0) {
      this.d3Boxes.drawBoxes(this.data);
      this.d3Boxes.drawVoronoi(this.data);
    } else {
      this.d3Boxes.clearBoxes();
    }
    if (this.totalPieCount() > 0) {
      this.d3Pie.drawPie(this.data);
    }
    // DO LINES AND LABELS AFTER OTHER TYPES, TO KEEP ON TOP OF OTHER DATA ELEMENTS
    if (!this.miniChart) {
      this.d3Tethers.drawTethers(this.data);
    }
    if (this.totalLineCount() > 0) {
      this.d3Lines.drawLines(this.data);
      this.d3Lines.drawVoronoi(this.data);
    } else {
      this.d3Lines.clearLines();
    }
    this.d3Labels.drawLabels(this.data);
    // DO FOCUS NOTE LAST, SO IT SHOWS ON TOP OF EVERYTHING
    this.createFocusNote();
    this.chartInitialized = true;
    this.holdData = _.cloneDeep(this.data);
  }

  // This is the main function for controlling the drawing of the chart
  redrawChart(): void {
    // Handle theme update items
    this.getStyles();
    this.setSVGStyles();
    // Handle window/chart resize items
    if (this.domChart.clientWidth <= 0) {
      return;
    }
    this.setDimensions();
    if (!this.miniChart) {
      const titleBox = this.drawTitle();
      if (this.data.showLegend) {
        this.d3Legend.drawLegend(this.data, titleBox.height, titleBox.width);
      }
    }
    this.d3Axes.redrawAxes(this.data);
    this.d3Axes.drawThresholds(this.data);
    this.d3Axes.drawEvents(this.data);
    this.drawDataGroups();
    if (this.totalBarCount() > 0) {
      this.d3Bars.drawBars(this.data);
    } else {
      this.d3Bars.clearBars();
    }
    if (this.totalBoxCount() > 0) {
      this.d3Boxes.drawBoxes(this.data);
    } else {
      this.d3Boxes.clearBoxes();
    }
    if (this.totalPieCount() > 0) {
      this.d3Pie.drawPie(this.data);
      this.d3Pie.drawPie(this.data); // because the first pass does not draw new pie charts, but the second refreshes them.
    } else {
      this.d3Pie.clearPie();
    }
    this.setClipPath();
    this.setBackgroundPatterns();
    // DO LINES AND LABELS LAST, TO KEEP ON TOP
    if (!this.miniChart) {
      this.d3Tethers.drawTethers(this.data);
    }
    if (this.totalLineCount() > 0) {
      this.d3Lines.drawLines(this.data);
      this.d3Lines.drawVoronoi(this.data);
    } else {
      this.d3Lines.clearLines();
    }
    this.d3Labels.drawLabels(this.data);
    // this.svg.select('clipPath')
    // .select('rect')
    // .attr('width', Math.max(0, this.dimensions.width - this.margin.left - this.margin.right))
    // .attr('height', Math.max(0, this.dimensions.height - this.margin.top - this.margin.bottom));
    this.renderComplete.emit();
  }

  setDimensions(): void {
    this.dimensions.width = this.domChart.clientWidth;
    if (this.lockAspect) {
      this.dimensions.height = this.domChart.clientWidth * this.aspect;
      this.renderer.setStyle(this.domChart, 'height', `${this.dimensions.height}px`);
    } else {
      this.renderer.setStyle(this.domChart, 'height', `100%`);
      this.dimensions.height = this.domChart.clientHeight;
    }
  }

  drawTitle(): any {
    let textAnchor = 'middle';
    if (this.data.titleAlign === Model.ChartTitleAlign.Left) {
      textAnchor = 'start';
    } else if (this.data.titleAlign === Model.ChartTitleAlign.Right) {
      textAnchor = 'end';
    }
    this.svg.selectAll('g.titleWrap').remove();
    const svgGroup = this.svg.append('g')
      .attr('class', 'titleWrap')
      .attr('dominant-baseline', 'hanging');
    if (this.data.title && this.data.title > '') {
      const titleGroup = svgGroup.append('text')
        .attr('class', 'title')
        .attr('text-anchor', textAnchor)
        .attr('transform', `translate(0,10)`);  // Small space at top
      titleGroup.append('tspan')
        .attr('x', 0)
        .style('font-size', this.titleFontSize)
        .text(this.data.title);
      if (this.data.subtitle && this.data.subtitle > '') {
        titleGroup.append('tspan')
          .attr('x', 0)
          .attr('dy', '1.4em')
          .style('font-size', this.subtitleFontSize)
          .text(this.data.subtitle);
      }
    }
    const box = svgGroup.node().getBBox();
    let translateX = (this.dimensions.width + this.margin.left) / 2;
    const scale = (box.width > this.dimensions.width) ? this.dimensions.width / box.width : 1;
    if (this.data.titleAlign === Model.ChartTitleAlign.Left) {
      translateX = this.margin.left;
    } else if (this.data.titleAlign === Model.ChartTitleAlign.Right) {
      translateX = this.dimensions.width - this.margin.right;
    }
    svgGroup.attr('transform', `translate(${translateX},0) scale(${scale})`);
    return box;
  }

  totalBarCount(): number {
    let totalCount = 0;
    this.data.groups.forEach(group => {
      if (group.bars && group.bars.points) {
        totalCount += group.bars.points.length;
      }
    });
    return totalCount;
  }

  totalBoxCount(): number {
    let totalCount = 0;
    this.data.groups.forEach(group => {
      if (group.boxes && group.boxes.points) {
        totalCount += group.boxes.points.length;
      }
    });
    return totalCount;
  }

  totalPieCount(): number {
    let totalCount = 0;
    this.data.groups.forEach(group => {
      if (group.pie && group.pie.values) {
        totalCount += group.pie.values.length;
      }
    });
    return totalCount;
  }

  totalNoteCount(): number {
    let totalCount = 0;
    this.data.groups.forEach(group => {
      if (group.notes) {
        totalCount += group.notes.length;
      }
    });
    return totalCount;
  }

  totalLineCount(): number {
    let totalCount = 0;
    this.data.groups.forEach(group => {
      if (group.lines) {
        totalCount += group.lines.length;
      }
    });
    return totalCount;
  }

  drawDataGroups(): void {
    let groupOrder: number[] = [];
    if (this.data.groupOrder && this.data.groupOrder.length === this.data.groups.length) {
      groupOrder = this.data.groupOrder;
    } else {
      groupOrder = this.data.groups.map((group, index) => index);
    }
    const dataGroups = this.svg.select('g.dataWrap').selectAll('.data-group')
      .data(this.data.groups);
    dataGroups.enter()
      .append('g')
      .attr('class', (d, i) => `data-group group-wrap-${groupOrder[i]}`)
      .style('display', (d, i) => {
        return (this.data.groups[groupOrder[i]].disabled) ? 'none' : '';
      });
    dataGroups.exit().remove();
    dataGroups.transition()
      .style('display', (d, i) => {
        return (this.data.groups[groupOrder[i]].disabled) ? 'none' : '';
      });
  }

  drawGroups(): void {
    // Create the wrappers for each group (if they don't exist)
    const dataWrap = this.svg.selectAll('g.dataWrap').data([this.data]);
    dataWrap.enter().append('g')
      .attr('class', 'dataWrap')
      .attr('transform', `translate(${this.margin.left},${this.margin.top})`);

    this.drawDataGroups();

    const textWrap = this.svg.selectAll('g.textWrap').data([this.data]);
    textWrap.enter().append('g')
      .attr('class', 'textWrap');

    const textGroups = this.svg.select('g.textWrap').selectAll('.text-group')
      .data(this.data.groups);
    textGroups.enter()
      .append('g')
      .attr('class', (d, i) => `text-group text-wrap-${i}`);
    textGroups.exit().remove();

    // Create the wrapper for the focus note, to keep on top
    const noteWrap = this.svg.selectAll('g.noteWrap').data([this.data]);
    noteWrap.enter().append('g')
      .attr('class', 'noteWrap');
  }

  formatForDisplay(origValue: number | string, decimals = 4): number | string {
    let val = origValue;
    if (typeof val === 'number') {
      const factor = Math.pow(10, decimals);
      val = (Math.round(val * factor) / factor).toLocaleString();
    }
    return val;
  }

  // #region Focus note handling (not annotations, which are always displayed)
  createFocusNote(): void {
    this.focusnote = this.svg.select('g.noteWrap').selectAll('.focus-note')
      .data([this.data]);
    const note = this.focusnote.enter()
      .append('g')
      .attr('class', 'focus-note')
      .attr('transform', 'translate(-1000,-1000)')
      .on('mouseover', () => {
        this.noteHasFocus = true;
      })
      .on('mouseout', () => {
        this.noteHasFocus = false;
      });
    note.append('rect')
      .attr('class', 'focus-note-rect')
      .attr('x', 15)
      .attr('y', -10)
      .attr('height', 20)
      .attr('width', 150);
    note.append('text')
      .style('font-size', this.hoverFontSize)
      .attr('class', 'focus-note-text1')
      .attr('x', 20)
      .attr('y', 6);
  }

  getFocusIconStyle(note: Model.ChartNote): any {
    const style: any= {};
    if (note.title && note.showPointData) {
      style.position = 'relative';
      style.top = '50%';
      style.transform = 'translateY(-50%)';
    }
    return style;
  }

  showFocusNote(
    data: any,
    d: any,
    showAltLabel = false,
    showMRNCount = false,
    bandAdjust = 0,
    clickableElement = null,
    showX = true
  ): void {
    // Look for a chart note for this point
    let chartNote: Model.ChartNote = null;
    if (data.groups[d.data.groupIndex].notes) {
      data.groups[d.data.groupIndex].notes.some(groupNote => {
        if (groupNote.point[0] === d.data[0] && groupNote.point[1] === d.data[1]) {
          chartNote = groupNote;
        }
        return groupNote.point[0] === d.data[0] && groupNote.point[1] === d.data[1];
      });
    }
    if (chartNote && chartNote.showPointData) {
      // If a note is found for this point, and it wants the point data in the label, skip the focus note.
      this.hideFocusNote();
      return;
    }

    const xScale = data.scales[data.groups[d.data.groupIndex].xScale];
    const yScale = data.scales[data.groups[d.data.groupIndex].yScale];
    const factor = Math.pow(10, data.decimals || 4);
    let fixedValue = (Math.round(d.data[1] * factor) / factor).toLocaleString();
    // Get total count for calculating altLabels
    // let groupBarTotal = 1;
    // if (showAltLabel) {
    //   groupBarTotal = data.groups[d.data.groupIndex].bars.points
    //     .map(point => point[1])
    //     .reduce((a, b) => a + b);
    // }
    if ((showAltLabel || showMRNCount) && d.data[2]) {
      fixedValue += ` (${d.data[2]})`;
    }
    const noteWrap = this.svg.select('g.noteWrap');
    noteWrap.attr('transform', `translate(${this.margin.left},${this.margin.top})`);
    const note = noteWrap.select('g.focus-note');
    if (clickableElement) {
      note.on('click', () => {
        const clickEvent = new MouseEvent('click', {
          view: window,
          bubbles: true,
          cancelable: false
        });
        clickableElement.dispatchEvent(clickEvent);
      });
    }
    // Get the d3 reference
    const emHeight = 1.1;
    const text = note.select('text.focus-note-text1');
    const x = text.attr('x');
    const y = text.attr('y');
    const textLine1 = (showX) ? `${d.data[0]}: ${fixedValue}` : `${fixedValue}`;
    text.selectAll('tspan').remove();
    text.append('tspan')
      .attr('x', x).attr('y', y)
      .text(textLine1);
    // Look for additional lines in the tooltip
    if (d.data.length > 3) {
      for (let i = 3; i < d.data.length; i++) {
        text.append('tspan')
          .attr('x', x).attr('y', y)
          .attr('dy', `${emHeight * (i - 2)}em`)
          .text(`${d.data[i]}`);
      }
    }

    // Set the positioning
    const textBox = note.select('text').node().getBBox();
    note.select('rect')
      .attr('width', textBox.width + 10)
      .attr('height', textBox.height + 10);
    const noteWidth = note.node().getBBox().width;
    const noteHeight = note.node().getBBox().height;
    // Horizontal
    // let translateX = xScale.scale(d.data[0]) - (xScale.scale.bandwidth() / 2) + bandAdjust + 8;
    let translateX = xScale.scale(d.data[0]) + bandAdjust;
    // let translateX = xScale.scale(d.data[0]);
    if (translateX + noteWidth > this.dimensions.width - this.margin.left - this.margin.right) {
      translateX = this.dimensions.width - this.margin.left - this.margin.right - noteWidth;
    }
    // Vertical
    const barTop = yScale.scale(d.data[1]);
    let translateY = (barTop + 18 > noteHeight) ? barTop - noteHeight : barTop + 18;
    if (translateY < 0) {
      translateY = 0;
    }
    note.attr('transform', `translate(${translateX},${translateY})`);
  }

  hideFocusNote(): void {
    this.svg.select('g.noteWrap').select('g.focus-note')
      .attr('transform', 'translate(-1000,-1000)');
  }
  // #endregion Focus note handling

  // #region Annotation handling (not the focus note, which appears on hover)
  getNoteWrapOffset(): any {
    return {
      position: 'absolute',
      top: this.margin.top + 'px',
      left: this.margin.left + 'px'
    };
  }

  getNoteStyle(groupIndex: number, noteIndex: number, group: Model.ChartGroup, note: Model.ChartNote): any {
    if (this.svg.selectAll('g.axis.left').nodes().length === 0 ||
    this.svg.selectAll('g.axis.bottom').nodes().length === 0) {
      return {};
    }
    const elem: any = this.domChart.querySelector(`.group_${groupIndex}_note_${noteIndex}`);
    let redrawTethers = false;
    const vertSpace = this.dimensions.height - this.margin.top - this.margin.bottom;
    // const horzSpace = this.dimensions.width - this.margin.left - this.margin.right;
    const availableSpace = this.dimensions.width - this.margin.left - this.data.scales[group.xScale].scale(note.point[0]);
    let alignRightBoundary = false;

    // adjust vertical position for out of bounds items
    if (this.data.scales[group.yScale].scale(note.point[1]) + note.boxOffsetTop < 0) {
      note.boxOffsetTop = -this.data.scales[group.yScale].scale(note.point[1]);
      redrawTethers = true;
    }
    if (this.data.scales[group.yScale].scale(note.point[1]) + note.boxOffsetTop > vertSpace) {
      note.boxOffsetTop = vertSpace - this.data.scales[group.yScale].scale(note.point[1]);
      redrawTethers = true;
    }
    const top = this.data.scales[group.yScale].scale(note.point[1]) + note.boxOffsetTop;

    if (this.data.scales[group.xScale].scale(note.point[0]) + note.boxOffsetLeft < 0) {
      note.boxOffsetLeft = -this.data.scales[group.xScale].scale(note.point[0]);
      redrawTethers = true;
    }
    let left = this.data.scales[group.xScale].scale(note.point[0]) + note.boxOffsetLeft;
    if (left + elem.offsetWidth > this.dimensions.width - this.margin.left) {
      // If the defaults are still set, and there isn't enough space, move the label to the left of the point
      if (availableSpace < elem.offsetWidth &&
        note.boxOffsetLeft === Model.DefaultBoxOffsetLeft &&
        note.boxOffsetTop === Model.DefaultBoxOffsetTop) {
        note.boxOffsetLeft = -elem.offsetWidth + 10;
      } else {
        alignRightBoundary = true;
      }
      redrawTethers = true;
    }
    left = this.data.scales[group.xScale].scale(note.point[0]) + note.boxOffsetLeft;

    if (note.clientWidth === 0 || note.clientHeight === 0) {
      note.clientWidth = elem.clientWidth;
      note.clientHeight = elem.clientHeight;
      redrawTethers = true;
    }
    if (redrawTethers) {
      this.d3Tethers.drawTethers(this.data);
    }

    // determine right/left binding based on width and available space
    if (alignRightBoundary) {
      const right = -(this.dimensions.width - this.margin.left);
      return { position: 'absolute', top: top + 'px', right: right + 'px', 'font-size': this.annotationFontSize };
    }
    return { position: 'absolute', top: top + 'px', left: left + 'px', 'font-size': this.annotationFontSize };
  }

  noteClicked(e: any, groupIndex: number, note: Model.ChartNote): void {
    this.noteClick.emit({
      group: groupIndex,
      note,
      event: e
    });
  }

  noteDeleted(event: any, groupIndex: number, note: Model.ChartNote): void {
    event.preventDefault();
    let foundIndex = -1;
    this.data.groups[groupIndex].notes.forEach((groupNote, index) => {
      if (groupNote.point[0] === note.point[0]) {
        foundIndex = index;
      }
      return groupNote.point[0] === note.point[0];
    });
    if (foundIndex >= 0) {
      this.data.groups[groupIndex].notes.splice(foundIndex, 1);
    }
    this.d3Tethers.drawTethers(this.data);
    this.noteDelete.emit({
      group: groupIndex,
      note,
      event
    });
  }

  noteMouseDown($downEvent: MouseEvent, groupIndex: number, note: Model.ChartNote): void {
    if ($downEvent.button !== 0) {
      return;
    }
    const noteRect = ($downEvent.target as any).getBoundingClientRect();
    const limitTop = -note.scaleY + (noteRect.height / 2);
    const limitBottom = this.dimensions.height - this.margin.top - this.margin.bottom - note.scaleY - (noteRect.height / 2);
    const limitLeft = -note.scaleX + 10; // 10px is the overlap for DIV and tether components
    const limitRight = this.dimensions.width - this.margin.left - note.scaleX - noteRect.width;
    const noteX = note.boxOffsetLeft;
    const noteY = note.boxOffsetTop;

    const checkShiftKey = ($event: MouseEvent) => {
      note.shiftLocked = $event.shiftKey;
      if ($event.shiftKey) {
        if (note.lockToYAxis) {
          note.boxOffsetTop = 0;
        } else {
          note.boxOffsetLeft = 0;
        }
      }
    };

    const checkBoundaries = () => {
      if (note.boxOffsetTop < limitTop) {
        note.boxOffsetTop = limitTop;
      }
      if (note.boxOffsetTop > limitBottom) {
        note.boxOffsetTop = limitBottom;
      }
      if (note.boxOffsetLeft < limitLeft) {
        note.boxOffsetLeft = limitLeft;
      }
      if (note.boxOffsetLeft > limitRight) {
        note.boxOffsetLeft = limitRight;
      }
    };

    const onMouseMove = ($moveEvent: MouseEvent) => {
      // console.log(limitTop - ($downEvent.screenY - $moveEvent.screenY));
      note.boxOffsetLeft = noteX + $moveEvent.screenX - $downEvent.screenX;
      note.boxOffsetTop = noteY + $moveEvent.screenY - $downEvent.screenY;
      checkShiftKey($moveEvent);
      checkBoundaries();
      this.d3Tethers.drawTethers(this.data);
    };

    const endMouseMove = () => {
      document.removeEventListener('mousemove', onMouseMove);
      document.onmouseup = null;
      document.onblur = null;
      this.noteMoved(groupIndex, note);
    };

    document.addEventListener('mousemove', onMouseMove);

    document.onblur = () => {
      endMouseMove();
    };

    document.onmouseup = () => {
      endMouseMove();
    };
  }

  noteMoved(groupIndex: number, note: Model.ChartNote): void {
    this.noteMove.emit({
      group: groupIndex,
      note
    });
  }
  // #endregion Annotation handling

  // #region Threshold handling
  getThresholdStyle(scaleIndex: number, thresholdIndex: number, scale: Model.ChartScale, threshold: Model.ChartThreshold): any {
    const elem: any = this.domChart.querySelector(`.scale_${scaleIndex}_threshold_${thresholdIndex}`);
    const limitLeft = 13;
    const limitRight = this.dimensions.width - this.margin.left - this.margin.right - elem.clientWidth + limitLeft;
    let left = limitRight * threshold.boxLeftPercent / 100;
    let top = this.data.scales[scaleIndex].scale(threshold.value);
    if (top < -this.margin.top) {
      top = -this.margin.top;
    }
    if (top > this.dimensions.height - this.margin.top) {
      top = this.dimensions.height - this.margin.top;
    }
    if (left < limitLeft) {
      left = limitLeft;
    }
    if (left > limitRight) {
      left = limitRight;
    }
    return {
      position: 'absolute',
      top: top + 'px',
      left: left + 'px',
      'font-size': this.annotationFontSize,
      'border-color': threshold.color
    };
  }

  thresholdMouseDown($downEvent: MouseEvent, scaleIndex: number, threshold: Model.ChartThreshold, thresholdIndex: number): void {
    if ($downEvent.button !== 0) {
      return;
    }
    const elem: any = this.domChart.querySelector(`.scale_${scaleIndex}_threshold_${thresholdIndex}`);
    // const startRect = ($downEvent.target as any).getBoundingClientRect();
    const limitTop = 0; // startRect.top - parseFloat(elem.style.top);
    const limitBottom = limitTop + (this.dimensions.height - this.margin.top - this.margin.bottom);
    const startTop = parseFloat(elem.style.top);
    let newTop = parseFloat(elem.style.top);
    const startLeft = parseFloat(elem.style.left);
    let newLeft = parseFloat(elem.style.left);

    const limitLeft = 13;
    const limitRight = this.dimensions.width - this.margin.left - this.margin.right - elem.clientWidth + limitLeft;

    const checkBoundaries = () => {
      if (newTop < limitTop) {
        newTop = limitTop;
      }
      if (newTop > limitBottom) {
        newTop = limitBottom;
      }
      if (newLeft < limitLeft) {
        newLeft = limitLeft;
      }
      if (newLeft > limitRight) {
        newLeft = limitRight;
      }
    };

    const onMouseMove = ($moveEvent: MouseEvent) => {
      if (!$moveEvent.shiftKey) {
        const deltaY = $moveEvent.y - $downEvent.y;
        newTop = startTop + deltaY;
      }
      const deltaX = $moveEvent.x - $downEvent.x;
      newLeft = startLeft + deltaX;
      checkBoundaries();
      const newValue = this.data.scales[scaleIndex].scale.invert(newTop);
      const factor = Math.pow(10, this.data.decimals || 4);
      threshold.value = Math.round(newValue * factor) / factor;
      threshold.boxLeftPercent = 100 * (newLeft - limitLeft) / (limitRight - limitLeft);
      this.d3Axes.drawThresholds(this.data);
    };

    document.addEventListener('mousemove', onMouseMove);

    document.onblur = () => {
      document.removeEventListener('mousemove', onMouseMove);
      document.onmouseup = null;
      document.onblur = null;
      this.thresholdMoved(scaleIndex, threshold, thresholdIndex);
    };

    document.onmouseup = () => {
      document.removeEventListener('mousemove', onMouseMove);
      document.onmouseup = null;
      document.onblur = null;
      this.thresholdMoved(scaleIndex, threshold, thresholdIndex);
    };
  }

  thresholdMoved(scaleIndex: number, threshold: Model.ChartThreshold, thresholdIndex: number): void {
    this.thresholdMove.emit({
      scaleIndex,
      thresholdIndex,
      threshold
    });
  }

  thresholdClicked(event: any, scaleIndex: number, thresholdIndex: number, threshold: Model.ChartThreshold): boolean {
    event.preventDefault();
    this.thresholdClick.emit({
      scaleIndex,
      thresholdIndex,
      threshold
    });
    return false;
  }

  thresholdDeleted(event: any, scaleIndex: number, thresholdIndex, threshold: Model.ChartThreshold): boolean {
    event.preventDefault();
    this.data.scales[scaleIndex].thresholds.splice(thresholdIndex, 1);
    this.d3Axes.drawThresholds(this.data);
    this.noteDelete.emit({
      scaleIndex,
      thresholdIndex,
      threshold
    });
    return false;
  }
  // #endregion

}
