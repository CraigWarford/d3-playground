import { Component, OnInit } from '@angular/core';

import { ChartData } from '../../chart/components';
import * as Mock from '../../_mocks';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-example',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.scss']
})
export class ExampleComponent implements OnInit {
  chartType = 'Scales';
  chartData: any = Mock.basicScales();
  useAlternateData = true;
  miniChart = false;

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        if (this.chartType !== params['type']) {
          this.chartData = new ChartData(); // reset the chart
          setTimeout(() => {
            this.chartType = params['type'];
            this.useAlternateData = true;
            this.miniChart = false;
            this.switchChartData();
          });
        }
      }
    );
  }

  switchChartData() {
    this.useAlternateData = !this.useAlternateData;
    switch (this.chartType) {
      case 'Scales':
        if (this.useAlternateData) {
          this.chartData = Mock.alternateScales();
        } else {
          this.chartData = Mock.basicScales();
        }
        break;
      case 'Split Scales':
        if (this.useAlternateData) {
          this.chartData = Mock.alternateSplitScales();
        } else {
          this.chartData = Mock.basicSplitScales();
        }
        break;
      case 'Line':
        if (this.useAlternateData) {
          this.chartData = Mock.alternateLines();
        } else {
          this.chartData = Mock.basicLines();
        }
        break;
      case 'LogLine':
        if (this.useAlternateData) {
          this.chartData = Mock.alternateLogLines();
        } else {
          this.chartData = Mock.basicLogLines();
        }
        break;
      case 'Bar':
        if (this.useAlternateData) {
          this.chartData = Mock.alternateBars();
        } else {
          this.chartData = Mock.basicBars();
        }
        break;
      case 'Pareto':
        if (this.useAlternateData) {
          this.chartData = Mock.alternatePareto();
        } else {
          this.chartData = Mock.basicPareto();
        }
        break;
      case 'BoxAndWhisker':
        if (this.useAlternateData) {
          this.chartData = Mock.alternateBoxes();
        } else {
          this.chartData = Mock.basicBoxes();
        }
        break;
      case 'LongLabels':
        if (this.useAlternateData) {
          this.chartData = Mock.alternateLongLabels();
        } else {
          this.chartData = Mock.basicLongLabels();
        }
        break;
      case 'Complex':
        if (this.useAlternateData) {
          this.chartData = Mock.complex2();
        } else {
          this.chartData = Mock.complex1();
        }
        break;
      case 'PieChart':
        if (this.useAlternateData) {
          this.chartData = Mock.alternatePieChart();
        } else {
          this.chartData = Mock.basicPieChart();
        }
        break;
      case 'Pivot':
        if (this.useAlternateData) {
          this.chartData = Mock.alternatePivot();
        } else {
          this.chartData = Mock.basicPivot();
        }
        break;
      case 'PivotSigma':
        if (this.useAlternateData) {
          this.chartData = Mock.basicPivotSigma();
        } else {
          this.chartData = Mock.basicPivotSigma();
        }
        break;
      case 'BellCurves':
        if (this.useAlternateData) {
          this.chartData = Mock.alternateBellCurves();
        } else {
          this.chartData = Mock.basicBellCurves();
        }
        break;
      }
  }

  pointClicked(pointData: any) {
    window.alert('Point Clicked:' + JSON.stringify(pointData));
  }

  barClicked(barData: any) {
    window.alert('Bar Clicked:' + JSON.stringify(barData));
  }

  boxClicked(boxData: any) {
    window.alert('Box Clicked:' + JSON.stringify(boxData));
  }

  legendClicked(data: any) {
    // window.alert('Legend Clicked:' + JSON.stringify(data));
  }

  axisClicked(tickData: any) {
    window.alert('Tick mark clicked::' + JSON.stringify(tickData));
  }

  noteClicked(boxData: any) {
    window.alert('Note Clicked. Typically, this should trigger opening a modal to ' +
    'edit the note using this passed data:' + JSON.stringify(boxData));
  }

  noteMoved(boxData: any) {
    console.log('Note Moved. The new coordinates should be recorded with the data:' + JSON.stringify(boxData));
  }

  dragUpdatedX(dragData: any) {
    console.log(dragData);
  }

}
