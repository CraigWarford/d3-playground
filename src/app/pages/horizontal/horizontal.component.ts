import { Component, OnInit } from '@angular/core';

import { ChartData } from '../../chart/components';
import * as Mock from '../../_mocks';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-horizontal',
  templateUrl: './horizontal.component.html',
  styleUrls: ['./horizontal.component.scss']
})
export class HorizontalComponent implements OnInit {
  chartType = 'Mini Plots';
  chartData: any = Mock.basicMiniPlots();
  useAlternateData = true;
  miniChart = false;

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        if (this.chartType !== params.type) {
          this.chartData = new ChartData(); // reset the chart
          setTimeout(() => {
            this.chartType = params.type;
            this.useAlternateData = true;
            this.miniChart = false;
            this.switchChartData();
          });
        }
      }
    );
  }

  switchChartData() {
    this.useAlternateData = !this.useAlternateData;
    switch (this.chartType) {
    case 'Mini Plots':
      if (this.useAlternateData) {
        this.chartData = Mock.alternateMiniPlots();
      } else {
        this.chartData = Mock.basicMiniPlots();
      }
      break;
    }
  }

  pointClicked(pointData: any) {
    window.alert('Point Clicked:' + JSON.stringify(pointData));
  }

  barClicked(barData: any) {
    window.alert('Bar Clicked:' + JSON.stringify(barData));
  }

  boxClicked(boxData: any) {
    window.alert('Box Clicked:' + JSON.stringify(boxData));
  }

  legendClicked(data: any) {
    // window.alert('Legend Clicked:' + JSON.stringify(data));
  }

  axisClicked(tickData: any) {
    window.alert('Tick mark clicked::' + JSON.stringify(tickData));
  }

  noteClicked(boxData: any) {
    window.alert('Note Clicked. Typically, this should trigger opening a modal to ' +
    'edit the note using this passed data:' + JSON.stringify(boxData));
  }

  noteMoved(boxData: any) {
    console.log('Note Moved. The new coordinates should be recorded with the data:' + JSON.stringify(boxData));
  }

  dragUpdatedX(dragData: any) {
    console.log(dragData);
  }

}
