import { Component, OnInit } from '@angular/core';

import { ChartData } from '../../chart/components';
import * as Mock from '../../_mocks';
import * as ChartModel from '../../chart/components/chart-data.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.scss']
})
export class TimeComponent implements OnInit {
  chartType = 'Scales';
  chartData: any = Mock.timePivot();
  useAlternateData = true;
  miniChart = false;
  timePivotMainFrames = {};

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        if (this.chartType !== params.type) {
          this.chartData = new ChartData(); // reset the chart
          setTimeout(() => {
            this.chartType = params.type;
            this.miniChart = false;
            this.setChartData();
          });
        }
      }
    );
  }

  findMainLine() {
    let main = null;
    this.chartData.groups[0].lines.some((line: ChartModel.ChartLine) => {
      if (line.type === ChartModel.ChartLineType.Main) {
        main = line;
      }
      return line.type === ChartModel.ChartLineType.Main;
    });
    return main;
  }

  setChartData() {
    switch (this.chartType) {
      case 'Time Pivot':
        this.chartData = Mock.timePivot();
        this.timePivotMainFrames = Mock.timePivotMainFrames;
        const main = this.findMainLine();
        if (main) {
          const key = Object.keys(Mock.timePivotMainFrames)[0];
          main.tails = Mock.timePivotMainFrames[key];
          console.log(main.tails);
        }
        break;
      }
  }

  pointClicked(pointData: any) {
    window.alert('Point Clicked:' + JSON.stringify(pointData));
  }

  barClicked(barData: any) {
    window.alert('Bar Clicked:' + JSON.stringify(barData));
  }

  boxClicked(boxData: any) {
    window.alert('Box Clicked:' + JSON.stringify(boxData));
  }

  legendClicked(data: any) {
    // window.alert('Legend Clicked:' + JSON.stringify(data));
  }

  axisClicked(tickData: any) {
    window.alert('Tick mark clicked::' + JSON.stringify(tickData));
  }

  noteClicked(boxData: any) {
    window.alert('Note Clicked. Typically, this should trigger opening a modal to ' +
    'edit the note using this passed data:' + JSON.stringify(boxData));
  }

  noteMoved(boxData: any) {
    console.log('Note Moved. The new coordinates should be recorded with the data:' + JSON.stringify(boxData));
  }

  dragUpdatedX(dragData: any) {
    console.log(dragData);
    const main = this.findMainLine();
    if (main) {
      main.tails = Mock.timePivotMainFrames[dragData.newDomainValue];
      console.log(main.tails);
    }
  }

}
