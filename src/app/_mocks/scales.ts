import * as d3 from 'd3';
import * as Model from '../chart/components';
import { D3ScaleType } from '../chart/components';

export function basicScales() {
  return {
    title: 'The Grand Scale',
    scales: [
      {
        label: 'Bottom Scale 1',
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Point,
        scale: d3.scalePoint(),
        domain: [
          '2015-11-01', '2015-12-01', '2016-01-01', '2016-02-01',
          '2016-03-01', '2016-04-01', '2016-05-01', '2016-06-01',
          '2016-07-01', '2016-08-01', '2016-09-01', '2016-10-01',
          '2016-11-01', '2016-12-01', '2017-01-01', '2017-02-01',
          '2017-03-01', '2017-04-01', '2017-05-01', '2017-06-01',
          '2017-07-01', '2017-08-01', '2017-09-01', '2017-10-01',
          '2017-11-01'],
        dragHandle: true,
        dragPosition: '2017-01-01'
      }, {
        label: 'Bottom Scale 2',
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Point,
        scale: d3.scalePoint(),
        domain: [
          '2015-11-01', '2015-12-01', '2016-01-01', '2016-02-01',
          '2016-03-01', '2016-04-01', '2016-05-01', '2016-06-01',
          '2016-11-01', '2016-12-01', '2017-01-01', '2017-02-01',
          '2017-03-01', '2017-04-01', '2017-05-01', '2017-06-01',
          '2017-07-01', '2017-08-01', '2017-09-01', '2017-10-01',
          '2017-11-01'],
        dragHandle: true
      }, {
        label: 'Left Scale 1',
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        domain: [0, 25]
      }, {
        label: 'Left Scale 2',
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        domain: [0, 37]
      }, {
        label: 'Left Scale 3',
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        domain: [0, 49]
      }, {
        label: 'Right Scale 1',
        location: Model.ChartAxisLocation.Right,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        domain: [0, 25]
      }, {
        label: 'Right Scale 2',
        location: Model.ChartAxisLocation.Right,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        domain: [0, 37]
      }, {
        label: 'Right Scale 3',
        location: Model.ChartAxisLocation.Right,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        domain: [0, 49]
      }
    ],
    groups: [
      {
        label: 'Data Group 1',
        disabled: false,
        xScale: 0,
        yScale: 2
      }, {
        label: 'Data Group 2',
        disabled: false,
        xScale: 0,
        yScale: 3
      }, {
        label: 'Data Group 3',
        disabled: false,
        xScale: 0,
        yScale: 4
      }, {
        label: 'Data Group 4',
        disabled: false,
        xScale: 1,
        yScale: 5
      }, {
        label: 'Data Group 5',
        disabled: false,
        xScale: 1,
        yScale: 6
      }, {
        label: 'Data Group 6',
        disabled: false,
        xScale: 1,
        yScale: 7
      }
    ]
  };
}

export function alternateScales() {
  return {
    title: 'The Grand Scale - Alternate',
    scales: [
      {
        label: 'Bottom Scale 1',
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Point,
        scale: d3.scalePoint(),
        domain: [
          '2015-11-01', '2015-12-01', '2016-01-01', '2016-02-01',
          '2016-03-01', '2016-04-01', '2016-05-01', '2016-06-01',
          '2016-11-01', '2016-12-01', '2017-01-01', '2017-02-01',
          '2017-03-01', '2017-04-01', '2017-05-01', '2017-06-01',
          '2017-07-01', '2017-08-01', '2017-09-01', '2017-10-01',
          '2017-11-01'
        ]
      }, {
        label: 'Bottom Scale 2',
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Point,
        scale: d3.scalePoint(),
        domain: [
          '2015-11-01', '2015-12-01', '2016-01-01', '2016-02-01',
          '2016-03-01', '2016-04-01', '2016-05-01', '2016-06-01',
          '2016-07-01', '2016-08-01', '2016-09-01', '2016-10-01',
          '2016-11-01', '2016-12-01', '2017-01-01', '2017-02-01',
          '2017-03-01', '2017-04-01', '2017-05-01', '2017-06-01',
          '2017-07-01', '2017-08-01', '2017-09-01', '2017-10-01',
          '2017-11-01'
        ]
      }, {
        label: 'Left Scale 1',
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        domain: [0, 49]
      }, {
        label: 'Left Scale 2',
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        domain: [0, 37]
      }, {
        label: 'Left Scale 3',
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        domain: [0, 25]
      }, {
        label: 'Right Scale 1',
        location: Model.ChartAxisLocation.Right,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        domain: [0, 49]
      }, {
        label: 'Right Scale 2',
        location: Model.ChartAxisLocation.Right,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        domain: [0, 37]
      }, {
        label: 'Right Scale 3',
        location: Model.ChartAxisLocation.Right,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        domain: [0, 25]
      }
    ],
    groups: [
      {
        label: 'Data Group 1',
        disabled: false,
        xScale: 0,
        yScale: 2
      }, {
        label: 'Data Group 2',
        disabled: false,
        xScale: 0,
        yScale: 3
      }, {
        label: 'Data Group 3',
        disabled: false,
        xScale: 0,
        yScale: 4
      }, {
        label: 'Data Group 4',
        disabled: false,
        xScale: 1,
        yScale: 5
      }, {
        label: 'Data Group 5',
        disabled: false,
        xScale: 1,
        yScale: 6
      }, {
        label: 'Data Group 6',
        disabled: false,
        xScale: 1,
        yScale: 7
      }
    ]
  };
}

