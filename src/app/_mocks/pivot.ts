import * as d3 from 'd3';
import * as Model from '../chart/components';
import { D3ScaleType } from '../chart/components';

// export function alternateSplitScales() {
export function basicPivot() {
  return {
    title: 'The Pivot',
    showLegend: true,
    splitBottomScaleRange: false,
    splitLeftScaleRange: false,
    splitRightScaleRange: false,
    scales: [
      {
        label: 'Facility',
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Point,
        scale: d3.scalePoint(),
        grid: false,
        hideLabels: false,
        hideScale: false,
        domain: [
          'Radiology Surgery',
          'Radiology BCSC Surgery',
          'Cath Lab Surgery',
          'IRAD Surgery',
          'GI Surgery',
          'BCSC Surgery',
          'Main OR Surgery'
        ]
      }, {
        label: 'On Time Starts Avg.',
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        grid: false,
        scale: d3.scaleLinear(),
        domain: [0, 10]
      }
    ],
    groups: [
      {
        label: 'Data Group 1',
        disabled: false,
        xScale: 0,
        yScale: 1,
        lines: [
          {
            type: 0,
            label: '',
            points: [
              ['Radiology Surgery', 81.35, 'n=20', 'facility.Radiology Surgery'],
              ['Radiology BCSC Surgery', -14.4444444444444, 'n=36', 'facility.Radiology BCSC Surgery'],
              ['Cath Lab Surgery', 32.8024475524476, 'n=1144', 'facility.Cath Lab Surgery'],
              ['IRAD Surgery', 47.7449177153921, 'n=2066', 'facility.IRAD Surgery'],
              ['GI Surgery', 23.8346892287667, 'n=3073', 'facility.GI Surgery'],
              ['BCSC Surgery', -7.37491022264783, 'n=8354', 'facility.BCSC Surgery'],
              ['Main OR Surgery', 42.7601124214703, 'n=18146', 'facility.Main OR Surgery']
            ],
            visible: true,
            ghostFirstPoint: false,
            ghostLastPoint: false,
            blockLine: false,
            noLine: true
          }, {
            type: 1,
            label: '',
            points: [
              ['Radiology Surgery', 85.96364222559532],
              ['Radiology BCSC Surgery', 68.96334803588255],
              ['Cath Lab Surgery', 53.66308326514104],
              ['IRAD Surgery', 53.66308326514104],
              ['GI Surgery', 53.66308326514104],
              ['BCSC Surgery', 53.66308326514104],
              ['Main OR Surgery', 53.66308326514104]
            ],
            visible: true,
            ghostFirstPoint: false,
            ghostLastPoint: false,
            blockLine: true,
            noLine: false
          }, {
            type: 2,
            label: '',
            points: [
              ['Radiology Surgery', -29.638358264451533],
              ['Radiology BCSC Surgery', -12.638064074738757],
              ['Cath Lab Surgery', 2.6622006960027385],
              ['IRAD Surgery', 2.6622006960027385],
              ['GI Surgery', 2.6622006960027385],
              ['BCSC Surgery', 2.6622006960027385],
              ['Main OR Surgery', 2.6622006960027385]
            ],
            visible: true,
            ghostFirstPoint: false,
            ghostLastPoint: false,
            blockLine: true,
            noLine: false
          }, {
            type: 3,
            label: 'MEAN: 28.16',
            points: [
              ['Radiology Surgery', 28.16264198057189],
              ['Radiology BCSC Surgery', 28.16264198057189],
              ['Cath Lab Surgery', 28.16264198057189],
              ['IRAD Surgery', 28.16264198057189],
              ['GI Surgery', 28.16264198057189],
              ['BCSC Surgery', 28.16264198057189],
              ['Main OR Surgery', 28.16264198057189]
            ],
            visible: true,
            ghostFirstPoint: false,
            ghostLastPoint: false,
            blockLine: false,
            noLine: false
          }
        ],
      }
    ]
  };
}

// export function basicSplitScales() {
export function alternatePivot() {
  return {
    title: 'The Pivot - No Labels',
    showLegend: true,
    splitBottomScaleRange: false,
    splitLeftScaleRange: false,
    splitRightScaleRange: false,
    scales: [
      {
        label: 'Facility',
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Point,
        scale: d3.scalePoint(),
        grid: false,
        hideLabels: true,
        hideScale: true,
        domain: [
          'Radiology Surgery',
          'Radiology BCSC Surgery',
          'Cath Lab Surgery',
          'IRAD Surgery',
          'GI Surgery',
          'BCSC Surgery',
          'Main OR Surgery'
        ]
      }, {
        label: 'On Time Starts Avg.',
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        grid: false,
        hideLabels: true,
        hideScale: true,
        scale: d3.scaleLinear(),
        domain: [0, 10]
      }
    ],
    groups: [
      {
        label: 'Data Group 1',
        disabled: false,
        xScale: 0,
        yScale: 1,
        lines: [
          {
            type: 0,
            label: '',
            points: [
              ['Radiology Surgery', 81.35, 'n=20', 'facility.Radiology Surgery'],
              ['Radiology BCSC Surgery', -14.4444444444444, 'n=36', 'facility.Radiology BCSC Surgery'],
              ['Cath Lab Surgery', 32.8024475524476, 'n=1144', 'facility.Cath Lab Surgery'],
              ['IRAD Surgery', 47.7449177153921, 'n=2066', 'facility.IRAD Surgery'],
              ['GI Surgery', 23.8346892287667, 'n=3073', 'facility.GI Surgery'],
              ['BCSC Surgery', -7.37491022264783, 'n=8354', 'facility.BCSC Surgery'],
              ['Main OR Surgery', 42.7601124214703, 'n=18146', 'facility.Main OR Surgery']
            ],
            visible: true,
            ghostFirstPoint: false,
            ghostLastPoint: false,
            blockLine: false,
            noLine: true
          }, {
            type: 1,
            label: '',
            points: [
              ['Radiology Surgery', 85.96364222559532],
              ['Radiology BCSC Surgery', 68.96334803588255],
              ['Cath Lab Surgery', 53.66308326514104],
              ['IRAD Surgery', 53.66308326514104],
              ['GI Surgery', 53.66308326514104],
              ['BCSC Surgery', 53.66308326514104],
              ['Main OR Surgery', 53.66308326514104]
            ],
            visible: true,
            ghostFirstPoint: false,
            ghostLastPoint: false,
            blockLine: true,
            noLine: false
          }, {
            type: 2,
            label: '',
            points: [
              ['Radiology Surgery', -29.638358264451533],
              ['Radiology BCSC Surgery', -12.638064074738757],
              ['Cath Lab Surgery', 2.6622006960027385],
              ['IRAD Surgery', 2.6622006960027385],
              ['GI Surgery', 2.6622006960027385],
              ['BCSC Surgery', 2.6622006960027385],
              ['Main OR Surgery', 2.6622006960027385]
            ],
            visible: true,
            ghostFirstPoint: false,
            ghostLastPoint: false,
            blockLine: true,
            noLine: false
          }, {
            type: 3,
            label: 'MEAN: 28.16',
            points: [
              ['Radiology Surgery', 28.16264198057189],
              ['Radiology BCSC Surgery', 28.16264198057189],
              ['Cath Lab Surgery', 28.16264198057189],
              ['IRAD Surgery', 28.16264198057189],
              ['GI Surgery', 28.16264198057189],
              ['BCSC Surgery', 28.16264198057189],
              ['Main OR Surgery', 28.16264198057189]
            ],
            visible: true,
            ghostFirstPoint: false,
            ghostLastPoint: false,
            blockLine: false,
            noLine: false
          }
        ],
      }
    ]
  };
}
