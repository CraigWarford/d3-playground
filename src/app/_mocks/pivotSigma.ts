import * as d3 from 'd3';
import * as Model from '../chart/components';
import { D3ScaleType } from '../chart/components';

export function basicPivotSigma() {
  return {
    title: 'The Quality Pivot - Balancing Metrics',
    showLegend: true,
    splitBottomScaleRange: false,
    splitLeftScaleRange: false,
    splitRightScaleRange: false,
    scales: [
      {
        label: 'Surgeon Name',
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Band,
        scale: d3.scaleBand(),
        grid: false,
        hideLabels: false,
        hideScale: false,
        domain: [
          'Surgeon One',
          'Surgeon Two',
          'Surgeon Three',
          'Surgeon Four',
          'Surgeon Five',
          'Surgeon Six',
          'Surgeon Seven'
        ]
      }, {
        label: 'Sigma Variance per Metric',
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        grid: false,
        hideLabels: false,
        hideScale: false,
        scale: d3.scaleLinear(),
        domain: [-4, 4],
        tickValues: [-3, -2, -1, 0, 1, 2, 3],
        tickFormat: ['-3σ', '-2σ', '-1σ', '0', '1σ', '2σ', '3σ']
      }
    ],
    groups: [
      {
        label: 'On Time Starts Avg. / Mortality Within 30 Days / Time to Lollipop',
        disabled: false,
        xScale: 0,
        yScale: 1,
        boxes: {
          hideLabels: true,
          hideOutlierLabels: true,
          points: [
            ['Surgeon One', 2.6, 2.6, 2.6, 2.9, 1, 2.9, 2, 1],
            ['Surgeon Two', -3.1, -3.1, -3.1, -2.9, -3.3, -3.2, -3.3, -2.9],
            ['Surgeon Three', .5, .5, .5, 2.9, -.1, -.1, 2.9, 1.5],
            ['Surgeon Four', 2.5, 2.5, 2.5, 2.8, 2.2, 2.8, 2.7, 2.2],
            ['Surgeon Five', -.6, -.6, -.6, 1.5, -1, .5, -1, 1.5],
            ['Surgeon Six', -2, -2, -2, .5, -3.7, -3.7, .5, -1],
            ['Surgeon Seven', 1.8, 1.8, 1.8, 2.9, 1, 2.9, 2, 1],
          ]
        },
        lines: [
          {
            type: 1,
            label: '',
            points: [
              ['Surgeon One', 3],
              ['Surgeon Two', 3],
              ['Surgeon Three', 3],
              ['Surgeon Four', 3],
              ['Surgeon Five', 3],
              ['Surgeon Six', 3],
              ['Surgeon Seven', 3]
            ],
            visible: true,
            ghostFirstPoint: false,
            ghostLastPoint: false,
            blockLine: true,
            noLine: false,
            color: 'gainsboro'
          }, {
            type: 2,
            label: '',
            points: [
              ['Surgeon One', -3],
              ['Surgeon Two', -3],
              ['Surgeon Three', -3],
              ['Surgeon Four', -3],
              ['Surgeon Five', -3],
              ['Surgeon Six', -3],
              ['Surgeon Seven', -3]
            ],
            visible: true,
            ghostFirstPoint: false,
            ghostLastPoint: false,
            blockLine: true,
            noLine: false,
            color: 'gainsboro'
          }, {
            type: 3,
            label: '',
            points: [
              ['Surgeon One', 0],
              ['Surgeon Two', 0],
              ['Surgeon Three', 0],
              ['Surgeon Four', 0],
              ['Surgeon Five', 0],
              ['Surgeon Six', 0],
              ['Surgeon Seven', 0]
            ],
            visible: true,
            ghostFirstPoint: false,
            ghostLastPoint: false,
            blockLine: false,
            noLine: false,
            color: 'gainsboro'
          }
        ],
      }
    ]
  };
}
