import * as d3 from 'd3';
import * as Model from '../chart/components';
import { D3ScaleType } from '../chart/components';

export function basicMiniPlots() {
  return [{
    showLegend: false,
    scales: [
      {
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Band,
        scale: d3.scaleBand(),
        domain: ['On Time Starts Delta']
      }, {
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        hideScale: true,
        domain: [1, 4.5]
      }
    ],
    groups: [
      {
        disabled: false,
        xScale: 0,
        yScale: 1,
        boxes: {
          hideOutlierLabels: true,
          points: [
            ['On Time Starts Delta', 3, 3, 3, 4.5, 1, 2.5]
          ]
        }
      }
    ]
  }, {
    showLegend: false,
    scales: [
      {
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Band,
        scale: d3.scaleBand(),
        domain: ['PACU Length of Stay and Some Other Lengthy Text to Wrap']
      }, {
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        hideScale: true,
        domain: [25, 50]
      }
    ],
    groups: [
      {
        disabled: false,
        xScale: 0,
        yScale: 1,
        color: 'red',
        boxes: {
          hideOutlierLabels: true,
          points: [
            ['PACU Length of Stay and Some Other Lengthy Text to Wrap', 30, 30, 30, 50, 25, 45]
          ]
        }
      }
    ]
  }, {
    showLegend: false,
    scales: [
      {
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Band,
        scale: d3.scaleBand(),
        domain: ['Max Pain Score']
      }, {
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        hideScale: true,
        domain: [0, 10]
      }
    ],
    groups: [
      {
        disabled: false,
        xScale: 0,
        yScale: 1,
        color: 'yellow',
        boxes: {
          hideOutlierLabels: true,
          points: [
            ['Max Pain Score', 3, 3, 3, 9, 1, 2.5]
          ]
        }
      }
    ]
  }];
}

export function alternateMiniPlots() {
  return [{
    title: 'Boxes and Whiskers and Kittens',
    showLegend: true,
    scales: [
      {
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Band,
        scale: d3.scaleBand(),
        domain: ['2015-11-01']
      }, {
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        domain: [0, 12]
      }
    ],
    groups: [
      {
        label: 'Data Group 1',
        disabled: false,
        xScale: 0,
        yScale: 1,
        boxes: {
          hideLabels: true,
          hideOutlierLabels: false,
          points: [
            ['2015-11-01', 8, 5, 6.5, 9, 3, 2.5, 10]
          ]
        }
      }
    ]
  }];
}

