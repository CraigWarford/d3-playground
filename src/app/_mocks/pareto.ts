import * as d3 from 'd3';
import * as Model from '../chart/components';
import { D3ScaleType } from '../chart/components';

export function basicPareto() {
  return {
    title: 'Pareto Alfredo! (Split Scales)',
    showLegend: true,
    splitBottomScaleRange: false,
    scales: [
      {
        label: 'Date of Service',
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Band,
        grid: true,
        scale: d3.scaleBand(),
        domain: [
          '2015-11-01', '2015-12-01', '2016-01-01', '2016-02-01',
          '2016-03-01', '2016-04-01', '2016-05-01', '2016-06-01',
          '2016-07-01', '2016-08-01', '2016-09-01', '2016-10-01',
          '2016-11-01']
      }, {
        label: 'Date of Service',
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Band,
        grid: true,
        scale: d3.scaleBand(),
        domain: [
          '2016-11-01', '2016-12-01', '2017-01-01', '2017-02-01',
          '2017-03-01', '2017-04-01', '2017-05-01', '2017-06-01',
          '2017-07-01', '2017-08-01', '2017-09-01', '2017-10-01',
          '2017-11-01']
      }, {
        label: 'Average Value',
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        grid: true,
        scale: d3.scaleLinear(),
        domain: [0, 40]
      }
    ],
    groups: [
      {
        label: 'Count',
        disabled: false,
        xScale: 0,
        yScale: 2,
        lines: [
          {
            type: Model.ChartLineType.Main,
            visible: true,
            points: [
              ['2015-11-01', 6.2],
              ['2015-12-01', 10.6],
              ['2016-01-01', 14.8],
              ['2016-02-01', 17.7],
              ['2016-03-01', 20.5],
              ['2016-04-01', 23.1],
              ['2016-05-01', 25.3],
              ['2016-06-01', 27.4],
              ['2016-07-01', 29.5],
              ['2016-08-01', 31.5],
              ['2016-09-01', 33.4],
              ['2016-10-01', 34.7],
              ['2016-11-01', 35.4]
            ]
          }
        ],
        bars: {
          showAltLabel: true,
          points: [
            ['2015-11-01', 6.2],
            ['2015-12-01', 4.4],
            ['2016-01-01', 4.2],
            ['2016-02-01', 2.9],
            ['2016-03-01', 2.8],
            ['2016-04-01', 2.6],
            ['2016-05-01', 2.2],
            ['2016-06-01', 2.1],
            ['2016-07-01', 2.1],
            ['2016-08-01', 2],
            ['2016-09-01', 1.9],
            ['2016-10-01', 1.3],
            ['2016-11-01', 0.7]
          ]
        }
      }, {
        label: 'Capacity',
        disabled: false,
        xScale: 1,
        yScale: 2,
        bars: {
          showAltLabel: false,
          points: [
            ['2016-11-01', 10],
            ['2016-12-01', 20],
            ['2017-01-01', 15],
            ['2017-02-01', 4.2],
            ['2017-03-01', 4],
            ['2017-04-01', 3.0],
            ['2017-05-01', 2.7],
            ['2017-06-01', 2.6],
            ['2017-07-01', 2],
            ['2017-08-01', 1.5],
            ['2017-09-01', 1.0],
            ['2017-10-01', 0],
            ['2017-11-01', 0]
          ]
        }
      }
    ]
  };
}

export function alternatePareto() {
  return {
    title: 'Plato Pareto (Shared Scale)',
    showLegend: true,
    scales: [
      {
        label: 'Date of Service',
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Band,
        grid: true,
        scale: d3.scaleBand(),
        domain: [
          '2016-11-01', '2016-12-01', '2017-01-01', '2017-02-01',
          '2017-03-01', '2017-04-01', '2017-05-01', '2017-06-01',
          '2017-07-01', '2017-08-01', '2017-09-01', '2017-10-01',
          '2017-11-01']
      }, {
        label: 'Date of Service',
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Band,
        grid: true,
        scale: d3.scaleBand(),
        domain: [
          '2016-11-01', '2016-12-01', '2017-01-01', '2017-02-01',
          '2017-03-01', '2017-04-01', '2017-05-01', '2017-06-01',
          '2017-07-01', '2017-08-01', '2017-09-01', '2017-10-01',
          '2017-11-01']
      }, {
        label: 'Median Value',
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        grid: true,
        scale: d3.scaleLinear(),
        domain: [0, 100],
        lockDomain: true
      }
    ],
    groups: [
      {
        label: 'Data Group 1',
        disabled: false,
        xScale: 0,
        yScale: 2,
        lines: [
          {
            type: Model.ChartLineType.Main,
            visible: true,
            points: [
              ['2016-11-01', 6.2],
              ['2016-12-01', 10.6],
              ['2017-01-01', 14.8],
              ['2017-02-01', 17.7],
              ['2017-03-01', 20.5],
              ['2017-04-01', 23.1],
              ['2017-05-01', 25.3],
              ['2017-06-01', 27.4],
              ['2017-07-01', 29.5],
              ['2017-08-01', 31.5],
              ['2017-09-01', 33.4],
              ['2017-10-01', 34.7],
              ['2017-11-01', 35.4]
            ]
          }
        ],
        bars: {
          showAltLabel: true,
          points: [
            ['2016-11-01', 6.2],
            ['2016-12-01', 4.4],
            ['2017-01-01', 4.2],
            ['2017-02-01', 2.9],
            ['2017-03-01', 2.8],
            ['2017-04-01', 2.6],
            ['2017-05-01', 2.2],
            ['2017-06-01', 2.1],
            ['2017-07-01', 2.1],
            ['2017-08-01', 2],
            ['2017-09-01', 1.9],
            ['2017-10-01', 1.3],
            ['2017-11-01', 0.7]
          ]
        }
      }, {
        label: 'Data Group 2',
        disabled: false,
        xScale: 0,
        yScale: 2,
        lines: [
          {
            type: Model.ChartLineType.Main,
            visible: true,
            points: [
              ['2016-11-01', 5.6],
              ['2016-12-01', 10.2],
              ['2017-01-01', 14.8],
              ['2017-02-01', 19],
              ['2017-03-01', 23],
              ['2017-04-01', 26],
              ['2017-05-01', 28.7],
              ['2017-06-01', 31.3],
              ['2017-07-01', 33.3],
              ['2017-08-01', 34.8],
              ['2017-09-01', 35.8],
              ['2017-10-01', 35.8],
              ['2017-11-01', 35.8]
            ]
          }
        ],
        bars: {
          showAltLabel: false,
          points: [
            ['2016-11-01', 5.6],
            ['2016-12-01', 4.6],
            ['2017-01-01', 4.6],
            ['2017-02-01', 4.2],
            ['2017-03-01', 4],
            ['2017-04-01', 3.0],
            ['2017-05-01', 2.7],
            ['2017-06-01', 2.6],
            ['2017-07-01', 2],
            ['2017-08-01', 1.5],
            ['2017-09-01', 1.0],
            ['2017-10-01', 0],
            ['2017-11-01', 0]
          ]
        }
      }
    ]
  };
}

