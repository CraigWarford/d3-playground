import * as d3 from 'd3';
import * as Model from '../chart/components';
import { D3ScaleType } from '../chart/components';

export function basicPieChart() {
  return {
    title: 'Pie Chart 1',
    showLegend: true,
    singleLegendMode: false,
    scales: [],
    groups: [
      {
        label: 'Data Group 1',
        color: '#2276dd',
        disabled: false,
        pie: {
          values: [
            ['Label 1', 10],
            ['Label 2', 20],
            ['Label 3', 100],
          ]
        }
      },
      {
        label: 'Data Group 2',
        color: '#2276dd',
        disabled: false,
        pie: {
          values: [
            ['Alfa', 20],
            ['Bravo', 50],
            ['Charlie', 70],
            ['Delta', 20],
            ['Echo', 5],
            ['Foxtrot', 4],
            ['Golf', 3],
            ['Hotel', 2],
            ['India', 1],
            ['Juliet', 0.5],
            ['Kilo', 0.25],
            ['Lima', 0.1],
          ]
        }
      }
    ]
  };
}

export function alternatePieChart() {
  return {
    title: 'Pie Chart 2',
    showLegend: true,
    singleLegendMode: false,
    scales: [],
    groups: [
      {
        label: 'Data Group 1',
        color: '#2276dd',
        disabled: false,
        pie: {
          values: [
            ['Alpha', 20],
            ['Beta', 50],
            ['Gamma', 70],
            ['Delta', 20],
            ['Epsilon', 5],
            ['Zeta', 4],
            ['Eta', 3],
            ['Theta', 2],
            ['Iota', 1],
            ['Kappa', 0.5],
            ['Lambda', 0.25],
            ['Mu', 0.1],
          ]
        }
      },
      {
        label: 'Data Group 2',
        color: '#2276dd',
        disabled: false,
        pie: {
          values: [
            ['Label A', 100],
            ['Label B', 20],
            ['Label C', 10],
          ]
        }
      }
    ]
  };
}
