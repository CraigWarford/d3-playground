import * as d3 from 'd3';
import * as Model from '../chart/components';
import { D3ScaleType } from '../chart/components';

const gaussian = 1 / Math.sqrt(2 * Math.PI);
function getExp(x, mean, sigma) {
  return Math.pow(x - mean, 2) / (2 * Math.pow(sigma, 2));
}
function getDistribution(x, mean, sigma) {
  const exp = getExp(x, mean, sigma);
  const e = Math.pow(Math.E, -exp);
  const y = e * gaussian / sigma;
  return y;
}
function getBulkPoints(min, max, mean = 0, sigma = 1) {
  const points = [];
  const span = max - min;
  const pointCount = 100;
  const increment = span / pointCount;
  for (let x = min; x <= max; x += increment) {
    points.push([x, getDistribution(x, mean, sigma)]);
  }
  return points;
}

export function basicBellCurves() {
  return {
    title: 'Curvy Calculations',
    showLegend: true,
    scales: [
      {
        label: 'X',
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        domain: [-10, 10]
      }, {
        label: 'Y',
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        domain: [0, 12]
      }
    ],
    groups: [
      {
        label: 'Data Group 1',
        disabled: false,
        xScale: 0,
        yScale: 1,
        lines: [
          {
            type: Model.ChartLineType.Main,
            visible: true,
            noPoints: true,
            fill: true,
            fillOpacity: '.5',
            points: getBulkPoints(-5, 15, 5, 2)
          }
        ]
      }, {
        label: 'Data Group 2',
        disabled: false,
        xScale: 0,
        yScale: 1,
        lines: [
          {
            type: Model.ChartLineType.Main,
            visible: true,
            noPoints: true,
            fill: true,
            fillOpacity: '.5',
            points: getBulkPoints(-12, 2, -5, 1.5)
          }
        ]
      }, {
        label: 'Data Group 3',
        disabled: false,
        xScale: 0,
        yScale: 1,
        lines: [
          {
            type: Model.ChartLineType.Main,
            visible: true,
            noPoints: true,
            fill: true,
            fillOpacity: '.5',
            points: getBulkPoints(-5, 5, 0, 1)
          }
        ]
      }
    ]
  };
}

export function alternateBellCurves() {
  return {
    title: 'Curvy Calculations',
    showLegend: true,
    scales: [
      {
        label: 'X',
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        domain: [-5, 5]
      }, {
        label: 'Y',
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        scale: d3.scaleLinear(),
        domain: [0, 12]
      }
    ],
    groups: [
      {
        label: 'Data Group 1',
        disabled: false,
        xScale: 0,
        yScale: 1,
        lines: [
          {
            type: Model.ChartLineType.Main,
            visible: true,
            noPoints: true,
            fill: 'none',
            points: getBulkPoints(-5, 5)
          }
        ]
      }
    ]
  };
}