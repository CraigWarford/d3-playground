import * as d3 from 'd3';
import * as Model from '../chart/components';
import { D3ScaleType } from '../chart/components';

export function basicLongLabels() {
  return {
    title: 'Some Really Long Labels',
    showLegend: true,
    scales: [
      {
        label: 'Type of Service',
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Band,
        grid: true,
        scale: d3.scaleBand(),
        domain: [
          'This is an example of a label too long to show on one line',
          'Here\'s another example of something that should wrap',
          'No wrap',
          'Abercrombie&Fitch&Mather&Something should split'
        ]
      }, {
        label: 'Average Value',
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        grid: true,
        scale: d3.scaleLinear(),
        domain: [0, 10]
      }
    ],
    groups: [
      {
        label: 'Just Some Bars',
        disabled: false,
        xScale: 0,
        yScale: 1,
        bars: {
          showAltLabel: true,
          points: [
          ['This is an example of a label too long to show on one line', 4.25531914893617],
          ['Here\'s another example of something that should wrap', 2.272727272727273],
          ['No wrap', 0.78125],
          ['Abercrombie&Fitch&Mather&Something should split', 2.631578947368421]
        ]}
      }
    ]
  };
}

export function alternateLongLabels() {
  return {
    title: 'Change it Up!',
    showLegend: true,
    scales: [
      {
        label: 'Type of Service',
        location: Model.ChartAxisLocation.Bottom,
        type: D3ScaleType.Band,
        grid: true,
        scale: d3.scaleBand(),
        domain: [
          'No wrap',
          'Let us wrap this one as well',
          'Here\'s another example of something that should wrap',
          'This is an example of a label too long to show on one line'
        ]
      }, {
        label: 'Median Value',
        location: Model.ChartAxisLocation.Left,
        type: D3ScaleType.Linear,
        grid: true,
        scale: d3.scaleLinear(),
        domain: [0, 100],
        lockDomain: true
      }
    ],
    groups: [
      {
        label: 'Just Some Bars',
        disabled: false,
        xScale: 0,
        yScale: 1,
        bars: {
          showAltLabel: true,
          displayAsPercentage: false,
          points: [
            ['No wrap', 0.78125],
            ['Let us wrap this one as well', 2.631578947368421],
            ['Here\'s another example of something that should wrap', 2.272727272727273],
            ['This is an example of a label too long to show on one line', 4.25531914893617]
          ]}
      }
    ]
  };
}

